//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_TYPES_H__
#define __AN_TYPES_H__

#include "Namespaces.h"

//---------------------------------------------------------------------------------------
AN_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
// Base Types
//---------------------------------------------------------------------------------------

typedef int                 AInt32;
typedef unsigned int        AUInt32;

typedef long long           AInt64;
typedef unsigned long long  AUInt64;

typedef short               AInt16;
typedef unsigned short      AUInt16;

typedef char                AInt8;
typedef unsigned char       AUInt8;

typedef char                AByte;
typedef unsigned char       AUByte;

typedef float               AReal32;
typedef double              AReal64;

typedef char                AChar;
typedef wchar_t             AWChar;

typedef void                AVoid;
typedef void*               AVoidPtr;

typedef bool                ABool;

typedef short               AIndex16;
typedef int                 AIndex32;
typedef long long           AIndex64;



//---------------------------------------------------------------------------------------
AN_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_TYPES_H__
//---------------------------------------------------------------------------------------
