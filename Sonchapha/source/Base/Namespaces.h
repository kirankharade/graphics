//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_NAMESPACES_H__
#define __AN_NAMESPACES_H__
//---------------------------------------------------------------------------------------

#define AN_START_NAMESPACE   namespace An {
#define AN_END_NAMESPACE }
    
#define AN_MATHS_START_NAMESPACE namespace An { namespace Maths {
#define AN_MATHS_END_NAMESPACE } }
    
#define AN_CONTAINERS_START_NAMESPACE namespace An { namespace Containers {
#define AN_CONTAINERS_END_NAMESPACE } }

//---------------------------------------------------------------------------------------
#endif //__AN_NAMESPACES_H__
//---------------------------------------------------------------------------------------
