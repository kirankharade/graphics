//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_BOX_H__
#define __C_BOX_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CVector3f.h"
#include "CMatrix4f.h"
#include <vector>

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
class CLine;
class CRay;
class CLineSegment;
class CPointTriangle;
class CPointIntersectionInfo;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CBox
{
    protected:
        
        CPoint3f    m_origin;
        CVector3f   m_xAxis;
        CVector3f   m_yAxis;
        CVector3f   m_zAxis;
        AReal32     m_width;
        AReal32     m_height;
        AReal32     m_depth;

    public:
        
        CBox();

        CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
             const AReal32& width, const AReal32& height, const AReal32& depth);
    
        CBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);
    
        CBox(const AReal32& width, const AReal32& height, const AReal32& depth);

        CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size);

        CBox(const CPoint3f& origin, const AReal32& size);

        CBox(const AReal32& size);

        CBox(const CBox& box);
    
        virtual ~CBox();
    
        void set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
                 const AReal32& width, const AReal32& height, const AReal32& depth);
    
        void set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size);

        void setOrigin(const CPoint3f& origin);

        void setDirections(const CVector3f& xDirection, const CVector3f& yDirection);

        void setXDirection(const CVector3f& xDirection);

        void setYDirection(const CVector3f& yDirection);

        void setSize(const AReal32& size);

        void setWidth(const AReal32& width);

        void setHeight(const AReal32& height);

        void setDepth(const AReal32& width);

        CPoint3f getOrigin() const;

        CVector3f getXDirection() const;

        CVector3f getYDirection() const;

        AReal32 getWidth() const;

        AReal32 getHeight() const;

        AReal32 getDepth() const;

        CPoint3f getCentre() const;

        AReal32 getVolume() const;

        AReal32 getSurfaceArea() const;

        CPoint3f* getCorners() const;

        CLineSegment* getEdges() const;

        CPointTriangle* getFaceTriangles() const;

        CPlane* getPlanes() const;

        virtual bool isIntersecting(const CPlane* pPlane) const;
       
        virtual CPointIntersectionInfo* getIntersections(const CPlane* pPlane) const;

        virtual bool isIntersecting(const CLine* pLine) const;

        virtual CPointIntersectionInfo* getIntersections(const CLine* pLine) const;

        virtual bool isIntersecting(const CLineSegment* pLineSegment) const;

        virtual CPointIntersectionInfo* getIntersections(const CLineSegment* pLineSegment) const;

        virtual bool isIntersecting(const CRay* pRay) const;

        virtual CPointIntersectionInfo* getIntersections(const CRay* pRay) const;

        virtual bool isInside(const CPoint3f& point, const AReal32& tolerance = TOLERANCE_32) const;

        CBox* transform(const CMatrix4f& matrix) const;

        //Assignment operator

        CBox& operator = (const CBox& box);
    
        //Comparison Operators
         
        bool operator == (const CBox& box) const;
    
        bool operator != (const CBox& box) const;
    
        bool operator < (const CBox& box) const;
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_BOX_H__
//---------------------------------------------------------------------------------------

