//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CRay.h"
#include "MathsSettings.h"
#include "CPlane.h"
#include "CLine.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CRay::CRay()
:  m_point(CPoint3f())
 , m_direction(CVector3f(0.0f, 0.0f, 1.0f))
{
}
//---------------------------------------------------------------------------------------

CRay::CRay(const CPoint3f& point, const CVector3f& direction)
:  m_point(point)
 , m_direction(direction)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CRay::CRay(const CPoint3f& firstPoint, const CPoint3f& secondPoint)
:  m_point(firstPoint)
 , m_direction(secondPoint - firstPoint)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CRay::CRay(const CRay& ray)
{
    m_point = ray.m_point;
    m_direction = ray.m_direction;
}
//---------------------------------------------------------------------------------------

CRay::~CRay()
{
}
//---------------------------------------------------------------------------------------

void CRay::set(const CPoint3f& point, const CVector3f& direction)
{
    m_point = point;
    m_direction = direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

void CRay::set(const CPoint3f& firstPoint, const CPoint3f& secondPoint)
{
    m_point = firstPoint;
    m_direction = secondPoint - firstPoint;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CPoint3f CRay::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

CVector3f CRay::getDirection() const
{
    return m_direction;
}
//---------------------------------------------------------------------------------------

bool CRay::isIntersecting(const CPlane* pPlane) const
{
    CPoint3f intersectionPoint;
    return isIntersecting(pPlane, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool CRay::isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const
{
    CLine line(m_point, m_direction);

    if(false == line.isIntersecting(pPlane, intersectionPoint))
    {
        return false;
    }
    
    const AReal32 dist = 100.0f;
    CPoint3f secondPoint = (m_point + (m_direction * dist));
    CVector3f v1 = secondPoint - m_point;
    CVector3f v2 = intersectionPoint - m_point;

    AReal32 u = v1.dot(v2) / v1.dot(v1);

    if(u >= (-TOLERANCE_32))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CRay::passesThrough(const CPoint3f& point, const AReal32 tolerance) const
{
    CLine line(m_point, m_direction);

    if(line.passesThrough(point, tolerance))
    {
        const AReal32 dist = 100.0f;
        CPoint3f secondPoint = m_point + (m_direction * dist);

        CVector3f v1 = secondPoint - m_point;
        CVector3f v2 = point - m_point;

        AReal32 u = v1.dot(v2) / v1.dot(v1);

        if(u >= (-TOLERANCE_32))
        {
            return true;
        }

        //AReal32 xProjDist = secondPoint.x - m_point.x;
        //AReal32 yProjDist = secondPoint.y - m_point.y;
        //AReal32 zProjDist = secondPoint.z - m_point.z;
        //AReal32 u = 0;
        //if(!EQ_32(xProjDist, 0))
        //{
        //    u = (point.x - m_point.x) / xProjDist;
        //}
        //else if(!EQ_32(yProjDist, 0))
        //{
        //    u = (point.y - m_point.y) / yProjDist;
        //}
        //else if(!EQ_32(zProjDist, 0))
        //{
        //    u = (point.z - m_point.z) / zProjDist;
        //}
        //else
        //{
        //    return false;
        //}
        //if(u >= 0.0f)
        //{
        //    return true;
        //}
    }
    return false;
}
//---------------------------------------------------------------------------------------


CPoint3f CRay::closestPoint(const CPoint3f& point) const
{
    CLine line(m_point, m_direction);
    CPoint3f pt = line.closestPoint(point);
    if(point.dist(pt) > point.dist(m_point))
    {
        return m_point;
    }
    return pt;
}
//---------------------------------------------------------------------------------------

AReal32 CRay::distFromPoint(const CPoint3f& point) const
{
    CPoint3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CRay& CRay::operator = (const CRay& ray)
{
    m_point = ray.m_point;
    m_direction = ray.m_direction;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CRay::operator == (const CRay& ray) const
{
    CVector3f dir = m_direction;
    CVector3f otherDir = ray.m_direction;
    dir.normalize();
    otherDir.normalize();

    if((dir == otherDir) && (m_point == ray.m_point))
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CRay::operator != (const CRay& ray) const
{
    if( !(*this == ray) )
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CRay::operator < (const CRay& ray) const
{
    if(m_point < ray.m_point)
    {
        return true;
    }
    if(m_point > ray.m_point)
    {
        return false;
    }
    if(m_direction < ray.m_direction)
    {
        return true;
    }
    if(m_direction > ray.m_direction)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
