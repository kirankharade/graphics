//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CUtils.h"
#include "MathsMiscUtils.h"
#include "CPoint2f.h"
#include "CPoint3f.h"
#include "CRay.h"
#include "CLineSegment.h"
#include "CPlane.h"
#include "CPointTriangle.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

AReal32 CUtils::getCornerAngleDeg(const CPoint2f& pa, const CPoint2f& pb, const CPoint2f& pc)
{
    CVector3f v1 = pa - pb;
    CVector3f v2 = pc - pb;
    AReal32 angleDeg = v1.angleDeg(v2);

    CVector3f normal = (pb - pa).cross(pc - pb);
    normal.normalize();

    if(normal.z > 0.0f)
    {
        angleDeg = (AReal32) (360.0 - angleDeg);
    }

    return angleDeg;
}
//---------------------------------------------------------------------------------------

AReal32 CUtils::getCornerAngleDeg(const CPoint3f& pa, const CPoint3f& pb, const CPoint3f& pc)
{
    CVector3f v1 = pa - pb;
    CVector3f v2 = pc - pb;
    AReal32 angleDeg = v1.angleDeg(v2);

    CVector3f normal = (pb - pa).cross(pc - pb);
    normal.normalize();

    if(normal.z > 0.0f)
    {
        angleDeg = (AReal32) (360.0 - angleDeg);
    }

    return angleDeg;
}
//---------------------------------------------------------------------------------------

void CUtils::sortAscending(std::vector<CPoint2f>& points)
{
    AUInt32 size = points.size();

    for(AUInt32 i = 0; i < size; i++)
    {
        for(AUInt32 j = 0; j < size; j++)
        {
            if(points[i] > points[j])
            {
               Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void CUtils::sortAscending(std::vector<CPoint3f>& points)
{
    AUInt32 size = points.size();

    for(AUInt32 i = 0; i < size; i++)
    {
        for(AUInt32 j = 0; j < size; j++)
        {
            if(points[i] > points[j])
            {
               Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void CUtils::sortDescending(std::vector<CPoint2f>& points)
{
    AUInt32 size = points.size();

    for(AUInt32 i = 0; i < size; i++)
    {
        for(AUInt32 j = 0; j < size; j++)
        {
            if(points[i] < points[j])
            {
               Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void CUtils::sortDescending(std::vector<CPoint3f>& points)
{
    AUInt32 size = points.size();

    for(AUInt32 i = 0; i < size; i++)
    {
        for(AUInt32 j = 0; j < size; j++)
        {
            if(points[i] < points[j])
            {
               Swap(points[i], points[j]);
            }
        }
    }
}
//---------------------------------------------------------------------------------------

CPoint2f CUtils::getCentroid(const std::vector<CPoint2f>& points)
{
    AUInt32 size = points.size();
    CPoint2f centroid = CPoint2f(0, 0);
    if(size)
    {
        for(AUInt32 i = 0; i < size; i++)
        {
            centroid += points[i];
        }
    }
    return centroid / (AReal32) size;
}
//---------------------------------------------------------------------------------------

CPoint3f CUtils::getCentroid(const std::vector<CPoint3f>& points)
{
    AUInt32 size = points.size();
    CPoint3f centroid = CPoint3f(0, 0, 0);
    if(size)
    {
        for(AUInt32 i = 0; i < size; i++)
        {
            centroid += points[i];
        }
    }
    return centroid / (AReal32) size;
}
//---------------------------------------------------------------------------------------


SCoordinateExtents CUtils::getExtents(const std::vector<CPoint2f>& points)
{
    AUInt32 count = points.size();

    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;

    for(AUInt32 i = 0; i < count; i++)
    {
        const CPoint2f& pt = points[i];
        if(pt.x > maxx) maxx = pt.x;
        if(pt.y > maxy) maxy = pt.y;

        if(pt.x < minx) minx = pt.x;
        if(pt.y < miny) miny = pt.y;
    }

    return SCoordinateExtents(CPoint3f(minx, miny, 0), CPoint3f(maxx, maxy, 0));
}

//---------------------------------------------------------------------------------------

SCoordinateExtents CUtils::getExtents(const std::vector<CPoint3f>& points)
{
    AUInt32 count = points.size();

    AReal32 maxx = -MAX_REAL32;
    AReal32 maxy = -MAX_REAL32;
    AReal32 maxz = -MAX_REAL32;

    AReal32 minx = MAX_REAL32;
    AReal32 miny = MAX_REAL32;
    AReal32 minz = MAX_REAL32;

    for(AUInt32 i = 0; i < count; i++)
    {
        const CPoint3f& pt = points[i];
        if(pt.x > maxx) maxx = pt.x;
        if(pt.y > maxy) maxy = pt.y;
        if(pt.z > maxz) maxz = pt.z;

        if(pt.x < minx) minx = pt.x;
        if(pt.y < miny) miny = pt.y;
        if(pt.z < minz) minz = pt.z;
    }

    return SCoordinateExtents(CPoint3f(minx, miny, minz), CPoint3f(maxx, maxy, maxz));
}
//---------------------------------------------------------------------------------------

AReal32 CUtils::GetMax(AReal32 a, AReal32 b, AReal32 c)
{
    AReal32 max = a;
    if(b > max)	max = b;
    if(c > max) max = c;
    return max;
}
//---------------------------------------------------------------------------------------

AReal32 CUtils::GetMin(AReal32 a, AReal32 b, AReal32 c)
{
    AReal32 min = a;
    if(b < min)	min = b;
    if(c < min) min = c;
    return min;
}
//---------------------------------------------------------------------------------------

