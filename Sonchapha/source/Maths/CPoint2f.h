//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_POINT_2F_H__
#define __C_POINT_2F_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CPoint2i.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_MATHS_API CPoint2f
{
    public:
        
        union 
        {
            AReal32 v[2];

            struct 
            {
                AReal32 x;
                AReal32 y;
            };
        };


    public:
        
        CPoint2f();
    
        CPoint2f(const AReal32 v[2]);
    
        CPoint2f(const AReal32& a, const AReal32& b);
    
        CPoint2f(const CPoint2f& point);
    
        CPoint2f(const CPoint2i& point);
    
        CPoint2f(const CPoint3f& point);

        void set(const AReal32& a, const AReal32& b);
    
        void set(const CPoint2f& point);
    
        AReal32 dist(const CPoint2f& point) const;
    
        //Index Operators
    
        AReal32& operator [] (const AIndex32& i);
    
        const AReal32& operator [] (const AIndex32& i) const;
    
        //Summation and subtraction operators

        CVector3f operator - (const CPoint2f& point) const;

        CPoint2f operator + (const CPoint2f& point) const;

        CPoint2f operator + (const CVector3f& vec) const;

        CPoint2f& operator += (const CPoint2f& point);

        CPoint2f& operator += (const CVector3f& vec);

        CPoint2f& operator -= (const CVector3f& vec);

        //assignment operator

        CPoint2f& operator = (const CPoint2f& point);
    
        CPoint2f& operator = (const CPoint3f& point);
    
        CPoint2f& operator = (const CPoint2i& point);
    
        //Comparison Operators
         
        bool operator == (const CPoint2f& point) const;
    
        bool operator != (const CPoint2f& point) const;
    
        bool operator < (const CPoint2f& point) const;
        
        bool operator > (const CPoint2f& point) const;

        //Operators with scalar arguments
    
        CPoint2f operator * (const AReal32& scalar) const;
    
        CPoint2f& operator *= (const AReal32& scalar);
    
        CPoint2f operator / (const AReal32& scalar) const;
    
        CPoint2f& operator /= (const AReal32& scalar);
    
        CPoint2f operator - () const;

};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<CPoint2f>;
template class AN_MATHS_API std::vector<CPoint2f>;
#endif

typedef std::vector<CPoint2f> CPoint2fList;
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_POINT_2I_H__
//---------------------------------------------------------------------------------------

