//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CPolygon2D.h"
#include "CUtils.h"
#include "MathsSettings.h"
#include "CPointTriangle.h"
#include "CConverter.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

const static AReal32 POLYGON2D_TOLERANCE = TOLERANCE_32 * 10;
const static AReal32 POLYGON_EXTERIOR_REGION_SCALE  = 5.0;
//---------------------------------------------------------------------------------------

CPolygon2D::CPolygon2D()
{
    init();
    m_convexHull = CConvexHull2D();
}
//---------------------------------------------------------------------------------------

CPolygon2D::CPolygon2D(const std::vector<CPoint2f>& points)
{
    init();
    m_inputPoints = points;
    m_convexHull = CConvexHull2D(m_inputPoints);
    update();
}
//---------------------------------------------------------------------------------------

CPolygon2D::CPolygon2D(const std::vector<CPoint2i>& points)
{
    init();
    m_inputPoints = CConverter::convertTo2f(points);
    m_convexHull = CConvexHull2D(m_inputPoints);
    update();
}
//---------------------------------------------------------------------------------------

CPolygon2D::CPolygon2D(const CPolygon2D& polygon)
{
    copy(polygon);
}
//---------------------------------------------------------------------------------------

CPolygon2D::~CPolygon2D()
{
}
//---------------------------------------------------------------------------------------

void CPolygon2D::set(const std::vector<CPoint2f>& points)
{
    m_inputPoints = points;
    m_convexHull = CConvexHull2D(m_inputPoints);
    m_bIsSelfIntersecting = false;
    m_bIsConcave = false;
    update();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::add(const std::vector<CPoint2f>& points)
{
    m_inputPoints.insert(m_inputPoints.end(), points.begin(), points.end());
    m_convexHull = CConvexHull2D(m_inputPoints);
    m_bIsSelfIntersecting = false;
    m_bIsConcave = false;
    update();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::add(const CPoint2f& point)
{
    m_inputPoints.push_back(point);
    m_convexHull = CConvexHull2D(m_inputPoints);
    m_bIsSelfIntersecting = false;
    m_bIsConcave = false;
    update();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::remove(const CPoint2f& point)
{
    bool bPointRemoved = false;
    for(std::vector<CPoint2f>::iterator itr = m_inputPoints.begin(); itr != m_inputPoints.end(); itr++)
    {
        if(point == *itr)
        {
            m_inputPoints.erase(itr);
            bPointRemoved = true;
            break;
        }
    }
    if(bPointRemoved)
    {
        m_convexHull = CConvexHull2D(m_inputPoints);
        m_bIsSelfIntersecting = false;
        m_bIsConcave = false;
        update();
    }
}
//---------------------------------------------------------------------------------------

void CPolygon2D::removeAt(const AIndex32& index)
{
    AInt32 count = (AInt32) m_inputPoints.size();
    if((index < 0 && index >= count) || (count == 0))
    {
        return;
    }
    m_inputPoints.erase(m_inputPoints.begin() + index);
    m_convexHull = CConvexHull2D(m_inputPoints);
    m_bIsSelfIntersecting = false;
    m_bIsConcave = false;
    update();
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isSelfIntersecting() const
{
    return m_bIsSelfIntersecting;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isConcave() const
{
    return m_bIsConcave;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isInside(const CPoint2i& point) const
{
    return isInside(CConverter::convertTo2f(point));
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isInside(const CPoint2f& point) const
{
    //This algorithm checks whether a given point is inside the polygon by drawing a line
    //passing through point and finding the no. of intersections of that line with the
    //polygon edges. If the intersections are even, the point is outside the polygon,
    //else it is inside.

    if(m_inputPoints.size() < 3)
    {
        return false;
    }

    for(AUInt32 i = 0; i < m_allEdges.size(); i++)
    {
        const CLineSegment2D& edge = m_allEdges[i];
        if(edge.passesThrough(point, POLYGON2D_TOLERANCE))
        {
            return true;
        }
    }

    //References:
    //1. http://alienryderflex.com/polygon/
    //2. http://www.ytechie.com/2009/08/determine-if-a-point-is-contained-within-a-polygon.html

    bool oddIntersectionCount = false;
    AInt32 count = (AInt32) m_inputPoints.size();
    AIndex32 i = 0;
    AIndex32 j = count - 1;

    const std::vector<CPoint2f>& pts = m_inputPoints;

    for(i = 0; i < count; i++)
    {
        if( (pts[i].y < point.y && pts[j].y >= point.y) ||
            (pts[j].y < point.y && pts[i].y >= point.y)
            )
        {
            AReal32 ratio = (point.y - pts[i].y) / (pts[j].y - pts[i].y);
            AReal32 x = pts[i].x +  (ratio * (pts[j].x - pts[i].x));
            if(x < (point.x - POLYGON2D_TOLERANCE))
            {
                oddIntersectionCount = !oddIntersectionCount;
            }
        }
        if ((fabs(pts[i].y - point.y) < POLYGON2D_TOLERANCE && pts[i].x >= point.x))
        {
           return true;
        }

        j = i;
    }
    return oddIntersectionCount;

    /*
    //Alternative approach: Little more thorough ...

    //Create a line which passes through 'point' and is parallel to X-axis
    CRay ray(point, X_AXIS);
    AReal32 intersectionCount = 0;
    AUInt32 edgeCount = m_allEdges.size();

    for(AUInt32 i = 0; i < edgeCount; i++)
    {
        const CLineSegment2D& edge = m_allEdges[i];
        if(edge.passesThrough(point, POLYGON2D_TOLERANCE))
        {
            return true;
        }
        CLineSegment2D::IntersectionClassification ic = edge.classifyByIntersection(ray);
        if(ic == CLineSegment2D::Intersecting)
        {
            intersectionCount += 1.0;
        }
        if(ic == CLineSegment2D::IntersectingAtEnds)
        {
            //To avoid multiple consideration of the same intersection

            bool bFirst = ray.passesThrough(CConverter::convertTo3f(edge.getFirstPoint()));
            bool bSecond = ray.passesThrough(CConverter::convertTo3f(edge.getSecondPoint()));

            if(bFirst && bSecond)
            {
                //Whole edge lies on the line...
                intersectionCount += (0.5 + 1.0 + 0.5);
            }
            if((bFirst && !bSecond) || (!bFirst && bSecond))
            {
                intersectionCount += 0.5;
            }
        }
    }

    AInt32 intersections = (AInt32) intersectionCount;
    if(0 == (intersections % 2))
    {
        return false;
    }
    return true;
    */
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isInside(const CLineSegment2D& ls) const
{
    CPoint2f p[6];

    p[0] = ls.getFirstPoint();
    p[1] = ls.getSecondPoint();
    CVector3f v = p[1] - p[0];
    p[2] = p[0] + (v * 0.2f);
    p[3] = p[0] + (v * 0.4f);
    p[4] = p[0] + (v * 0.6f);
    p[5] = p[0] + (v * 0.8f);

    for(AUInt32 i = 0; i < 6; i++)
    {
        if(false == isInside(p[i]))
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isIntersectingEdges(const CLineSegment2D& lineSegment) const
{
    AUInt32 edgeCount = m_allEdges.size();

    for(AUInt32 i = 0; i < edgeCount; i++)
    {
        const CLineSegment2D& edge = m_allEdges[i];
        CLineSegment2D::IntersectionClassification ic = edge.classifyByIntersection(&lineSegment);
        if(ic == CLineSegment2D::Intersecting || ic == CLineSegment2D::IntersectingAtEnds)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPolygon2D::BoundaryQualification CPolygon2D::qualifyAgainstBoundary(const CPoint2f& point) const
{
    CPolygon2D::BoundaryQualification q = CPolygon2D::UndefinedBoundaryQualification;

    AInt32 pointCount = (AInt32) m_inputPoints.size();
    if(pointCount == 0)
    {
        return q;
    }
    const std::vector<CPoint2f>& hullPoints = m_convexHull.getHullPoints();
    AInt32 hullPointCount = (AInt32) hullPoints.size();

    bool oddIntersectionCount = false;

    if(!m_bIsSelfIntersecting)
    {
        AIndex32 i = 0;
        AIndex32 j = pointCount - 1;

        const std::vector<CPoint2f>& pts = m_inputPoints;

        for(i = 0; i < pointCount; i++)
        {
            if( (pts[i].y < point.y && pts[j].y >= point.y) ||
                (pts[j].y < point.y && pts[i].y >= point.y)
                )
            {
                AReal32 ratio = (point.y - pts[i].y) / (pts[j].y - pts[i].y);
                AReal32 x = pts[i].x +  (ratio * (pts[j].x - pts[i].x));
                if(x < (point.x - POLYGON2D_TOLERANCE))
                {
                    oddIntersectionCount = !oddIntersectionCount;
                }
            }
            if ((fabs(pts[i].y - point.y) < POLYGON2D_TOLERANCE && pts[i].x >= point.x))
            {
               q = CPolygon2D::OnBoundary;
               return q;
            }

            j = i;
        }
        if(oddIntersectionCount)
        {
            q = CPolygon2D::InsideBoundary;
        }
        else
        {
            q = CPolygon2D::OutsideBoundary;
        }
    }
    else
    {
        if(hullPointCount > 0)
        {
            AIndex32 i = 0;
            AIndex32 j = hullPointCount - 1;

            const std::vector<CPoint2f>& pts = hullPoints;

            for(i = 0; i < hullPointCount; i++)
            {
                if( (pts[i].y < point.y && pts[j].y >= point.y) ||
                    (pts[j].y < point.y && pts[i].y >= point.y)
                    )
                {
                    AReal32 ratio = (point.y - pts[i].y) / (pts[j].y - pts[i].y);
                    AReal32 x = pts[i].x +  (ratio * (pts[j].x - pts[i].x));
                    if(x < (point.x - POLYGON2D_TOLERANCE))
                    {
                        oddIntersectionCount = !oddIntersectionCount;
                    }
                }
                if ((fabs(pts[i].y - point.y) < POLYGON2D_TOLERANCE && pts[i].x >= point.x))
                {
                   q = CPolygon2D::OnHullBoundary;
                   return q;
                }

                j = i;
            }

            if(oddIntersectionCount)
            {
                q = CPolygon2D::InsideHullBoundary;
            }
            else
            {
                q = CPolygon2D::OutsideHullBoundary;
            }
        }
    }
    return q;
}
//---------------------------------------------------------------------------------------

CPolygon2D::BoundaryQualification CPolygon2D::qualifyAgainstBoundary(const CLineSegment2D& ls) const
{
    CPoint2f p1 = ls.getFirstPoint();
    CPoint2f p2 = ls.getSecondPoint();

    CPolygon2D::BoundaryQualification q1 = qualifyAgainstBoundary(p1);
    CPolygon2D::BoundaryQualification q2 = qualifyAgainstBoundary(p2);

    if(m_bIsSelfIntersecting)
    {
        if(q1 == CPolygon2D::OutsideHullBoundary && q2 == CPolygon2D::OutsideHullBoundary)
        {
           return CPolygon2D::OutsideHullBoundary;
        }
        if(q1 == CPolygon2D::InsideHullBoundary && q2 == CPolygon2D::InsideHullBoundary)
        {
           return CPolygon2D::InsideHullBoundary;
        }
        return CPolygon2D::OnHullBoundary;
    }
    else
    {
        if(q1 == CPolygon2D::OutsideBoundary && q2 == CPolygon2D::OutsideBoundary)
        {
           return CPolygon2D::OutsideBoundary;
        }
        if(q1 == CPolygon2D::InsideBoundary && q2 == CPolygon2D::InsideBoundary)
        {
           return CPolygon2D::InsideBoundary;
        }
        return CPolygon2D::OnBoundary;
    }
    return CPolygon2D::UndefinedBoundaryQualification;
}
//---------------------------------------------------------------------------------------

CPolygon2D::BoundaryQualification CPolygon2D::qualifyAgainstBoundary(const CPointTriangle& triangle) const
{
    CPolygon2D::BoundaryQualification q = CPolygon2D::UndefinedBoundaryQualification;

    AInt32 pointCount = (AInt32) m_inputPoints.size();
    if(pointCount == 0)
    {
        return q;
    }

    CPoint2f p1 = triangle[0];
    CPoint2f p2 = triangle[1];
    CPoint2f p3 = triangle[2];

    CPolygon2D::BoundaryQualification q1 = qualifyAgainstBoundary(p1);
    CPolygon2D::BoundaryQualification q2 = qualifyAgainstBoundary(p2);
    CPolygon2D::BoundaryQualification q3 = qualifyAgainstBoundary(p3);

    if(m_bIsSelfIntersecting)
    {
        if( q1 == CPolygon2D::InsideHullBoundary || q2 == CPolygon2D::InsideHullBoundary || q3 == CPolygon2D::InsideHullBoundary )
        {
            return CPolygon2D::InsideHullBoundary;
        }
        if( q1 == CPolygon2D::OnHullBoundary || q2 == CPolygon2D::OnHullBoundary || q3 == CPolygon2D::OnHullBoundary )
        {
            return CPolygon2D::OnHullBoundary;
        }
    }
    else
    {
        if( q1 == CPolygon2D::InsideBoundary || q2 == CPolygon2D::InsideBoundary || q3 == CPolygon2D::InsideBoundary )
        {
            return CPolygon2D::InsideBoundary;
        }
        if( q1 == CPolygon2D::OnBoundary || q2 == CPolygon2D::OnBoundary || q3 == CPolygon2D::OnBoundary )
        {
            return CPolygon2D::OnBoundary;
        }
    }

    CLineSegment2D ls[3];
    ls[0] = CLineSegment2D(p1, p2);
    ls[1] = CLineSegment2D(p2, p3);
    ls[2] = CLineSegment2D(p3, p1);

    CPolygon2D::BoundaryQualification qx = qualifyAgainstBoundary(ls[0]);
    CPolygon2D::BoundaryQualification qy = qualifyAgainstBoundary(ls[1]);
    CPolygon2D::BoundaryQualification qz = qualifyAgainstBoundary(ls[2]);

    AUInt32 edgeCount = m_allEdges.size();

    CPolygon2D::BoundaryQualification onBoundaryQualification = m_bIsSelfIntersecting ? CPolygon2D::OnHullBoundary : CPolygon2D::OnBoundary;

    if( qx == CPolygon2D::InsideHullBoundary || qy == CPolygon2D::InsideHullBoundary || qz == CPolygon2D::InsideHullBoundary ||
        qx == CPolygon2D::OnHullBoundary || qy == CPolygon2D::OnHullBoundary || qz == CPolygon2D::OnHullBoundary ||
        qx == CPolygon2D::InsideBoundary || qy == CPolygon2D::InsideBoundary || qz == CPolygon2D::InsideBoundary ||
        qx == CPolygon2D::OnBoundary || qy == CPolygon2D::OnBoundary || qz == CPolygon2D::OnBoundary)
    {
        return onBoundaryQualification;
    }
    for(AUInt32 i = 0; i < edgeCount; i++)
    {
        const CLineSegment2D& edge = m_allEdges[i];
        for(AUInt32 j = 0; j < 3; j++)
        {
            CLineSegment2D::IntersectionClassification ic = ls[j].classifyByIntersection(&edge);
            if(ic == CLineSegment2D::Intersecting || ic == CLineSegment2D::IntersectingAtEnds)
            {
                return onBoundaryQualification;
            }
        }
    }

    for(AIndex32 i = 0; i < pointCount; i++)
    {
        if(triangle.isInside(CConverter::convertTo3f(m_inputPoints[i])))
        {
            return CPolygon2D::OnBoundary;
        }
    }

    return CPolygon2D::UndefinedBoundaryQualification;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isPolygonOnBothSidesOfSegment(const CLineSegment2D& lineSegment) const
{
    AInt32 side1PointCount = 0;
    AInt32 side2PointCount = 0;
    CVector3f v1 = lineSegment.getDirection();
    CPoint2f p = lineSegment.getFirstPoint();

    AUInt32 count = m_inputPoints.size();
    for(AUInt32 i = 0; i < count; i++)
    {
        CVector3f v2 = m_inputPoints[i] - p;
        v2.normalize();
        CVector3f n = v1.cross(v2);
        if(n.z < TOLERANCE_32)
        {
            side1PointCount++;
        }
        if(n.z > TOLERANCE_32)
        {
            side2PointCount++;
        }
        if(side1PointCount > 0 && side2PointCount > 0)
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

AReal32 CPolygon2D::getArea() const
{
    AReal32 area = -1.0; //For self-intersecting polygon
    if(m_bIsSelfIntersecting)
    {
        return area;
    }
    AUInt32 count = m_interiorTriangles.size();
    for(AUInt32 i = 0; i < count; i++)
    {
        area += m_interiorTriangles[i].area();
    }
    return area;
}
//---------------------------------------------------------------------------------------

AReal32 CPolygon2D::getPerimeter() const
{
    AReal32 perimeter = 0;
    AUInt32 count = m_allEdges.size();
    for(AUInt32 i = 0; i < count; i++)
    {
        perimeter += m_allEdges[i].getLength();
    }
    return perimeter;
}
//---------------------------------------------------------------------------------------

CPoint2f CPolygon2D::getCentroid() const
{
    return m_centroidPoint;
}
//---------------------------------------------------------------------------------------

AUInt32 CPolygon2D::getPointCount() const
{
    return m_inputPoints.size();
}
//---------------------------------------------------------------------------------------

const std::vector<CPoint2f>& CPolygon2D::getPoints() const
{
    return m_inputPoints;
}
//---------------------------------------------------------------------------------------

const CConvexHull2D& CPolygon2D::getConvexHull() const
{
    return m_convexHull;
}
//---------------------------------------------------------------------------------------

const std::vector<CLineSegment2D>& CPolygon2D::getAllEdges() const
{
    return m_allEdges;
}
//---------------------------------------------------------------------------------------

const std::vector<CPointTriangle>& CPolygon2D::getInteriorTriangles() const
{
    return m_interiorTriangles;
}
//---------------------------------------------------------------------------------------

std::vector<CPointTriangle> CPolygon2D::getExteriorTriangles(const AReal32& containerScale) const
{
    const SCoordinateExtents& extents = m_convexHull.getExtents();
    AReal32 maxExtents = (extents.maxPoint - extents.minPoint).length();
    AReal32 outerBoundaryOffset = containerScale * maxExtents;

    std::vector<CPointTriangle> tris;
    getTrianglesExteriorToConvexHull(outerBoundaryOffset, tris);
    return tris;
}
//---------------------------------------------------------------------------------------

CPolygon2D CPolygon2D::getScaled(const AReal32& scaleX, const AReal32& scaleY) const
{
    const AInt32 pointCount = (AInt32) m_inputPoints.size();
    std::vector<CPoint2f> transformedPoints = m_inputPoints;

    for(AIndex32 i = 0; i < pointCount; i++)
    {
        transformedPoints[i].x *= scaleX;
        transformedPoints[i].y *= scaleY;
    }

    return CPolygon2D(transformedPoints);
}
//---------------------------------------------------------------------------------------

CPolygon2D CPolygon2D::getRotated(const CPoint2f& rotationCentre, const AReal32& angleDeg) const
{
    const AInt32 pointCount = (AInt32) m_inputPoints.size();
    std::vector<CPoint2f> transformedPoints = m_inputPoints;
    AReal32 cosTheta = (AReal32) cos(DEG_TO_RAD64(angleDeg));
    AReal32 sinTheta = (AReal32) sin(DEG_TO_RAD64(angleDeg));

    AReal32 x = 0, y = 0;

    for(AIndex32 i = 0; i < pointCount; i++)
    {
        transformedPoints[i].x -= rotationCentre.x;
        transformedPoints[i].y -= rotationCentre.y;

        x = (transformedPoints[i].x * cosTheta) - (transformedPoints[i].y * sinTheta);
        y = (transformedPoints[i].y * cosTheta) + (transformedPoints[i].x * sinTheta);

        transformedPoints[i].x = x + rotationCentre.x;
        transformedPoints[i].y = y + rotationCentre.y;
    }

    return CPolygon2D(transformedPoints);
}
//---------------------------------------------------------------------------------------

CPolygon2D CPolygon2D::getTranslated(const AReal32& dx, const AReal32& dy) const
{
    const AInt32 pointCount = (AInt32) m_inputPoints.size();
    std::vector<CPoint2f> transformedPoints = m_inputPoints;

    for(AIndex32 i = 0; i < pointCount; i++)
    {
        transformedPoints[i].x += dx;
        transformedPoints[i].y += dy;
    }

    return CPolygon2D(transformedPoints);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CPolygon2D& CPolygon2D::operator = (const CPolygon2D& polygon)
{
    copy(polygon);
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators
 
bool CPolygon2D::operator == (const CPolygon2D& polygon) const
{
    AUInt32 count = m_inputPoints.size();
    std::vector<CPoint2f> otherPoints = polygon.getPoints();
    if(count != otherPoints.size())
    {
        return false;
    }
    for(AUInt32 i = 0; i < count; i++)
    {
        if(m_inputPoints[i] != otherPoints[i])
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::operator != (const CPolygon2D& polygon) const
{
    return !(*this == polygon);
}
//---------------------------------------------------------------------------------------

//protected and private methods

void CPolygon2D::init()
{
    m_bIsSelfIntersecting = false;
    m_bIsConcave = false;
    m_centroidPoint = CPoint2f();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::update()
{
    m_validEdges.clear();
    m_allEdges.clear();

    m_centroidPoint = CUtils::getCentroid(m_inputPoints);
    generateAllEdges();
    checkIfSelfIntersecting();
    checkIfConcave();
    findTriangles();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::checkIfSelfIntersecting()
{
    m_bIsSelfIntersecting = false;

    AUInt32 count = m_allEdges.size();
    if(count > 3)
    {
        for(AUInt32 i = 0; i < count; i++)
        {
            for(AUInt32 j =  i + 2; i < count; i++)
            {
                CLineSegment2D::IntersectionClassification c = m_allEdges[i].classifyByIntersection(&m_allEdges[j]);
                if( c == CLineSegment2D::Intersecting || c == CLineSegment2D::IntersectingAtEnds)
                {
                    m_bIsSelfIntersecting = true;
                    return;
                }
            }
        }
    }
    else
    {
        //Triangle case...
        for(AUInt32 i = 0; i < count; i++)
        {
            for(AUInt32 j =  i + 2; i < count; i++)
            {
                CLineSegment2D::IntersectionClassification c = m_allEdges[i].classifyByIntersection(&m_allEdges[j]);
                if( c == CLineSegment2D::Intersecting)
                {
                    m_bIsSelfIntersecting = true;
                    return;
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------------

void CPolygon2D::checkIfConcave()
{
    m_bIsConcave = false;
    AUInt32 hullPointCount = m_convexHull.getHullPoints().size();
    if(hullPointCount > 2)
    {
        if(m_inputPoints.size() > hullPointCount)
        {
            m_bIsConcave = true;
            return;
        }
    }
    m_bIsConcave = false;

    //Other (round-about) way...

    //    AUInt32 count = m_allEdges.size();
    //    for(AUInt32 i = 0; i < count; i++)
    //    {
    //        if(isDividerSegment(m_allEdges[i]))
    //        {
    //            return true;
    //        }
    //    }
}
//---------------------------------------------------------------------------------------

void CPolygon2D::generateAllEdges()
{
    AUInt32 count = m_inputPoints.size();
    for(AUInt32 i = 0; i < count - 1; i++)
    {
        m_allEdges.push_back(CLineSegment2D(m_inputPoints[i], m_inputPoints[i+1]));
    }
    m_allEdges.push_back(CLineSegment2D(m_inputPoints[count - 1], m_inputPoints[0]));
}
//---------------------------------------------------------------------------------------

void CPolygon2D::copy(const CPolygon2D& polygon)
{
    m_inputPoints = polygon.m_inputPoints;
    m_convexHull = polygon.m_convexHull;
    m_interiorTriangles = polygon.m_interiorTriangles;
    m_exteriorTriangles = polygon.m_exteriorTriangles;
    m_validEdges = polygon.m_validEdges;
    m_allEdges = polygon.m_allEdges;
    m_bIsSelfIntersecting = polygon.m_bIsSelfIntersecting;
    m_bIsConcave = polygon.m_bIsConcave;
    m_centroidPoint = polygon.m_centroidPoint;
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findTriangles()
{
    m_interiorTriangles.clear();
    m_exteriorTriangles.clear();

    if(false == m_bIsConcave || m_bIsSelfIntersecting)
    {
        //If the polygon in convex or self-intersecting, then
        //find triangles using convex hull.
        findExteriorTrianglesForConvexCase();
        findInteriorTrianglesForConvexCase();
    }
    else
    {
        findExteriorTrianglesForConcaveCase();
        findInteriorTrianglesForConcaveCase();
    }
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isSegmentCreatingTwoValidChildrenPolygons(const CLineSegment2D& ls) const
{
    if( isPolygonOnBothSidesOfSegment(ls) &&
        isInside(ls) &&
        false == isIntersectingEdges(ls) )
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::isSegmentCreatingTwoValidChildrenPolygons(const AIndex32& ev1, const AIndex32& ev2) const
{
    AInt32 pointCount = (AInt32) m_inputPoints.size();
    if(ev1 >= pointCount || ev2 >= pointCount)
    {
        return false;
    }
    return isSegmentCreatingTwoValidChildrenPolygons(CLineSegment2D(m_inputPoints[ev1], m_inputPoints[ev2]));
}
//---------------------------------------------------------------------------------------

void CPolygon2D::addTrianglesExteriorToConvexHull()
{
    const SCoordinateExtents& extents = m_convexHull.getExtents();
    AReal32 maxExtents = (extents.maxPoint - extents.minPoint).length();
    AReal32 outerBoundaryOffset = POLYGON_EXTERIOR_REGION_SCALE * maxExtents;

    std::vector<CPointTriangle> tris;
    getTrianglesExteriorToConvexHull(outerBoundaryOffset, tris);

    m_exteriorTriangles.insert(m_exteriorTriangles.end(), tris.begin(), tris.end());
}
//---------------------------------------------------------------------------------------

void CPolygon2D::getTrianglesExteriorToConvexHull(const AReal32& outerBoundaryOffset, std::vector<CPointTriangle>& triangles) const
{
    triangles.clear();

    const std::vector<CPoint2f>& hullPoints = m_convexHull.getHullPoints();
    AInt32 hullPointCount = (AInt32) hullPoints.size();
    const CPoint2f& centroid = m_convexHull.getCentroid();
    if(hullPointCount < 3)
    {
        return;
    }

    std::vector<CPoint2f> outerBoundaryPoints;
    outerBoundaryPoints.resize(hullPointCount);

    for(AIndex32 i = 0; i < hullPointCount; i++)
    {
        CVector3f vec = hullPoints[i] - centroid;
        vec.normalize();
        outerBoundaryPoints[i] = hullPoints[i] + (vec * outerBoundaryOffset);
    }

    std::vector<CPoint3f> hpts = CConverter::convertTo3f(hullPoints);
    std::vector<CPoint3f> opts = CConverter::convertTo3f(outerBoundaryPoints);

    for(AIndex32 i = 0; i < hullPointCount - 1; i++)
    {
        triangles.push_back(CPointTriangle(hpts[i], hpts[i + 1], opts[i + 1]));
        triangles.push_back(CPointTriangle(hpts[i], opts[i + 1], opts[i]));
    }
    triangles.push_back(CPointTriangle(hpts[hullPointCount - 1], hpts[0], opts[0]));
    triangles.push_back(CPointTriangle(hpts[hullPointCount - 1], opts[0], opts[hullPointCount - 1]));
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findExteriorTrianglesForConcaveCase()
{
    m_exteriorTriangles.clear();

    const AInt32 pointCount = (AInt32) m_inputPoints.size();
    const std::vector<CPoint2f>& hullPoints = m_convexHull.getHullPoints();
    const AInt32 hullPointCount = (AInt32) hullPoints.size();

    if(hullPointCount == 0)
    {
        return;
    }

    addTrianglesExteriorToConvexHull();

    AIndex32 startIndex = 0;
    for(AIndex32 i = 0; i < pointCount; i++)
    {
        if(m_convexHull.isPointOnHull(m_inputPoints[i]))
        {
            startIndex = i;
            break;
        }
    }

    //Now for each of the pocket between concave polygon and convex hull, get a separate polygon.

    bool hasPocketStarted = false;
    typedef std::vector<CPoint2f> PointsArray;
    typedef std::vector<PointsArray> PointsArrayVector;
    PointsArrayVector polyPointsArrayVector;
    AIndex32 currentIndex = startIndex + 1;


    while(currentIndex != startIndex)
    {
        if(m_convexHull.isPointOnHull(m_inputPoints[currentIndex]))
        {
            if(hasPocketStarted == true)
            {
                AUInt32 arrayCount = polyPointsArrayVector.size();
                polyPointsArrayVector[arrayCount - 1].push_back(m_inputPoints[currentIndex]);
            }
            hasPocketStarted = false;
        }
        else
        {
            if(!hasPocketStarted)
            {
                PointsArray array;
                hasPocketStarted = true;
                if(currentIndex == 0)
                {
                    array.push_back(m_inputPoints[pointCount - 1]);
                }
                else
                {
                    array.push_back(m_inputPoints[currentIndex - 1]);
                }
                array.push_back(m_inputPoints[currentIndex]);
                polyPointsArrayVector.push_back(array);
            }
            else
            {
                AUInt32 arrayCount = polyPointsArrayVector.size();
                polyPointsArrayVector[arrayCount - 1].push_back(m_inputPoints[currentIndex]);
            }
        }
        if(currentIndex == pointCount - 1)
        {
            currentIndex = 0;
        }
        else
        {
            currentIndex++;
        }
    }

    if(hasPocketStarted == true && currentIndex == startIndex)
    {
        AUInt32 vCount = polyPointsArrayVector.size();
        polyPointsArrayVector[vCount-1].push_back(m_inputPoints[currentIndex]);
        hasPocketStarted = false;
    }

    AUInt32 arrayCount = polyPointsArrayVector.size();
    for(AUInt32 i = 0; i < arrayCount; i++)
    {
        CPolygon2D p(polyPointsArrayVector[i]);
        const std::vector<CPointTriangle> tris = p.getInteriorTriangles();
        m_exteriorTriangles.insert(m_exteriorTriangles.end(), tris.begin(), tris.end());
    }
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findExteriorTrianglesForConvexCase()
{
    m_exteriorTriangles.clear();
    addTrianglesExteriorToConvexHull();
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findInteriorTrianglesForConcaveCase()
{
    m_interiorTriangles.clear();
    findInnerTrianglesBySubDivision(m_interiorTriangles);
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findInteriorTrianglesForConvexCase()
{
    m_interiorTriangles.clear();
    const AInt32 pointCount = (AInt32) m_inputPoints.size();

    if(pointCount == 3)
    {
        CPointTriangle t(CConverter::convertTo3f(m_inputPoints[0]),
                    CConverter::convertTo3f(m_inputPoints[1]),
                    CConverter::convertTo3f(m_inputPoints[2]));
        m_interiorTriangles.push_back(t);
        return;
    }
    const std::vector<CPoint2f>& hullPoints = m_convexHull.getHullPoints();
    const AInt32 hullPointCount = (AInt32) hullPoints.size();
    const std::vector<CPoint2f>& pts = hullPoints;

    if(hullPointCount < 3)
    {
        return;
    }

    const CPoint2f& centroid = m_convexHull.getCentroid();
    const CPoint3f centroid3f = CConverter::convertTo3f(centroid);
    AUInt32 numTriangles = hullPointCount;
    m_interiorTriangles.resize(numTriangles);
    AInt32 lastIndex = (hullPointCount - 1);
    for(AInt32 i = 0; i < lastIndex; i++)
    {
        m_interiorTriangles[i] = CPointTriangle(CConverter::convertTo3f(pts[i]),
                                           CConverter::convertTo3f(pts[i + 1]),
                                           centroid3f);
    }
    m_interiorTriangles[lastIndex] = CPointTriangle(CConverter::convertTo3f(pts[lastIndex]),
                                               CConverter::convertTo3f(pts[0]),
                                               centroid3f);
}
//---------------------------------------------------------------------------------------

void CPolygon2D::findInnerTrianglesBySubDivision(std::vector<CPointTriangle>& triangles)
{
    AInt32 pointCount = (AInt32) m_inputPoints.size();
    if(pointCount == 0)
    {
        return;
    }

    const std::vector<CPoint2f>& pts = m_inputPoints;

    if(3 == m_inputPoints.size())
    {
        m_interiorTriangles.push_back(CPointTriangle(CConverter::convertTo3f(pts[0]),
                                                CConverter::convertTo3f(pts[1]),
                                                CConverter::convertTo3f(pts[2])));
        return;
    }

    AIndex32 ev1 = 0;
    AIndex32 ev2 = 0;

    if(!getValidDivider(ev1, ev2) || (ev1 == ev2))
    {
        return;
    }

    std::vector<CPoint2f> firstPointList;
    std::vector<CPoint2f> secondPointList;

    if(ev1 < ev2)
    {
        for(AIndex32 i = ev1; i <= ev2; i++)
        {
          firstPointList.push_back(m_inputPoints[i]);
        }
        for(AIndex32 i = ev2; i < pointCount; i++)
        {
          secondPointList.push_back(m_inputPoints[i]);
        }
        for(AIndex32 i = 0; i <= ev1; i++)
        {
          secondPointList.push_back(m_inputPoints[i]);
        }
    }
    else
    {
        //The starting index is bigger than end index,
        //It must index circulatory counting...
        for(AIndex32 i = ev1; i < pointCount; i++)
        {
          firstPointList.push_back(m_inputPoints[i]);
        }
        for(AIndex32 i = 0; i <= ev2; i++)
        {
          firstPointList.push_back(m_inputPoints[i]);
        }
        for(AIndex32 i = ev2; i <= ev1; i++)
        {
          secondPointList.push_back(m_inputPoints[i]);
        }
    }

    //Keep on dividing till we get a triangle as the minimum polygon...
    if(firstPointList.size() > 0)
    {
        CPolygon2D polygon(firstPointList);
        polygon.findInnerTrianglesBySubDivision(triangles);
    }
    if(secondPointList.size() > 0)
    {
        CPolygon2D polygon(secondPointList);
        polygon.findInnerTrianglesBySubDivision(triangles);
    }
}
//---------------------------------------------------------------------------------------

bool CPolygon2D::getValidDivider(AIndex32& idx1, AIndex32& idx2) const
{
    bool found = false;
    AInt32 pointCount = (AInt32) m_inputPoints.size();

    AIndex32 offset = 2;        //Initially start with 2 vertices which are differing by at-least two index positions
    AIndex32 divisionTry = 0;   //These tries are meant for changing the offsets between two indices in case
                                //they are not found for the current offset between indices.
    AIndex32 ev1 = 0;           //Start with first index as '0'
    AIndex32 ev2 = ev1 + offset;

    if(ev2 >= pointCount)
    {
        return false;
    }

    //This loop tries to find the indices of two vertices of polygon such that
    //they divide the polygon into two valid polygons.
    //The indices of the dividing segment
    while(!found && (divisionTry < 10))
    {
        if(isSegmentCreatingTwoValidChildrenPolygons(ev1, ev2))
        {
            found = true;
            break;
        }
        ev1++;
        ev2++;

        while( (ev2 < pointCount) && (ev1 < pointCount) )
        {
            if(isSegmentCreatingTwoValidChildrenPolygons(ev1, ev2))
            {
                found = true;
                break;
            }
            ev1++;
            ev2++;
        }
        if(found)
        {
            break;
        }

        offset++; //Try changing the offset
        if(offset > (pointCount - 2))
        {
            return false;
        }

        //Now start from fresh but with different offset between indices
        ev1 = 0;
        ev2 = ev1 + offset;
        divisionTry++;
    }

    if(found)
    {
        idx1 = ev1;
        idx2 = ev2;
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

