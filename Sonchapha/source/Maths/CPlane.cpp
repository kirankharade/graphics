#include "CPlane.h"
#include "CUtils.h"
#include "MathsSettings.h"
#include "CLine.h"
#include "CRay.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CPlane::CPlane()
:  m_point(CPoint3f())
  ,m_normal(CVector3f(0, 0, 1))
{
}
//---------------------------------------------------------------------------------------

CPlane::CPlane(const CPoint3f& point, const CVector3f& normal)
:  m_point(point)
  ,m_normal(normal)
{
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

CPlane::CPlane(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D)
:  m_point(CPoint3f())
  ,m_normal(CVector3f(0, 0, 1))
{
    m_normal.x = A;
    m_normal.y = B;
    m_normal.z = C;
    m_normal.normalize();

    CVector3f v = m_normal * (-D);
    m_point.x = v.x;
    m_point.y = v.y;
    m_point.z = v.z;
}
//---------------------------------------------------------------------------------------

CPlane::CPlane(const CPoint3f& p1, const CPoint3f& p2, const CPoint3f& p3)
{
    m_point = p1;
    m_normal = (p2 - p1).cross((p3 - p1));
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

CPlane::CPlane(const CPlane& plane)
{
    m_point = plane.m_point;
    m_normal = plane.m_normal;
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

CPlane::~CPlane()
{
}
//---------------------------------------------------------------------------------------

void CPlane::set(const CPoint3f& point, const CVector3f& normal)
{
    m_point = point;
    m_normal = normal;
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

void CPlane::set(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D)
{
    m_normal.x = A;
    m_normal.y = B;
    m_normal.z = C;
    m_normal.normalize();

    CVector3f v = m_normal * (-D);
    m_point.x = v.x;
    m_point.y = v.y;
    m_point.z = v.z;
}
//---------------------------------------------------------------------------------------

void CPlane::set(const CPoint3f& p1, const CPoint3f& p2, const CPoint3f& p3)
{
    m_point = p1;
    m_normal = (p2 - p1).cross((p3 - p1));
    m_normal.normalize();
}
//---------------------------------------------------------------------------------------

CPoint3f CPlane::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

CVector3f CPlane::getNormal() const
{
    return m_normal;
}
//---------------------------------------------------------------------------------------

void CPlane::getCoefficients(AReal32& A, AReal32& B, AReal32& C, AReal32& D)
{
    A = m_normal.x;
    B = m_normal.y;
    C = m_normal.z;
    D = -((m_point.x * A) + (m_point.y * B) + (m_point.z * C));
}
//---------------------------------------------------------------------------------------

AReal32 CPlane::getCoefficientD() const
{
    return -((m_point.x * m_normal.x) + (m_point.y * m_normal.y) + (m_point.z * m_normal.z));
}
//---------------------------------------------------------------------------------------

bool CPlane::isIntersecting(const CLine*& pLine) const
{
    return pLine->isIntersecting(this);
}
//---------------------------------------------------------------------------------------

bool CPlane::isIntersecting(const CLine*& pLine, CPoint3f& intersectionPoint) const
{
    return pLine->isIntersecting(this, intersectionPoint);
}
//---------------------------------------------------------------------------------------

CPoint3f CPlane::closestPoint(const CPoint3f& point) const
{
    AReal32 distFromPlane = m_normal.dot(point - m_point);
    CPoint3f pt = point + (m_normal * (-distFromPlane));
    return pt;
}
//---------------------------------------------------------------------------------------

AReal32 CPlane::distFromPoint(const CPoint3f& point) const
{
    return m_normal.dot(point - m_point);
}
//---------------------------------------------------------------------------------------

bool CPlane::isParallel(const CPlane& plane) const
{
    CVector3f thisNormal = m_normal;
    CVector3f otherNormal = plane.getNormal();
    thisNormal.normalize();
    otherNormal.normalize();

    return EQ_32(thisNormal.dot(otherNormal), 1.0f);
}
//---------------------------------------------------------------------------------------

EPlaneSideQualification CPlane::getQualification(const CPoint3f& point, const AReal32 tolerance) const
{
    AReal32 dot = (point - m_point).dot(m_normal);

    if(EQT(dot, 0.0f, tolerance))
    {
        return EPlaneSideOnPlane;
    }
    if(dot < 0)
    {
        return EPlaneSideBack;
    }
    return EPlaneSideFront;
}
//---------------------------------------------------------------------------------------

bool CPlane::Intersection(const CPlane& plane1, const CPlane& plane2, CPoint3f& intersectionPoint) const
{
	CPoint3f linePoint;
	CVector3f lineDir;
    bool bStatus = Intersection(plane1, linePoint, lineDir);
    if(false == bStatus)
    {
        return false;
    }
    CLine line(linePoint, lineDir);
    return line.isIntersecting(&plane2, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool CPlane::Intersection(const CPlane& plane, CPoint3f& intersectionPoint, CVector3f& intersectionlineDir) const
{
	const float u = m_normal.magnitude();           //u should be equal to 1.0
	const float dot = m_normal.dot(plane.m_normal);
	const float up = plane.m_normal.magnitude();    //up should be equal to 1.0
	const double det = (u * up) - (dot * dot);
	if (fabs(det) < TOLERANCE_64 )
    {
        return false;
    }
	float r  = getCoefficientD();		
    float rp = plane.getCoefficientD();
	const float idet = (float)1.0 /(float) det;

	const float a = (up * (-r)  + dot * (rp)) * idet;
	const float b = ( u * (-rp) + dot *  (r)) * idet;

	intersectionlineDir = m_normal.cross(plane.m_normal);

	CVector3f v(m_normal * a + plane.m_normal * b);

    intersectionPoint.x = v.x;
    intersectionPoint.y = v.y;
    intersectionPoint.z = v.z;

	return true;
}
//---------------------------------------------------------------------------------------

//void CPlane::getSquarePlaneCorners(const CPoint3f& ptProjected, const AReal32& squareSize, CPoint3f*& corners) const
//{
//
//}
////---------------------------------------------------------------------------------------
//
//void CPlane::getSquarePlaneCorners(const CVector3f& yAxisProjected, const AReal32& squareSize, CPoint3f*& corners) const
//{
//
//}
////---------------------------------------------------------------------------------------

//TO DO:

//bool CPlane::isIntersecting(const CBox* pBox) const
//{
//
//}
//---------------------------------------------------------------------------------------

//bool CPlane::isIntersecting(const CBox* pBox, IntersectionList*& list) const
//{
//
//}
//---------------------------------------------------------------------------------------

//Assignment operator

CPlane& CPlane::operator = (const CPlane& plane)
{
    m_normal = plane.m_normal;
    m_point = plane.m_point;
    m_normal.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CPlane::operator == (const CPlane& plane) const
{
    CVector3f normal = m_normal;
    normal.normalize();

    CVector3f otherNormal = plane.getNormal();
    otherNormal.normalize();

    if(normal == otherNormal)
    {
        if(EPlaneSideOnPlane == getQualification(plane.getPoint(), TOLERANCE_32))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CPlane::operator != (const CPlane& plane) const
{
    return !(*this == plane);
}
//---------------------------------------------------------------------------------------

bool CPlane::operator < (const CPlane& plane) const
{
    if(*this == plane)
    {
        return false;
    }

    CVector3f normal = m_normal;
    normal.normalize();

    CVector3f otherNormal = plane.getNormal();
    otherNormal.normalize();

    return (normal < otherNormal);
}
//---------------------------------------------------------------------------------------
