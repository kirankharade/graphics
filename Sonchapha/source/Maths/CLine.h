//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LINE_H__
#define __C_LINE_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CVector3f.h"
#include "CRay.h"
#include "CLineSegment.h"
#include "CLineSegment2D.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CLine
{
    protected:
        
        CPoint3f    m_point;
        CVector3f   m_direction;

    public:
        
        CLine();

        CLine(const CPoint3f& point, const CVector3f& direction);
    
        CLine(const CPoint3f& firstPoint, const CPoint3f& secondPoint);
    
        CLine(const CLine& line);
    
        CLine(const CRay& ray);

        CLine(const CLineSegment& lineSegment);

        CLine(const CLineSegment2D& lineSegment);

        virtual ~CLine();
    
        void set(const CPoint3f& point, const CVector3f& direction);
    
        void set(const CPoint3f& firstPoint, const CPoint3f& secondPoint);

        CPoint3f getPoint() const;
       
        CVector3f getDirection() const;

        bool isIntersecting(const CPlane* pPlane) const;
       
        bool isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const;

        bool passesThrough(const CPoint3f& point, const AReal32 tolerance = TOLERANCE_32) const;
       
        CPoint3f closestPoint(const CPoint3f& point) const;
       
        AReal32 distFromPoint(const CPoint3f& point) const;

        //Assignment operator

        CLine& operator = (const CLine& line);
    
        CLine& operator = (const CRay& ray);

        CLine& operator = (const CLineSegment& lineSegment);

        CLine& operator = (const CLineSegment2D& lineSegment);

        //Comparison Operators
         
        bool operator == (const CLine& line) const;
    
        bool operator != (const CLine& line) const;
    
        bool operator < (const CLine& line) const;
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LINE_H__
//---------------------------------------------------------------------------------------

