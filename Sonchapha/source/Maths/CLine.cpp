//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CLine.h"
#include "CPlane.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CLine::CLine()
:  m_point(CPoint3f())
 , m_direction(CVector3f(0.0f, 0.0f, 1.0f))
{
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CPoint3f& point, const CVector3f& direction)
:  m_point(point)
 , m_direction(direction)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CPoint3f& start, const CPoint3f& end)
:  m_point(start)
 , m_direction(end - start)
{
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CLine& line)
{
    m_point = line.m_point;
    m_direction = line.m_direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CRay& ray)
{
    m_point = ray.getPoint();
    m_direction = ray.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CLineSegment& lineSegment)
{
    m_point = lineSegment.getFirstPoint();
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::CLine(const CLineSegment2D& lineSegment)
{
    CPoint2f point = lineSegment.getFirstPoint();
    m_point = CPoint3f(point.x, point.y, 0);
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CLine::~CLine()
{
}
//---------------------------------------------------------------------------------------

void CLine::set(const CPoint3f& point, const CVector3f& direction)
{
    m_point = point;
    m_direction = direction;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

void CLine::set(const CPoint3f& start, const CPoint3f& end)
{
    m_point = start;
    m_direction = end - start;
    m_direction.normalize();
}
//---------------------------------------------------------------------------------------

CPoint3f CLine::getPoint() const
{
    return m_point;
}
//---------------------------------------------------------------------------------------

CVector3f CLine::getDirection() const
{
    return m_direction;
}
//---------------------------------------------------------------------------------------

bool CLine::isIntersecting(const CPlane* pPlane) const
{
    if(!pPlane)
    {
        return false;
    }
    CVector3f planeNormal = pPlane->getNormal();
    CVector3f lineDirection = m_direction;
    planeNormal.normalize();
    lineDirection.normalize();

    if(EQ_32(planeNormal.dot(lineDirection), 0.0f))
    {
        //Plane normal and line direction are perpendicular to each other,
        //which means the line is parallel to the plane.
        //Now check whether line lies in plane or not.
        if(EQ_32(pPlane->distFromPoint(m_point), 0.0f))
        {
            //Line lies in plane
            return true;
        }
        return false;
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CLine::isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const
{
    if(!pPlane)
    {
        return false;
    }
    CVector3f lineDirection = m_direction;
    lineDirection.normalize();
    CVector3f planeNormal = pPlane->getNormal();
    planeNormal.normalize();

    const AReal32 sampleDistance = 100.0f;
    CPoint3f anotherLinePt = m_point + (lineDirection * sampleDistance);
    CVector3f vecLine = anotherLinePt - m_point;

    AReal32 d1 = planeNormal.dot(vecLine);
    if(EQ_32(d1, 0.0f))
    {
        //Plane normal and line direction are perpendicular to each other,
        //which means the line is parallel to the plane.
        //Now check whether line lies in plane or not.
        if(EQ_32(pPlane->distFromPoint(m_point), 0.0f))
        {
            //Line lies in plane
            return true;
        }
        return false;
    }

    AReal32 d2 = planeNormal.dot((pPlane->getPoint() - m_point));
    AReal32 u = d2 / d1;
    intersectionPoint = m_point + (vecLine * u);

    return true;
}
//---------------------------------------------------------------------------------------

bool CLine::passesThrough(const CPoint3f& point, const AReal32 tolerance) const
{
    if(point.dist(closestPoint(point)) <= tolerance)
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPoint3f CLine::closestPoint(const CPoint3f& point) const
{
    if(m_direction.isNull())
    {
        return m_point;
    }
    //Get another point on line
    const AReal32 cDist = 100.0f;
    CPoint3f p2 = m_point + (m_direction * cDist);
    CVector3f v = p2 - m_point;
    //Do not check denominator for zero, already done above implicitly.
    AReal32 t = v.dot((point - m_point)) / v.dot(v);
    return CPoint3f(m_point + (v * t));
}
//---------------------------------------------------------------------------------------

AReal32 CLine::distFromPoint(const CPoint3f& point) const
{
    CPoint3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//operators

CLine& CLine::operator = (const CLine& line)
{
    m_point = line.m_point;
    m_direction = line.m_direction;
    return *this;
}
//---------------------------------------------------------------------------------------

CLine& CLine::operator = (const CRay& ray)
{
    m_point = ray.getPoint();
    m_direction = ray.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

CLine& CLine::operator = (const CLineSegment& lineSegment)
{
    m_point = lineSegment.getFirstPoint();
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

CLine& CLine::operator = (const CLineSegment2D& lineSegment)
{
    CPoint2f point = lineSegment.getFirstPoint();
    m_point = CPoint3f(point.x, point.y, 0);
    m_direction = lineSegment.getDirection();
    m_direction.normalize();
    return *this;
}
//---------------------------------------------------------------------------------------

bool CLine::operator == (const CLine& line) const
{
    CVector3f dir = m_direction;
    CVector3f otherDir = line.m_direction;
    dir.normalize();
    otherDir.normalize();

    if(dir == otherDir)
    {
        if(m_point == line.m_point)
        {
           return true;
        }
        if(passesThrough(line.m_point))
        {
           return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLine::operator != (const CLine& line) const
{
    if( !(*this == line) )
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLine::operator < (const CLine& line) const
{
    if(m_point < line.m_point)
    {
        return true;
    }
    if(m_point > line.m_point)
    {
        return false;
    }
    if(m_direction < line.m_direction)
    {
        return true;
    }
    if(m_direction > line.m_direction)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------

