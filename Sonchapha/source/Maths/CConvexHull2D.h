
//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_CONVEX_HULL_2D_H__
#define __C_CONVEX_HULL_2D_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "MathsStructs.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API CConvexHull2D
{
    private:

        CPoint2fList                    m_inputPoints;
        CPoint2fList                    m_hullPoints;
        CPoint2f                        m_centroid;
        SCoordinateExtents              m_extents;

    public:

        CConvexHull2D();

        CConvexHull2D(const std::vector<CPoint2f>& points);

        CConvexHull2D(const CConvexHull2D& hull);

        virtual ~CConvexHull2D();

        void addPoint(const CPoint2f& point);

        void addPoints(const std::vector<CPoint2f>& points);

        const std::vector<CPoint2f>& getInputPoints() const;

        const std::vector<CPoint2f>& getHullPoints() const;

        bool isPointOnHull(const CPoint2f& point) const;

        const CPoint2f& getCentroid() const;

        const SCoordinateExtents& getExtents() const;

        //Assignment operator

        CConvexHull2D& operator = (const CConvexHull2D& hull);

    private:

        void generate();

        void generateHalfHull(std::vector<CPoint2f>& inputPoints);

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_CONVEX_HULL_2D_H__
//---------------------------------------------------------------------------------------
