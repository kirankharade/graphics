//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_POINT_3F_H__
#define __C_POINT_3F_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CVector3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

#define ORIGIN CPoint3f(0, 0, 0)

//---------------------------------------------------------------------------------------

class AN_MATHS_API CPoint3f
{
    public:
        
        union 
        {
            AReal32 v[3];

            struct 
            {
                AReal32 x;
                AReal32 y;
                AReal32 z;
            };
        };


    public:
        
        CPoint3f();
    
        CPoint3f(const AReal32 v[3]);
    
        CPoint3f(const AReal32& a, const AReal32& b, const AReal32& c);
    
        CPoint3f(const CPoint3f& point);
    
        void set(const AReal32& a, const AReal32& b, const AReal32& c);
    
        void set(const CPoint3f& point);
    
        CVector3f positionVector() const;
    
        AReal32 dist(const CPoint3f& point) const;
    
        CVector3f vectorTo(const CPoint3f& point) const;

        CVector3f vectorFrom(const CPoint3f& point) const;

        CPoint3f offsetAlongUnitDir(const CVector3f& dir, const AReal32& offsetDist) const;

        //Index Operators
    
        AReal32& operator [] (const AIndex32& i);
    
        const AReal32& operator [] (const AIndex32& i) const;
    
        //assignment operator

        CPoint3f& operator = (const CPoint3f& point);
    
        //Comparison Operators
         
       bool operator == (const CPoint3f& point) const;
    
        bool operator != (const CPoint3f& point) const;
    
        bool operator < (const CPoint3f& point) const;
        
        bool operator > (const CPoint3f& point) const;
        
        //Summation and subtraction operators
    
        CVector3f operator - (const CPoint3f& point) const;

        CPoint3f operator + (const CPoint3f& point) const;

        CPoint3f operator - (const CVector3f& vec) const;

        CPoint3f operator + (const CVector3f& vec) const;
    
        CPoint3f& operator += (const CVector3f& vec);

        CPoint3f& operator -= (const CVector3f& vec);

        CPoint3f& operator += (const CPoint3f& point);

        //Operators with scalar arguments
    
        CPoint3f operator * (const AReal32& scalar) const;
    
        CPoint3f& operator *= (const AReal32& scalar);
    
        CPoint3f operator / (const AReal32& scalar) const;
    
        CPoint3f& operator /= (const AReal32& scalar);
    
        CPoint3f operator - () const;

};
//---------------------------------------------------------------------------------------
typedef AN_MATHS_API std::vector<CPoint3f>   CPoint3fList;
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_POINT_3F_H__
//---------------------------------------------------------------------------------------

