//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CVector3f.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CVector3f::CVector3f()
{
    x = 0.0f;
    y = 0.0f;
    z = 1.0f;
}
//---------------------------------------------------------------------------------------

CVector3f::CVector3f(const AReal32 v[3])
{
    x = v[0];
    y = v[1];
    z = v[2];
}
//---------------------------------------------------------------------------------------

CVector3f::CVector3f(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

CVector3f::CVector3f(const CVector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}
//---------------------------------------------------------------------------------------

void CVector3f::set(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

void CVector3f::set(const CVector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
}
//---------------------------------------------------------------------------------------

void CVector3f::normalize()
{
    AReal32 mag = sqrt( (x*x) + (y*y) + (z*z) );
    if( !EQ_32(mag, 0.0f) )
    {
        x /= mag;
        y /= mag;
        z /= mag;
    }
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::unitVec() const
{
    CVector3f v;
    AReal32 mag = sqrt( (x*x) + (y*y) + (z*z) );
    if( !EQ_32(mag, 0.0f) )
    {
        v.x = x / mag;
        v.y = y / mag;
        v.z = z / mag;
    }
    return v;
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::magnitude() const
{
    return sqrt( (x*x) + (y*y) + (z*z) );
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::length() const
{
    return sqrt( (x*x) + (y*y) + (z*z) );
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::squareLength() const
{
    return ( (x*x) + (y*y) + (z*z) );
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::cross(const CVector3f& v) const
{
    //   x    y    z
    // v.x  v.y  v.z
    CVector3f c;
    c.x = (y * v.z) - (z * v.y);
    c.y = (z * v.x) - (x * v.z);
    c.z = (x * v.y) - (y * v.x);
    return c;
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::dot(const CVector3f& v) const
{
    return (x * v.x) + (y * v.y) + (z * v.z);
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::reverse() const
{
    return CVector3f(-x, -y, -z);
}
//---------------------------------------------------------------------------------------

bool CVector3f::isNull() const
{
    if(EQ_32(x, 0.0f) && EQ_32(y, 0.0f) && EQ_32(z, 0.0f))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

//operators

AReal32& CVector3f::operator [] (const AIndex32& index)
{
    if(0 == index) return x;
    if(1 == index) return y;
    if(2 == index) return z;
    throw "CVector3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& CVector3f::operator [] (const AIndex32& index) const
{
    if(0 == index) return x;
    if(1 == index) return y;
    if(2 == index) return z;
    throw "CVector3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

CVector3f& CVector3f::operator = (const CVector3f& v)
{
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

bool CVector3f::operator == (const CVector3f& v) const
{
   if( EQ_32(x, v.x) && EQ_32(y, v.y) && EQ_32(z, v.z) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CVector3f::operator != (const CVector3f& v) const
{
   if( (!EQ_32(x, v.x)) || (!EQ_32(y, v.y)) || (!EQ_32(z, v.z)) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CVector3f::operator < (const CVector3f& v) const
{
    if(EQ_32(x, v.x) && EQ_32(y, v.y) && EQ_32(z, v.z))
    {
        return false;
    }
    if(x < v.x)      return true;
    else if(x > v.x) return false;
    else if(y < v.y) return true;
    else if(y > v.y) return false;
    else return (z < v.z);
}
//---------------------------------------------------------------------------------------

bool CVector3f::operator > (const CVector3f& v) const
{
    if( (!(*this < v)) && (*this != v))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::operator + (const CVector3f& v) const
{
    return CVector3f( x + v.x, y + v.y, z + v.z);
}
//---------------------------------------------------------------------------------------

CVector3f& CVector3f::operator += (const CVector3f& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::operator - (const CVector3f& v) const
{
    return CVector3f( x - v.x, y - v.y, z - v.z);
}
//---------------------------------------------------------------------------------------

CVector3f& CVector3f::operator -= (const CVector3f& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::operator * (const AReal32& scalar) const
{
    return CVector3f( x * scalar, y * scalar, z * scalar);
}
//---------------------------------------------------------------------------------------

CVector3f& CVector3f::operator *= (const AReal32& scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::operator / (const AReal32& scalar) const
{
    return CVector3f( x / scalar, y / scalar, z / scalar);
}
//---------------------------------------------------------------------------------------

CVector3f& CVector3f::operator /= (const AReal32& scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::angleDeg(const CVector3f& v) const
{
    CVector3f v1 = *this;
    CVector3f v2 = v;

    v1.normalize();
    v2.normalize();

    AReal32 d = v1.dot(v2);

    if(d < -1.0f)   d = -1.0f;
    if(d >  1.0f)   d =  1.0f;

    AReal32 rad = acos(d);

    return (AReal32) RAD_TO_DEG64(rad);
}
//---------------------------------------------------------------------------------------

AReal32 CVector3f::angleRad(const CVector3f& v) const
{
    CVector3f v1 = *this;
    CVector3f v2 = v;

    v1.normalize();
    v2.normalize();

    AReal32 d = v1.dot(v2);

    if(d < -1.0f)   d = -1.0f;
    if(d >  1.0f)   d =  1.0f;

    return acos(d);
}
//---------------------------------------------------------------------------------------

CVector3f CVector3f::operator - () const
{
    return CVector3f( -x, -y, -z);
}
//---------------------------------------------------------------------------------------

