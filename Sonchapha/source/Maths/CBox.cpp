//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CBox.h"
#include "CLine.h"
#include "CRay.h"
#include "CLineSegment.h"
#include "CPlane.h"
#include "CPointTriangle.h"
#include "IntersectionInfo.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------
const static AReal32 DEFAULT_SIZE = 10.0f;
//---------------------------------------------------------------------------------------

CBox::CBox()
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(DEFAULT_SIZE)
 ,m_height(DEFAULT_SIZE)
 ,m_depth(DEFAULT_SIZE)
{
    m_zAxis = CVector3f(0, 0, 1);
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
     const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(origin)
 ,m_xAxis(xDirection)
 ,m_yAxis(yDirection)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(origin)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

CBox::CBox(const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size)
: m_origin(origin)
 ,m_xAxis(xDirection)
 ,m_yAxis(yDirection)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const AReal32& size)
: m_origin(origin)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

CBox::CBox(const AReal32& size)
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
    m_zAxis = Z_AXIS;
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CBox& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;
    m_zAxis = box.m_zAxis;
}
//---------------------------------------------------------------------------------------

CBox::~CBox()
{
}
//---------------------------------------------------------------------------------------

void CBox::set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
         const AReal32& width, const AReal32& height, const AReal32& depth)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = width;
    m_height = height;
    m_depth = depth;

    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = size;
    m_height = size;
    m_depth = size;

    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setOrigin(const CPoint3f& origin)
{
    m_origin = origin;
}
//---------------------------------------------------------------------------------------

void CBox::setDirections(const CVector3f& xDirection, const CVector3f& yDirection)
{
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_xAxis.normalize();
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setXDirection(const CVector3f& xDirection)
{
    m_xAxis = xDirection;
    m_xAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setYDirection(const CVector3f& yDirection)
{
    m_yAxis = yDirection;
    m_yAxis.normalize();
    m_zAxis = m_xAxis.cross(m_yAxis);
    m_zAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setSize(const AReal32& size)
{
    m_width = size;
    m_height = size;
    m_depth = size;
}
//---------------------------------------------------------------------------------------

void CBox::setWidth(const AReal32& width)
{
    m_width = width;
}
//---------------------------------------------------------------------------------------

void CBox::setHeight(const AReal32& height)
{
    m_height = height;
}
//---------------------------------------------------------------------------------------

void CBox::setDepth(const AReal32& depth)
{
    m_depth = depth;
}
//---------------------------------------------------------------------------------------

CPoint3f CBox::getOrigin() const
{
    return m_origin;
}
//---------------------------------------------------------------------------------------

CVector3f CBox::getXDirection() const
{
    return m_xAxis;
}
//---------------------------------------------------------------------------------------

CVector3f CBox::getYDirection() const
{
    return m_yAxis;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getWidth() const
{
    return m_width;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getHeight() const
{
    return m_height;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getDepth() const
{
    return m_depth;
}
//---------------------------------------------------------------------------------------

CPoint3f CBox::getCentre() const
{
    CPoint3f oppCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    return ((m_origin + oppCorner) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getVolume() const
{
    return (m_width * m_height * m_depth);
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getSurfaceArea() const
{
    return (2.0f * ((m_width * m_height) + (m_height * m_depth) + (m_width * m_depth)));
}
//---------------------------------------------------------------------------------------

CPoint3f* CBox::getCorners() const
{
    CPoint3f* corners = new CPoint3f[8];

    //XY-plane through origin
    corners[0] = m_origin;
    corners[1] = m_origin + (m_xAxis * m_width);
    corners[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    corners[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    CVector3f offset = m_zAxis * m_depth;

    corners[4] = corners[0] + offset;
    corners[5] = corners[1] + offset;
    corners[6] = corners[2] + offset;
    corners[7] = corners[3] + offset;

    return corners;
}
//---------------------------------------------------------------------------------------

CLineSegment* CBox::getEdges() const
{
    CPoint3f c[8];

    //XY-plane through origin
    c[0] = m_origin;
    c[1] = m_origin + (m_xAxis * m_width);
    c[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    c[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    CVector3f offset = m_zAxis * m_depth;

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    CLineSegment* edges = new CLineSegment[12];

    edges[0]  = CLineSegment(c[0], c[1]);
    edges[1]  = CLineSegment(c[1], c[2]);
    edges[2]  = CLineSegment(c[2], c[3]);
    edges[3]  = CLineSegment(c[3], c[0]);

    edges[4]  = CLineSegment(c[4], c[5]);
    edges[5]  = CLineSegment(c[5], c[6]);
    edges[6]  = CLineSegment(c[6], c[7]);
    edges[7]  = CLineSegment(c[7], c[4]);

    edges[8]  = CLineSegment(c[0], c[4]);
    edges[9]  = CLineSegment(c[1], c[5]);
    edges[10] = CLineSegment(c[2], c[6]);
    edges[11] = CLineSegment(c[3], c[7]);

    return edges;
}
//---------------------------------------------------------------------------------------

CPointTriangle* CBox::getFaceTriangles() const
{
    CPoint3f c[8];

    //XY-plane through origin
    c[0] = m_origin;
    c[1] = m_origin + (m_xAxis * m_width);
    c[2] = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height);
    c[3] = m_origin + (m_yAxis * m_height);

    //Plane parallel to XY-plane through origin at offset
    CVector3f offset = m_zAxis * m_depth;

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    CPointTriangle* tris = new CPointTriangle[12];

    //Face 1 - 0, 1, 2, 3
    tris[0]  = CPointTriangle(c[0], c[1], c[2]);
    tris[1]  = CPointTriangle(c[2], c[3], c[0]);

    //Face 2 - 4, 5, 6, 7
    tris[2]  = CPointTriangle(c[6], c[5], c[4]);
    tris[3]  = CPointTriangle(c[4], c[7], c[6]);

    //Face 3 - 0, 1, 5, 4
    tris[4]  = CPointTriangle(c[5], c[1], c[0]);
    tris[5]  = CPointTriangle(c[0], c[4], c[5]);

    //Face 4 - 1, 2, 6, 5
    tris[6]  = CPointTriangle(c[6], c[2], c[1]);
    tris[7]  = CPointTriangle(c[1], c[5], c[6]);

    //Face 5 - 2, 3, 7, 6
    tris[8]  = CPointTriangle(c[7], c[3], c[2]);
    tris[9]  = CPointTriangle(c[2], c[6], c[7]);

    //Face 6 - 3, 0, 4, 7
    tris[10] = CPointTriangle(c[4], c[0], c[3]);
    tris[11] = CPointTriangle(c[3], c[7], c[4]);

    return tris;
}
//---------------------------------------------------------------------------------------

//! The normals of these planes pointing outwards of the box

CPlane* An::Maths::CBox::getPlanes() const
{
    CPlane* planes = new CPlane[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);

    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    return planes;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CPlane* pPlane) const
{
    if(!pPlane)
    {
        return false;
    }
    CLineSegment* pEdges = getEdges();
    if(!pEdges)
    {
        return false;
    }
    bool bStatus = false;
    for(AIndex16 i = 0; i < 12; i++)
    {
        bStatus = pEdges->isIntersecting(pPlane);
        if(bStatus)
        {
            break;
        }
    }
    SAFE_ARRAY_CLEANUP(pEdges);
    return bStatus;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CBox::getIntersections(const CPlane* pPlane) const
{
    if(!pPlane)
    {
        return NULL;
    }
    CLineSegment* pEdges = getEdges();
    if(!pEdges)
    {
        return NULL;
    }

    std::vector<SPointIntersection> hits;
    CPoint3f hitPoint;

    for(AIndex16 i = 0; i < 12; i++)
    {
        if(pEdges->isIntersecting(pPlane, hitPoint))
        {
            SPointIntersection spi;
            spi.m_hitPrimitiveIndex = i;
            spi.m_intersectionPoint = hitPoint;
            hits.push_back(spi);
        }
    }
    SAFE_ARRAY_CLEANUP(pEdges);

    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLine* pLine) const
{
    if(!pLine)
    {
        return false;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    AReal32 tolernace = (AReal32) (5.0 * TOLERANCE_32);

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLine->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CBox::getIntersections(const CLine* pLine) const
{
    if(!pLine)
    {
        return NULL;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLine->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLineSegment* pLineSegment) const
{
    if(!pLineSegment)
    {
        return false;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    AReal32 tolernace = (AReal32) (5.0 * TOLERANCE_32);

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLineSegment->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CBox::getIntersections(const CLineSegment* pLineSegment) const
{
    if(!pLineSegment)
    {
        return NULL;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLineSegment->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CRay* pRay) const
{
    if(!pRay)
    {
        return false;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    AReal32 tolernace = (AReal32) (5.0 * TOLERANCE_32);

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pRay->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, tolernace))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CBox::getIntersections(const CRay* pRay) const
{
    if(!pRay)
    {
        return NULL;
    }

    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);
    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    CPoint3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pRay->isIntersecting(&planes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CBox::isInside(const CPoint3f& point, const AReal32& tolerance) const
{
    CPlane planes[6];
    CPoint3f opCorner = m_origin + (m_xAxis * m_width) + (m_yAxis * m_height) + (m_zAxis * m_depth);

    planes[0] = CPlane(m_origin, -m_xAxis);
    planes[1] = CPlane(m_origin, -m_yAxis);
    planes[2] = CPlane(m_origin, -m_zAxis);
    planes[3] = CPlane(opCorner,  m_xAxis);
    planes[4] = CPlane(opCorner,  m_yAxis);
    planes[5] = CPlane(opCorner,  m_zAxis);

    for(int i = 0; i < 6; i++)
    {
        if(EPlaneSideBack != planes[i].getQualification(point, tolerance))
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

CBox* CBox::transform(const CMatrix4f& matrix) const
{
    CPoint3f boxOrigin = matrix.transform(m_origin);
    CVector3f xAxis = matrix.rotate(m_xAxis);
    CVector3f yAxis = matrix.rotate(m_yAxis);
    return new CBox(boxOrigin, xAxis, yAxis, m_width, m_height, m_depth);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CBox& CBox::operator = (const CBox& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;

    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CBox::operator == (const CBox& box) const
{
    if((m_origin == box.m_origin) &&
       (m_xAxis == box.m_xAxis) &&
       (m_yAxis == box.m_yAxis) &&
       EQ_32(m_width, box.m_width) &&
       EQ_32(m_height, box.m_height) &&
       EQ_32(m_depth, box.m_depth))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::operator != (const CBox& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

