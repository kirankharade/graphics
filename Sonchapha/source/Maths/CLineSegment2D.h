//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LINE_SEGMENT2D_H__
#define __C_LINE_SEGMENT2D_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CVector3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class CLine;
class CLineSegment;
class CRay;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CLineSegment2D
{
    public:

        enum IntersectionClassification
        {
            Intersecting,
            IntersectingAtEnds,
            CoIncident,
            NonCoIncidentParallel,
            NonIntersecting,
            undefinedIntersection
        };

    protected:

        CPoint2f    m_firstPoint;
        CPoint2f    m_secondPoint;

    public:

        CLineSegment2D();

        CLineSegment2D(const CPoint2f& firstPoint, const CVector3f& direction, const AReal32& length);

        CLineSegment2D(const CPoint2f& firstPoint, const CPoint2f& secondPoint);

        CLineSegment2D(const CLineSegment2D& lineSegment);

        CLineSegment2D(const CLineSegment& lineSegment);

        virtual ~CLineSegment2D();

        void set(const CPoint2f& firstPoint, const CVector3f& direction, const AReal32& length);

        virtual void set(const CPoint2f& firstPoint, const CPoint2f& secondPoint);

        CPoint2f getFirstPoint() const;

        CPoint2f getSecondPoint() const;

        CVector3f getDirection() const;

        AReal32 getLength() const;

        IntersectionClassification classifyByIntersection(const CLineSegment2D* lineSegment, CPoint2f& intersectionPoint) const;

        IntersectionClassification classifyByIntersection(const CLineSegment2D* lineSegment) const;

        IntersectionClassification classifyByIntersection(const CLine* line, CPoint2f& intersectionPoint) const;

        IntersectionClassification classifyByIntersection(const CLine* line) const;

        IntersectionClassification classifyByIntersection(const CRay* ray, CPoint2f& intersectionPoint) const;

        IntersectionClassification classifyByIntersection(const CRay* ray) const;

        bool passesThrough(const CPoint2f& point, const AReal32 tolerance = TOLERANCE_32) const;

        CPoint2f closestPoint(const CPoint2f& point) const;

        AReal32 distFromPoint(const CPoint2f& point) const;

        //Assignment operator

        CLineSegment2D& operator = (const CLineSegment2D& lineSegment);

        //Comparison Operators

        bool operator == (const CLineSegment2D& lineSegment) const;

        bool operator != (const CLineSegment2D& lineSegment) const;

        bool operator < (const CLineSegment2D& lineSegment) const;

};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<CLineSegment2D>;
template class AN_MATHS_API std::vector<CLineSegment2D>;
#endif

typedef std::vector<CLineSegment2D> CLineSegment2DList;
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LINE_SEGMENT2D_H__
//---------------------------------------------------------------------------------------

