//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATHS_ENUMS_H__
#define __AN_MATHS_ENUMS_H__
//---------------------------------------------------------------------------------------
#include "MathsIncludes.h"
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

enum AN_MATHS_API ECoordSystemHandedness
{
    ECoordSystemRightHanded = 0,
    ECoordSystemLeftHanded
};

enum AN_MATHS_API EScreenConventionPosYAxis
{
    EScreenConventionPosYAxisUpPositive = 0,
    EScreenConventionPosYAxisUpNegative
};

enum AN_MATHS_API EPlaneSideQualification
{
    EPlaneSideFront = 0,
    EPlaneSideOnPlane,
    EPlaneSideBack,
};

//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_MATHS_ENUMS_H__
//---------------------------------------------------------------------------------------
