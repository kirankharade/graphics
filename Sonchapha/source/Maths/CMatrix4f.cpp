//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CMatrix4f.h"
#include <cmath>
#include <iostream>
#include <cstring>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CMatrix4f::CMatrix4f(const ABool& bSetIdentity)
{
    memset(m, 0, sizeof(AReal32)*16);
    if(bSetIdentity)
    {
        e[0][0] = 1.0f;
        e[1][1] = 1.0f;
        e[2][2] = 1.0f;
        e[3][3] = 1.0f;
    }
    m_scale.set(1.0f, 1.0f, 1.0f);
    m_bAbsoluteScaleRecoverable = true;
}
//---------------------------------------------------------------------------------------

CMatrix4f::CMatrix4f(const AReal32* v, const bool bScaleRecoverable)
{
    memcpy(m, v, sizeof(AReal32)*16);
    m_scale.set(1.0f, 1.0f, 1.0f);
    m_bAbsoluteScaleRecoverable = bScaleRecoverable;
}
//---------------------------------------------------------------------------------------

CMatrix4f::CMatrix4f(const CMatrix4f& matrix)
{
    memcpy(m, matrix.m, sizeof(AReal32)*16);
    m_scale.set(matrix.m_scale.x, matrix.m_scale.y, matrix.m_scale.z);
    m_bAbsoluteScaleRecoverable = true;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::set(const AReal32* v, const bool bScaleRecoverable)
{
    memcpy(m, v, sizeof(AReal32)*16);
    m_scale.set(1.0f, 1.0f, 1.0f);
    m_bAbsoluteScaleRecoverable = bScaleRecoverable;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::set(const CMatrix4f& matrix)
{
    memcpy(m, matrix.m, sizeof(AReal32)*16);
    m_scale.set(matrix.m_scale.x, matrix.m_scale.y, matrix.m_scale.z);
    m_bAbsoluteScaleRecoverable = true;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setIdentity()
{
    memset(m, 0, sizeof(AReal32)*16);
    e[0][0] = 1.0f;
    e[1][1] = 1.0f;
    e[2][2] = 1.0f;
    e[3][3] = 1.0f;
    m_scale.set(1.0f, 1.0f, 1.0f);
    m_bAbsoluteScaleRecoverable = true;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setZero()
{
    memset(m, 0, sizeof(AReal32)*16);
    m_scale.set(1.0f, 1.0f, 1.0f);
    m_bAbsoluteScaleRecoverable = true;
}
//---------------------------------------------------------------------------------------

bool CMatrix4f::isIdentity() const
{
    CMatrix4f identity(true);
    return (identity == *this);
}
//---------------------------------------------------------------------------------------

bool CMatrix4f::isZero() const
{
    for(AInt32 i = 0; i < 16; i++)
    {
        if( ! EQ_32(m[i], 0.0f) )
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

const AReal32* CMatrix4f::ptr() const
{
    return m;
}
//---------------------------------------------------------------------------------------

AReal32* CMatrix4f::ptr()
{
    return m;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::inverse() const
{
    //Calculate the determinant first...

    AReal32 A0 = (m[0] * m[5]) - (m[1] * m[4]);
    AReal32 A1 = (m[0] * m[6]) - (m[2] * m[4]);
    AReal32 A2 = (m[0] * m[7]) - (m[3] * m[4]);
    AReal32 A3 = (m[1] * m[6]) - (m[2] * m[5]);
    AReal32 A4 = (m[1] * m[7]) - (m[3] * m[5]);
    AReal32 A5 = (m[2] * m[7]) - (m[3] * m[6]);

    AReal32 B0 = (m[8] * m[13]) - (m[9] * m[12]);
    AReal32 B1 = (m[8] * m[14]) - (m[10] * m[12]);
    AReal32 B2 = (m[8] * m[15]) - (m[11] * m[12]);
    AReal32 B3 = (m[9] * m[14]) - (m[10] * m[13]);
    AReal32 B4 = (m[9] * m[15]) - (m[11] * m[13]);
    AReal32 B5 = (m[10] * m[15]) - (m[11] * m[14]);

    AReal32 det = (A0 * B5) - (A1 * B4) + (A2 * B3) + (A3 * B2) - (A4 * B1) + (A5 * B0);

    if(LTEQ_32(det, 0.0f))
    {
        return CMatrix4f();
    }

    AReal32 reciprocal_det = 1.0f / det;

    CMatrix4f result;

    result.m[0] =    (m[5] * B5)  - (m[6] * B4)  + (m[7] * B3);
    result.m[1] = -  (m[1] * B5)  + (m[2] * B4)  - (m[3] * B3);
    result.m[2] =    (m[13] * A5) - (m[14] * A4) + (m[15] * A3);
    result.m[3] = -  (m[9] * A5)  + (m[10] * A4) - (m[11] * A3);
    result.m[4] = -  (m[4] * B5)  + (m[6] * B2)  - (m[7] * B1);
    result.m[5] =    (m[0] * B5)  - (m[2] * B2)  + (m[3] * B1);
    result.m[6] = -  (m[12] * A5) + (m[14] * A2) - (m[15] * A1);
    result.m[7] =    (m[8] * A5)  - (m[10] * A2) + (m[11] * A1);
    result.m[8] =    (m[4] * B4)  - (m[5] * B2)  + (m[7] * B0);
    result.m[9] = -  (m[0] * B4)  + (m[1] * B2)  - (m[3] * B0);
    result.m[10] =   (m[12] * A4) - (m[13] * A2) + (m[15] * A0);
    result.m[11] = - (m[8] * A4)  + (m[9] * A2)  - (m[11] * A0);
    result.m[12] = - (m[4] * B3)  + (m[5] * B1)  - (m[6] * B0);
    result.m[13] =   (m[0] * B3)  - (m[1] * B1)  + (m[2] * B0);
    result.m[14] = - (m[12] * A3) + (m[13] * A1) - (m[14] * A0);
    result.m[15] =   (m[8] * A3)  - (m[9] * A1)  + (m[10] * A0);

    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] *= reciprocal_det;
    }

    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::transpose() const
{
    CMatrix4f result(false);

    AInt32 i = 0, j = 0;

    for(i = 0; i < 4; i++)
    {
        for(j = 0; j < 4; j++)
        {
            result.e[j][i] = e[i][j];
        }
    }
    return result;
}
//---------------------------------------------------------------------------------------

AReal32 CMatrix4f::determinent() const
{
    AReal32 A0 = (m[0] * m[5]) - (m[1] * m[4]);
    AReal32 A1 = (m[0] * m[6]) - (m[2] * m[4]);
    AReal32 A2 = (m[0] * m[7]) - (m[3] * m[4]);
    AReal32 A3 = (m[1] * m[6]) - (m[2] * m[5]);
    AReal32 A4 = (m[1] * m[7]) - (m[3] * m[5]);
    AReal32 A5 = (m[2] * m[7]) - (m[3] * m[6]);

    AReal32 B0 = (m[8] * m[13]) - (m[9] * m[12]);
    AReal32 B1 = (m[8] * m[14]) - (m[10] * m[12]);
    AReal32 B2 = (m[8] * m[15]) - (m[11] * m[12]);
    AReal32 B3 = (m[9] * m[14]) - (m[10] * m[13]);
    AReal32 B4 = (m[9] * m[15]) - (m[11] * m[13]);
    AReal32 B5 = (m[10] * m[15]) - (m[11] * m[14]);

    AReal32 det = (A0 * B5) - (A1 * B4) + (A2 * B3) + (A3 * B2) - (A4 * B1) + (A5 * B0);

    return det;
}
//---------------------------------------------------------------------------------------

//Rotation / scaling / translation

void CMatrix4f::resetRotation()
{
    e[0][0] = m_scale.x;
    e[0][1] = 0.0f;
    e[0][2] = 0.0f;

    e[1][0] = 0.0f;
    e[1][1] = m_scale.y;
    e[1][2] = 0.0f;

    e[2][0] = 0.0f;
    e[2][1] = 0.0f;
    e[2][2] = m_scale.z;
}
//---------------------------------------------------------------------------------------

CVector3f CMatrix4f::getTranslation() const
{
    CVector3f t(m[3], m[7], m[11]);
    return t;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::getRotationMatrix() const
{
    CMatrix4f r(true);

    r.e[0][0] = e[0][0] / m_scale.x;
    r.e[0][1] = e[0][1];
    r.e[0][2] = e[0][2];

    r.e[1][0] = e[1][0];
    r.e[1][1] = e[1][1] / m_scale.y;
    r.e[1][2] = e[1][2];

    r.e[2][0] = e[2][0];
    r.e[2][1] = e[2][1];
    r.e[2][2] = e[2][2] / m_scale.z;

    return r;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setRotationDeg(const AReal32& dx, const AReal32& dy, const AReal32& dz)
{
    setIdentity();

    AReal32 rx = (AReal32)DEG_TO_RAD64(dx);
    AReal32 ry = (AReal32)DEG_TO_RAD64(dy);
    AReal32 rz = (AReal32)DEG_TO_RAD64(dz);

    AReal32 cx = cos(rx);
    AReal32 sx = sin(rx);
    AReal32 cy = cos(ry);
    AReal32 sy = sin(ry);
    AReal32 cz = cos(rz);
    AReal32 sz = sin(rz);

    m[0] = ( cy * cz );
    m[1] = ( sx * sy * cz ) - ( cx * sz );
    m[2] = ( cx * sy * cz ) + ( sx * sz );
    m[4] = ( cy * sz );
    m[5] = ( sx * sy * sz ) + ( cx * cz );
    m[6] = ( cx * sy * sz ) - ( sx * cz );
    m[8] = ( -sy );
    m[9] = ( sx * cy );
    m[10] = ( cx * cy );
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setRotationDeg(const CVector3f& r)
{
    setRotationDeg(r.x, r.y, r.z);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setXRotationDeg(const AReal32& dx)
{
    resetRotation();
    AReal32 rx = (AReal32)DEG_TO_RAD64(dx);
    m[5] = cos(rx);
    m[6] = -sin(rx);
    m[9] = sin(rx);
    m[10] = cos(rx);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setYRotationDeg(const AReal32& dy)
{
    resetRotation();
    AReal32 ry = (AReal32)DEG_TO_RAD64(dy);
    m[0] = cos(ry);
    m[2] = sin(ry);
    m[8] = -sin(ry);
    m[10] = cos(ry);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setZRotationDeg(const AReal32& dz)
{
    resetRotation();
    AReal32 rz = (AReal32)DEG_TO_RAD64(dz);
    m[0] = cos(rz);
    m[1] = -sin(rz);
    m[4] = sin(rz);
    m[5] = cos(rz);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::resetScaling()
{
    e[0][0] /= m_scale.x;
    e[1][1] /= m_scale.y;
    e[2][2] /= m_scale.z;
    m_scale.set(1.0f, 1.0f, 1.0f);
}
//---------------------------------------------------------------------------------------

CVector3f CMatrix4f::getScaleValues()
{
    CVector3f scale(m_scale);
    if(false == m_bAbsoluteScaleRecoverable)
    {
        scale = recoverScale();
    }
    return scale;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setScaleValues(const AReal32& sx, const AReal32& sy, const AReal32& sz)
{
    if(EQ_32(sx, 0) || EQ_32(sy, 0) || EQ_32(sz, 0))
    {
        throw "CMatrix4f::setScaleValues : One or more scale values are zero.";
    }
    e[0][0] *= (sx / m_scale.x);
    e[1][1] *= (sy / m_scale.y);
    e[2][2] *= (sz / m_scale.z);
    m_scale.set(sx, sy, sz);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setScaleValues(const CVector3f& s)
{
    if(EQ_32(s.x, 0) || EQ_32(s.y, 0) || EQ_32(s.z, 0))
    {
        throw "CMatrix4f::setScaleValues : One or more scale values are zero.";
    }
    e[0][0] *= (s.x / m_scale.x);
    e[1][1] *= (s.y / m_scale.y);
    e[2][2] *= (s.z / m_scale.z);
    m_scale.set(s.x, s.y, s.z);
}
//---------------------------------------------------------------------------------------

void CMatrix4f::scaleMatrix(const AReal32& sx, const AReal32& sy, const AReal32& sz)
{
    if(EQ_32(sx, 0) || EQ_32(sy, 0) || EQ_32(sz, 0))
    {
        throw "CMatrix4f::scaleMatrix : One or more scale values are zero.";
    }
    e[0][0] *= sx;
    e[1][1] *= sy;
    e[2][2] *= sz;

    m_scale.x *= sx;
    m_scale.y *= sy;
    m_scale.z *= sz;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::scaleMatrix(const CVector3f& s)
{
    if(EQ_32(s.x, 0) || EQ_32(s.y, 0) || EQ_32(s.z, 0))
    {
        throw "CMatrix4f::scaleMatrix : One or more scale values are zero.";
    }
    e[0][0] *= s.x;
    e[1][1] *= s.y;
    e[2][2] *= s.z;

    m_scale.x *= s.x;
    m_scale.y *= s.y;
    m_scale.z *= s.z;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::scaleMatrixX(const AReal32& sx)
{
    if(EQ_32(sx, 0))
    {
        throw "CMatrix4f::scaleMatrixX : X scale value is zero.";
    }
    e[0][0] *= sx;
    m_scale.x *= sx;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::scaleMatrixY(const AReal32& sy)
{
    if(EQ_32(sy, 0))
    {
        throw "CMatrix4f::scaleMatrixY : Y scale value is zero.";
    }
    e[1][1] *= sy;
    m_scale.y *= sy;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::scaleMatrixZ(const AReal32& sz)
{
    if(EQ_32(sz, 0))
    {
        throw "CMatrix4f::scaleMatrixZ : Z scale value is zero.";
    }
    e[2][2] *= sz;
    m_scale.z *= sz;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::resetTranslation()
{
    m[3] = 0.0f;
    m[7] = 0.0f;
    m[11] = 0.0f;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz)
{
    m[3] = tx;
    m[7] = ty;
    m[11] = tz;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setTranslation(const CVector3f& t)
{
    m[3] = t.x;
    m[7] = t.y;
    m[11] = t.z;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setXTranslation(const AReal32& tx)
{
    m[3] = tx;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setYTranslation(const AReal32& ty)
{
    m[7] = ty;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::setZTranslation(const AReal32& tz)
{
    m[11] = tz;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::addTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz)
{
    m[3] += tx;
    m[7] += ty;
    m[11] += tz;
}
//---------------------------------------------------------------------------------------

void CMatrix4f::addTranslation(const CVector3f& t)
{
    m[3] += t.x;
    m[7] += t.y;
    m[11] += t.z;
}
//---------------------------------------------------------------------------------------

//Courtsey Reference: http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/ArbitraryAxisRotation.html

CMatrix4f CMatrix4f::RotateDegAboutVectorThroughOrigin(const CVector3f& vector, const AReal32& angleDeg)
{
    CVector3f tempVec = vector;
    tempVec.normalize();

    setIdentity();

    AReal32 angle = (AReal32) DEG_TO_RAD64(angleDeg);

    AReal32 u = tempVec.x;
    AReal32 v = tempVec.y;
    AReal32 w = tempVec.z;

    AReal32 c = cos(angle);
    AReal32 s = sin(angle);
    AReal32 p = 1.0f - cos(angle);

    e[0][0] = (p * u * u) + c;
    e[0][1] = (p * u * v) + (s * w);
    e[0][2] = (p * u * w) - (s * v);

    e[1][0] = (p * u * v) - (s * w);
    e[1][1] = (p * v * v) + c;
    e[1][2] = (p * v * w) + (s * u);

    e[2][0] = (p * u * w) + (s * v);
    e[2][1] = (p * v * w) - (s * u);
    e[2][2] = (p * w * w) + c;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::RotateDegAboutVectorThroughPoint(const CVector3f& vector, const CPoint3f& point, const AReal32& angleDeg)
{
    CVector3f tempVec = vector;
    tempVec.normalize();

    setIdentity();

    AReal32 angle = (AReal32) DEG_TO_RAD64(angleDeg);

    AReal32 u = tempVec.x;
    AReal32 v = tempVec.y;
    AReal32 w = tempVec.z;

    AReal32 c = cos(angle);
    AReal32 s = sin(angle);
    AReal32 p = 1.0f - cos(angle);

    AReal32 x = point.x;
    AReal32 y = point.y;
    AReal32 z = point.z;

    AReal32 Usq = u * u;
    AReal32 Vsq = v * v;
    AReal32 Wsq = w * w;

    AReal32 K = Usq + Vsq;
    AReal32 I = Vsq + Wsq;
    AReal32 J = Wsq + Usq;

    //Rotation terms
    e[0][0] = Usq + (I * c);
    e[0][1] = (u * v * p) - (w * s);
    e[0][2] = (u * w * p) + (v * s);

    e[1][0] = (u * v * p) + (w * s);
    e[1][1] = Vsq + (J * c);
    e[1][2] = (v * w * p) - (u * s);

    e[2][0] = (u * w * p) - (v * s);
    e[2][1] = (v * w * p) + (u * s);
    e[2][2] = Wsq + (K * c);

    //Translation terms
    e[0][3] = (x * I) - (u * ((y * v) + (z * w))) + (( (u * ((y * v) + (z * w))) - (x * I) ) * c) + (((y * w) - (z * v)) * s);
    e[1][3] = (y * J) - (v * ((x * u) + (z * w))) + (( (v * ((x * u) + (z * w))) - (y * J) ) * c) + (((z * u) - (x * w)) * s);
    e[2][3] = (z * K) - (w * ((x * u) + (y * v))) + (( (w * ((x * u) + (y * v))) - (z * K) ) * c) + (((x * v) - (y * u)) * s);

    return *this;
}
//---------------------------------------------------------------------------------------

//Camera related transformation matrix

CMatrix4f CMatrix4f::perspectiveProjectionFieldOfViewRHS(const AReal32& fovDeg, const AReal32& aspectRatio, 
                                                         const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewRHS: Unable to find perspective projection. Near and Far values are same.";
    }
    if(EQ_32(aspectRatio, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewRHS: Unable to find perspective projection. Aspect ratio is zero.";
    }

    AReal64 fovRad = DEG_TO_RAD64(fovDeg);
    AReal32 height = (AReal32) (1.0f / (tan(0.5f * fovRad)));
    AReal32 width = height / aspectRatio;

    if(EQ_32(height, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewRHS: Unable to find perspective projection. Height of view volume is zero.";
    }

    e[0][0] = width;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = height;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = far /(near - far);// DX. //e[2][2] = (far + near) /(near - far); // OpenGL
    e[2][3] = (near * far)/(near - far); // DX. //e[2][3] = (2.0f * near * far) / (near - far)); // OpenGL

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = -1;
    e[3][3] = 0;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::perspectiveProjectionFieldOfViewLHS(const AReal32& fovDeg, const AReal32& aspectRatio, const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewLHS: Unable to find perspective projection. Near and Far values are same.";
    }
    if(EQ_32(aspectRatio, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewLHS: Unable to find perspective projection. Aspect ratio is zero.";
    }

    AReal64 fovRad = DEG_TO_RAD64(fovDeg);
    AReal32 height = (AReal32) (1.0f / (tan(0.5f * fovRad)));
    AReal32 width = height / aspectRatio;

    if(EQ_32(height, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionFieldOfViewLHS: Unable to find perspective projection. Height of view volume is zero.";
    }

    e[0][0] = width;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = height;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = far /(near - far);// DX. //e[2][2] = (far + near) /(near - far); // OpenGL
    e[2][3] = -((near * far)/(near - far)); // DX. //e[2][3] = (-2.0f * near * far) / (near - far)); // OpenGL

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = 1.0f;
    e[3][3] = 0;

    return *this;
}
//---------------------------------------------------------------------------------------

//Imp-KK: Check this. Is this the average view volume height and width for perspective?
//If it is problematic, uncomment it.
CMatrix4f CMatrix4f::perspectiveProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, 
                                              const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::perspectiveProjectionRHS: Unable to find perspective projection. Near and Far values are same.";
    }
    if(EQ_32(viewVolumeWidth, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionRHS: Unable to find perspective projection. Width of view volume is zero.";
    }
    if(EQ_32(viewVolumeHeight, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionRHS: Unable to find perspective projection. Height of view volume is zero.";
    }

    e[0][0] = (2.0f * near) / viewVolumeWidth;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = (2.0f * near) / viewVolumeHeight;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = far /(near - far);// DX. //e[2][2] = (far + near) /(near - far); // OpenGL
    e[2][3] = (near * far)/(near - far); // DX. //e[2][3] = (2.0f * near * far) / (near - far)); // OpenGL

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = -1;
    e[3][3] = 0;

    return *this;
}
//---------------------------------------------------------------------------------------

//Imp-KK: Check this. Is this the average view volume height and width for perspective?
//If it is problematic, uncomment it.
CMatrix4f CMatrix4f::perspectiveProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::perspectiveProjectionLHS: Unable to find perspective projection. Near and Far values are same.";
    }
    if(EQ_32(viewVolumeWidth, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionLHS: Unable to find perspective projection. Width of view volume is zero.";
    }
    if(EQ_32(viewVolumeHeight, 0.0f))
    {
        throw "CMatrix4f::perspectiveProjectionLHS: Unable to find perspective projection. Height of view volume is zero.";
    }

    e[0][0] = (2.0f * near) / viewVolumeWidth;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = (2.0f * near) / viewVolumeHeight;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = far /(near - far);// DX. //e[2][2] = (far + near) /(near - far); // OpenGL
    e[2][3] = -((near * far)/(near - far)); // DX. //e[2][3] = (-2.0f * near * far) / (near - far)); // OpenGL
    //Imp-KK: Check for the sign above...

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = 1;
    e[3][3] = 0;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::orthographicProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::orthographicProjectionRHS: Unable to find orthographic projection. Near and Far values are same.";
    }
    if(EQ_32(viewVolumeWidth, 0.0f))
    {
        throw "CMatrix4f::orthographicProjectionRHS: Unable to find orthographic projection. Width of view volume is zero.";
    }
    if(EQ_32(viewVolumeHeight, 0.0f))
    {
        throw "CMatrix4f::orthographicProjectionRHS: Unable to find orthographic projection. Height of view volume is zero.";
    }

    e[0][0] = (2.0f) / viewVolumeWidth;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = (2.0f) / viewVolumeHeight;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = 1.0f /(near - far);
    e[2][3] = (near)/(near - far);

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = 0;
    e[3][3] = 1.0f;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::orthographicProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far)
{
    if(EQ_32(near, far))
    {
        throw "CMatrix4f::orthographicProjectionLHS: Unable to find orthographic projection. Near and Far values are same.";
    }
    if(EQ_32(viewVolumeWidth, 0.0f))
    {
        throw "CMatrix4f::orthographicProjectionLHS: Unable to find orthographic projection. Width of view volume is zero.";
    }
    if(EQ_32(viewVolumeHeight, 0.0f))
    {
        throw "CMatrix4f::orthographicProjectionLHS: Unable to find orthographic projection. Height of view volume is zero.";
    }

    e[0][0] = (2.0f) / viewVolumeWidth;
    e[0][1] = 0;
    e[0][2] = 0;
    e[0][3] = 0;

    e[1][0] = 0;
    e[1][1] = (2.0f) / viewVolumeHeight;
    e[1][2] = 0;
    e[1][3] = 0;

    e[2][0] = 0;
    e[2][1] = 0;
    e[2][2] = 1.0f /(far - near);
    e[2][3] = (near)/(near - far);

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = 0;
    e[3][3] = 1.0f;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::viewMatrixRHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector)
{
    //Calculate camera coordinate axes
    CVector3f camDirection = camPosition - camTarget;  //This is view direction
    camDirection.normalize();

    return cameraLookInDirection(camPosition, camDirection, upVector);
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::viewMatrixLHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector)
{
    //Calculate camera coordinate axes
    CVector3f camDirection = camTarget - camPosition;  //This is view direction
    camDirection.normalize();

    return cameraLookInDirection(camPosition, camDirection, upVector);
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::cameraLookAtRHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector)
{
    return viewMatrixRHS(camPosition, camTarget, upVector);
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::cameraLookAtLHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector)
{
    return viewMatrixLHS(camPosition, camTarget, upVector);
}
//---------------------------------------------------------------------------------------

const CMatrix4f& CMatrix4f::cameraLookInDirection(const CPoint3f& camPosition, const CVector3f& camDirection, const CVector3f& upVector)
{
    CVector3f zAxis = camDirection;
    zAxis.normalize();

    CVector3f xAxis = upVector.cross(zAxis);
    xAxis.normalize();

    CVector3f camPositionVector = camPosition.positionVector();

    CVector3f yAxis = zAxis.cross(xAxis);

    e[0][0] = xAxis.x;
    e[0][1] = xAxis.y;
    e[0][2] = xAxis.z;
    e[0][3] = -(xAxis.dot(camPositionVector));

    e[1][0] = yAxis.x;
    e[1][1] = yAxis.y;
    e[1][2] = yAxis.z;
    e[1][3] = -(yAxis.dot(camPositionVector));

    e[2][0] = zAxis.x;
    e[2][1] = zAxis.y;
    e[2][2] = zAxis.z;
    e[2][3] = -(zAxis.dot(camPositionVector));

    e[3][0] = 0;
    e[3][1] = 0;
    e[3][2] = 0;
    e[3][3] = 1.0f;

    return *this;
}
//---------------------------------------------------------------------------------------

//Transformations for vector alignments

CMatrix4f CMatrix4f::AlignVecToPositiveZ(const CVector3f& vector)
{
    CVector3f tempVec = vector;
    tempVec.normalize();

    if(tempVec == Z_AXIS)
    {
        setIdentity();
        return *this;
    }

    //Step 1: Rotate about Z-axis to bring vector in XZ plane.
    CMatrix4f matrix = RotateVecToPlaneXZ(vector);
    setIdentity();

    //Step 2: Rotate about Y-axis to bring vector in along +Z-Axis.

    const AReal32 TRUNCATION_CORRECTOR = 10.0f;

    AReal32 u = tempVec.x * TRUNCATION_CORRECTOR;
    AReal32 v = tempVec.y * TRUNCATION_CORRECTOR;
    AReal32 w = tempVec.z * TRUNCATION_CORRECTOR;

    AReal32 Usq = u * u;
    AReal32 Vsq = v * v;
    AReal32 Wsq = w * w;

    AReal32 K = Usq + Vsq;

    AReal32 Rsqrt = sqrt(Usq + Vsq + Wsq);
    AReal32 Ksqrt = sqrt(K);

    e[0][0] = w / Rsqrt;
    e[0][2] = - Ksqrt / Rsqrt;
    e[2][0] = Ksqrt / Rsqrt;
    e[2][2] = w / Rsqrt;
    
    *this = (*this) * matrix;

    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::RotateVecToPlaneXZ(const CVector3f& vector)
{
    CVector3f tempVec = vector;
    tempVec.normalize();

    setIdentity();

    if(tempVec == Z_AXIS)
    {
        return *this;
    }

    const AReal32 TRUNCATION_CORRECTOR = 10.0f;

    AReal32 u = tempVec.x * TRUNCATION_CORRECTOR;
    AReal32 v = tempVec.y * TRUNCATION_CORRECTOR;

    AReal32 Usq = u * u;
    AReal32 Vsq = v * v;
    AReal32 Ksqrt = sqrt(Usq + Vsq);

    e[0][0] =  u / Ksqrt;
    e[0][1] =  v / Ksqrt;
    e[1][0] = -v / Ksqrt;
    e[1][1] =  u / Ksqrt;

    return *this;
}
//---------------------------------------------------------------------------------------

//Transformation methods

CVector3f CMatrix4f::rotate(const CVector3f& vec) const
{
    return CVector3f( (e[0][0] * vec.x / m_scale.x) + (e[0][1] * vec.y)             + (e[0][2] * vec.z),
                      (e[1][0] * vec.x)             + (e[1][1] * vec.y / m_scale.y) + (e[1][2] * vec.z),
                      (e[2][0] * vec.x)             + (e[2][1] * vec.y)             + (e[2][2] * vec.z / m_scale.z)
                    );
}
//---------------------------------------------------------------------------------------

CPoint3f CMatrix4f::rotate(const CPoint3f& point) const
{
    return CPoint3f( (e[0][0] * point.x / m_scale.x) + (e[0][1] * point.y)             + (e[0][2] * point.z),
                     (e[1][0] * point.x)             + (e[1][1] * point.y / m_scale.y) + (e[1][2] * point.z),
                     (e[2][0] * point.x)             + (e[2][1] * point.y)             + (e[2][2] * point.z / m_scale.z)
                    );
}
//---------------------------------------------------------------------------------------

CVector3f CMatrix4f::rotateAndScale(const CVector3f& vec) const
{
    return CVector3f( (e[0][0] * vec.x) + (e[0][1] * vec.y) + (e[0][2] * vec.z),
                      (e[1][0] * vec.x) + (e[1][1] * vec.y) + (e[1][2] * vec.z),
                      (e[2][0] * vec.x) + (e[2][1] * vec.y) + (e[2][2] * vec.z)
                    );
}
//---------------------------------------------------------------------------------------

CPoint3f CMatrix4f::rotateAndScale(const CPoint3f& point) const
{
    return CPoint3f( (e[0][0] * point.x) + (e[0][1] * point.y) + (e[0][2] * point.z),
                     (e[1][0] * point.x) + (e[1][1] * point.y) + (e[1][2] * point.z),
                     (e[2][0] * point.x) + (e[2][1] * point.y) + (e[2][2] * point.z)
                    );
}
//---------------------------------------------------------------------------------------

CPoint3f CMatrix4f::translate(const CPoint3f& point) const
{
    return CPoint3f(point.x + m[3], point.y + m[7], point.z + m[11]);
}
//---------------------------------------------------------------------------------------

CPoint3f CMatrix4f::transform(const CPoint3f& point) const
{
    return CPoint3f( (e[0][0] * point.x) + (e[0][1] * point.y) + (e[0][2] * point.z) + (e[0][3]),
                     (e[1][0] * point.x) + (e[1][1] * point.y) + (e[1][2] * point.z) + (e[1][3]),
                     (e[2][0] * point.x) + (e[2][1] * point.y) + (e[2][2] * point.z) + (e[2][3])
                    );
}
//---------------------------------------------------------------------------------------

//Operators

AReal32& CMatrix4f::operator () (const AIndex32& rowIndex, const AIndex32& colIndex)
{
    if( (rowIndex < 0 || rowIndex >= 4) && (colIndex < 0 || colIndex >= 4) )
    {
        throw "CMatrix4f::Operator (rowIndex, columnIndex) : Invalid index passed.";
    }
    return e[rowIndex][colIndex];
}
//---------------------------------------------------------------------------------------

const AReal32& CMatrix4f::operator () (const AIndex32& rowIndex, const AIndex32& colIndex) const
{
    if( (rowIndex < 0 || rowIndex >= 4) && (colIndex < 0 || colIndex >= 4) )
    {
        throw "CMatrix4f::Operator (rowIndex, columnIndex) : Invalid index passed.";
    }
    return e[rowIndex][colIndex];
}
//---------------------------------------------------------------------------------------

//Index Operators

AReal32& CMatrix4f::operator [] (const AIndex32& index)
{
    if(index < 0 || index >= 16)
    {
        throw "CMatrix4f::Operator [] : Invalid index passed.";
    }
    return m[index];
}
//---------------------------------------------------------------------------------------

const AReal32& CMatrix4f::operator [] (const AIndex32& index) const
{
    if(index < 0 || index >= 16)
    {
        throw "CMatrix4f::Operator [] : Invalid index passed.";
    }
    return m[index];
}
//---------------------------------------------------------------------------------------

//assignment operator

CMatrix4f& CMatrix4f::operator = (const CMatrix4f& matrix)
{
    memcpy(m, matrix.m, sizeof(AReal32)*16);
    m_scale.set(matrix.m_scale);
    m_bAbsoluteScaleRecoverable = matrix.m_bAbsoluteScaleRecoverable;

    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators
 
bool CMatrix4f::operator == (const CMatrix4f& matrix) const
{
    for(AInt32 i = 0; i < 16; i++)
    {
        if(! EQ_32(m[i], matrix.m[i]))
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CMatrix4f::operator != (const CMatrix4f& matrix) const
{
    for(AInt32 i = 0; i < 16; i++)
    {
        if(! EQ_32(m[i], matrix.m[i]))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CMatrix4f::operator < (const CMatrix4f& matrix) const
{
    for(AInt32 i = 0; i < 16; i++)
    {
        if((m[i] < matrix.m[i]) && !EQ_32(m[i], matrix.m[i]))
        {
            return true;
        }
        if((m[i] > matrix.m[i]) && !EQ_32(m[i], matrix.m[i]))
        {
            return false;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator + (const CMatrix4f& matrix) const
{
    CMatrix4f result(*this);
    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] += matrix.m[i];
    }
    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f& CMatrix4f::operator += (const CMatrix4f& matrix)
{
    for(AInt32 i = 0; i < 16; i++)
    {
        m[i] += matrix.m[i];
    }
    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator - (const CMatrix4f& matrix) const
{
    CMatrix4f result(*this);
    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] -= matrix.m[i];
    }
    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f& CMatrix4f::operator -= (const CMatrix4f& matrix)
{
    for(AInt32 i = 0; i < 16; i++)
    {
        m[i] -= matrix.m[i];
    }
    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator * (const AReal32& scalar) const
{
    CMatrix4f result(*this);
    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] *= scalar;
    }
    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f& CMatrix4f::operator *= (const AReal32& scalar)
{
    for(AInt32 i = 0; i < 16; i++)
    {
        m[i] *= scalar;
    }
    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator / (const AReal32& scalar) const
{
    CMatrix4f result(*this);
    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] /= scalar;
    }
    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f& CMatrix4f::operator /= (const AReal32& scalar)
{
    for(AInt32 i = 0; i < 16; i++)
    {
        m[i] /= scalar;
    }
    return *this;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator - () const
{
    CMatrix4f result(*this);
    for(AInt32 i = 0; i < 16; i++)
    {
        result.m[i] = -result.m[i];
    }
    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f CMatrix4f::operator * (const CMatrix4f& matrix) const
{
    AReal32 a[16] = {
        m[ 0] * matrix.m[0] + m[ 1] * matrix.m[ 4] + m[ 2] * matrix.m[ 8] + m[ 3] * matrix.m[12],
        m[ 0] * matrix.m[1] + m[ 1] * matrix.m[ 5] + m[ 2] * matrix.m[ 9] + m[ 3] * matrix.m[13],
        m[ 0] * matrix.m[2] + m[ 1] * matrix.m[ 6] + m[ 2] * matrix.m[10] + m[ 3] * matrix.m[14],
        m[ 0] * matrix.m[3] + m[ 1] * matrix.m[ 7] + m[ 2] * matrix.m[11] + m[ 3] * matrix.m[15],

        m[ 4] * matrix.m[0] + m[ 5] * matrix.m[ 4] + m[ 6] * matrix.m[ 8] + m[ 7] * matrix.m[12],
        m[ 4] * matrix.m[1] + m[ 5] * matrix.m[ 5] + m[ 6] * matrix.m[ 9] + m[ 7] * matrix.m[13],
        m[ 4] * matrix.m[2] + m[ 5] * matrix.m[ 6] + m[ 6] * matrix.m[10] + m[ 7] * matrix.m[14],
        m[ 4] * matrix.m[3] + m[ 5] * matrix.m[ 7] + m[ 6] * matrix.m[11] + m[ 7] * matrix.m[15],

        m[ 8] * matrix.m[0] + m[ 9] * matrix.m[ 4] + m[10] * matrix.m[ 8] + m[11] * matrix.m[12],
        m[ 8] * matrix.m[1] + m[ 9] * matrix.m[ 5] + m[10] * matrix.m[ 9] + m[11] * matrix.m[13],
        m[ 8] * matrix.m[2] + m[ 9] * matrix.m[ 6] + m[10] * matrix.m[10] + m[11] * matrix.m[14],
        m[ 8] * matrix.m[3] + m[ 9] * matrix.m[ 7] + m[10] * matrix.m[11] + m[11] * matrix.m[15],

        m[12] * matrix.m[0] + m[13] * matrix.m[ 4] + m[14] * matrix.m[ 8] + m[15] * matrix.m[12],
        m[12] * matrix.m[1] + m[13] * matrix.m[ 5] + m[14] * matrix.m[ 9] + m[15] * matrix.m[13],
        m[12] * matrix.m[2] + m[13] * matrix.m[ 6] + m[14] * matrix.m[10] + m[15] * matrix.m[14],
        m[12] * matrix.m[3] + m[13] * matrix.m[ 7] + m[14] * matrix.m[11] + m[15] * matrix.m[15] };

    CMatrix4f result((const AReal32*) a, false);

    result.setScaleValues(m_scale.x * matrix.m_scale.x, 
                          m_scale.y * matrix.m_scale.y, 
                          m_scale.z * matrix.m_scale.z);

    return result;
}
//---------------------------------------------------------------------------------------

CMatrix4f& CMatrix4f::operator *= (const CMatrix4f& matrix)
{
    AReal32 a[16] = {
        m[ 0] * matrix.m[0] + m[ 1] * matrix.m[ 4] + m[ 2] * matrix.m[ 8] + m[ 3] * matrix.m[12],
        m[ 0] * matrix.m[1] + m[ 1] * matrix.m[ 5] + m[ 2] * matrix.m[ 9] + m[ 3] * matrix.m[13],
        m[ 0] * matrix.m[2] + m[ 1] * matrix.m[ 6] + m[ 2] * matrix.m[10] + m[ 3] * matrix.m[14],
        m[ 0] * matrix.m[3] + m[ 1] * matrix.m[ 7] + m[ 2] * matrix.m[11] + m[ 3] * matrix.m[15],

        m[ 4] * matrix.m[0] + m[ 5] * matrix.m[ 4] + m[ 6] * matrix.m[ 8] + m[ 7] * matrix.m[12],
        m[ 4] * matrix.m[1] + m[ 5] * matrix.m[ 5] + m[ 6] * matrix.m[ 9] + m[ 7] * matrix.m[13],
        m[ 4] * matrix.m[2] + m[ 5] * matrix.m[ 6] + m[ 6] * matrix.m[10] + m[ 7] * matrix.m[14],
        m[ 4] * matrix.m[3] + m[ 5] * matrix.m[ 7] + m[ 6] * matrix.m[11] + m[ 7] * matrix.m[15],

        m[ 8] * matrix.m[0] + m[ 9] * matrix.m[ 4] + m[10] * matrix.m[ 8] + m[11] * matrix.m[12],
        m[ 8] * matrix.m[1] + m[ 9] * matrix.m[ 5] + m[10] * matrix.m[ 9] + m[11] * matrix.m[13],
        m[ 8] * matrix.m[2] + m[ 9] * matrix.m[ 6] + m[10] * matrix.m[10] + m[11] * matrix.m[14],
        m[ 8] * matrix.m[3] + m[ 9] * matrix.m[ 7] + m[10] * matrix.m[11] + m[11] * matrix.m[15],

        m[12] * matrix.m[0] + m[13] * matrix.m[ 4] + m[14] * matrix.m[ 8] + m[15] * matrix.m[12],
        m[12] * matrix.m[1] + m[13] * matrix.m[ 5] + m[14] * matrix.m[ 9] + m[15] * matrix.m[13],
        m[12] * matrix.m[2] + m[13] * matrix.m[ 6] + m[14] * matrix.m[10] + m[15] * matrix.m[14],
        m[12] * matrix.m[3] + m[13] * matrix.m[ 7] + m[14] * matrix.m[11] + m[15] * matrix.m[15] };

    memcpy(m, a, sizeof(AReal32)*16);

    m_scale.x *= matrix.m_scale.x;
    m_scale.y *= matrix.m_scale.y;
    m_scale.z *= matrix.m_scale.z;

    m_bAbsoluteScaleRecoverable = false;

    return *this;
}
//---------------------------------------------------------------------------------------

CVector3f CMatrix4f::operator* (const CVector3f& vec) const
{
    return CVector3f( (e[0][0] * vec.x / m_scale.x) + (e[0][1] * vec.y)             + (e[0][2] * vec.z),
                      (e[1][0] * vec.x)             + (e[1][1] * vec.y / m_scale.y) + (e[1][2] * vec.z),
                      (e[2][0] * vec.x)             + (e[2][1] * vec.y)             + (e[2][2] * vec.z / m_scale.z)
                    );
}
//---------------------------------------------------------------------------------------

CPoint3f CMatrix4f::operator* (const CPoint3f& point) const
{
    return CPoint3f( (e[0][0] * point.x) + (e[0][1] * point.y) + (e[0][2] * point.z) + (e[0][3]),
                     (e[1][0] * point.x) + (e[1][1] * point.y) + (e[1][2] * point.z) + (e[1][3]),
                     (e[2][0] * point.x) + (e[2][1] * point.y) + (e[2][2] * point.z) + (e[2][3])
                    );
}
//---------------------------------------------------------------------------------------


void CMatrix4f::add(const AReal32* ptr)
{
    for(AUInt32 i = 0; i < 16; i++)
    {
        m[i] += ptr[i];
    }
}
//---------------------------------------------------------------------------------------

void CMatrix4f::subtract(const AReal32* ptr)
{
    for(AUInt32 i = 0; i < 16; i++)
    {
        m[i] -= ptr[i];
    }
}
//---------------------------------------------------------------------------------------

//Returns the absolute values of the scales of the matrix. Does not return the
//  negative value if it is there...

//Courtsey Ref: http://www.robertblum.com/articles/2005/02/14/decomposing-matrices

CVector3f CMatrix4f::recoverScale() const
{
    if( EQ_32(e[0][1], 0.0f) && EQ_32(e[0][2], 0.0f) &&
        EQ_32(e[1][0], 0.0f) && EQ_32(e[1][2], 0.0f) &&
        EQ_32(e[2][0], 0.0f) && EQ_32(e[2][1], 0.0f) )
    {
        return CVector3f(e[0][0], e[1][1], e[2][2]);
    }

    AReal32 a = sqrt((e[0][0] * e[0][0]) + (e[1][0] * e[1][0]) + (e[2][0] * e[2][0]));
    AReal32 b = sqrt((e[0][1] * e[0][1]) + (e[1][1] * e[1][1]) + (e[2][1] * e[2][1]));
    AReal32 c = sqrt((e[0][2] * e[0][2]) + (e[1][2] * e[1][2]) + (e[2][2] * e[2][2]));

    return CVector3f(a, b, c);
}
//---------------------------------------------------------------------------------------

//Returns a equivalent Euler rotation of the rotation part.

CVector3f CMatrix4f::getRotationDeg() const
{
    CVector3f scale(m_scale);
    if(false == m_bAbsoluteScaleRecoverable)
    {
        scale = recoverScale();
    }

    const CVector3f invScale((AReal32)(1.0f / scale.x), (AReal32)(1.0f / scale.y), (AReal32)(1.0f / scale.z));

    AReal64 xRad = 0.0, yRad = 0.0, zRad = 0.0;
    AReal32 xDeg = 0.0f, yDeg = 0.0f, zDeg = 0.0f;
    AReal64 rx = 0.0, ry = 0.0;

    yRad = - asin(m[8] * invScale.x);
    yDeg = (AReal32) RAD_TO_DEG64(yRad);
    const AReal64 cy = cos(yRad);

    if(EQ_64(cy, 0.0))
    {
        xDeg = 0.0f;
        rx = m[5] * invScale.y;
        ry = -m[1] * invScale.y;
        zRad = atan2(ry, rx);
        zDeg = (AReal32) RAD_TO_DEG64(zRad);
    }
    else
    {
        const AReal64 icy = 1.0 / cy;

        rx = m[10] * icy * invScale.z;
        ry = m[9] * icy * invScale.y;
        xRad = atan2(ry, rx);
        xDeg = (AReal32) RAD_TO_DEG64(xRad);

        rx = m[0] * icy * invScale.x;
        ry = m[4] * icy * invScale.x;
        zRad = atan2(ry, rx);
        zDeg = (AReal32) RAD_TO_DEG64(zRad);
    }

    if (xDeg < 0.0f) xDeg += 360.0f;
    if (yDeg < 0.0f) yDeg += 360.0f;
    if (zDeg < 0.0f) zDeg += 360.0f;

    if (xDeg > 360.0f) xDeg -= 360.0f;
    if (yDeg > 360.0f) yDeg -= 360.0f;
    if (zDeg > 360.0f) zDeg -= 360.0f;

    return CVector3f(xDeg, yDeg, zDeg);
}
//---------------------------------------------------------------------------------------

