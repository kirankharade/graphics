//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATH_STRUCTS_H__
#define __AN_MATH_STRUCTS_H__
//---------------------------------------------------------------------------------------
#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CPoint3f.h"
#include "CConverter.h"
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

struct AN_MATHS_API SCoordinateExtents
{
    CPoint3f    minPoint;
    CPoint3f    maxPoint;

    SCoordinateExtents()
    {
        minPoint = CPoint3f();
        maxPoint = CPoint3f();
    }

    SCoordinateExtents(const CPoint3f& minPt, const CPoint3f& maxPt)
    {
        minPoint = minPt;
        maxPoint = maxPt;
    }

    SCoordinateExtents(const CPoint2f& minPt, const CPoint2f& maxPt)
    {
        minPoint = CConverter::convertTo3f(minPt);
        maxPoint = CConverter::convertTo3f(maxPt);
    }
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_MATH_STRUCTS_H__
//---------------------------------------------------------------------------------------
