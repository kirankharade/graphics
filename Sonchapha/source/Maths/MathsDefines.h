//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATH_DEFINES_H__
#define __AN_MATH_DEFINES_H__
//---------------------------------------------------------------------------------------

#define AN_MATHS_START_NAMESPACE namespace An { namespace Maths {
#define AN_MATHS_END_NAMESPACE } }
//---------------------------------------------------------------------------------------

#include <limits.h>
#include <float.h>
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//------------//
// Math Types
//------------//

#define TOLERANCE_32            0.000001f
#define TOLERANCE_64            0.00000001

#define PI_32                   3.14159265359f
#define PI_64                   3.1415926535897932384626433832795028841971693993751

#define PI_32_RECIPROCAL        0.31830988618f
#define PI_64_RECIPROCAL        0.31830988618379067153776752674508

#define PI_32_HALF              1.57079632679f
#define PI_64_HALF              1.5707963267948966192313216916395

#define PI_32_QUARTER           0.78539816340f
#define PI_64_QUARTER           0.78539816339744830961566084581975

#define PI_32_TWICE             6.28318530718f
#define PI_64_TWICE             6.283185307179586476925286766558

#define SQRT_2_64               1.4142135623730950488016887242097
#define SQRT_3_64               1.7320508075688772935274463415059

#define SQRT_2_64_RECIPROCAL    0.70710678118654752440084436210485
#define SQRT_3_64_RECIPROCAL    0.57735026918962576450914878050196

#define MAX_REAL32              FLT_MAX
#define MAX_REAL64              DBL_MAX

#define DEG_TO_RAD64(x)         (0.017453292519943295769236907684883 * x)
#define RAD_TO_DEG64(x)         (57.295779513082320876798154814114 * x)

//-------------//
// Math Macros
//-------------//

#define MIN2(a, b)               (a < b ? a : b)
#define MAX2(a, b)               (a < b ? b : a)
#define MIN3(a, b, c)            (a < b ? MIN2(a, c) : MIN2(b, c))
#define MAX3(a, b, c)            (a < b ? MAX2(b, c) : MAX2(a, c))

#define ABS(a)                  ((a < 0) ? -a : a)
#define CLAMP(v, r1, r2)        (MIN (MAX(v,r1), r2))

#define ISZERO_32(x)            (fabs(x) <= TOLERANCE_32)
#define ISZERO_64(x)            (fabs(x) <= TOLERANCE_64)

#define ISNOTZERO_32(x)         (fabs(x) > TOLERANCE_32)
#define ISNOTZERO_64(x)         (fabs(x) > TOLERANCE_64)

#define EQ_32(x, y)             (fabs(x - y) < TOLERANCE_32)
#define GTEQ_32(x, y)           ((x >= y) || (EQ_32(x, y)))
#define LTEQ_32(x, y)           ((x <= y) || (EQ_32(x, y)))
#define LT_32(x, y)             (x < (y - TOLERANCE_32))
#define GT_32(x, y)             (x > (y + TOLERANCE_32))

#define EQ_64(x, y)             (fabs(x - y) < TOLERANCE_64)
#define GTEQ_64(x, y)           ((x >= y) || (EQ_64(x, y)))
#define LTEQ_64(x, y)           ((x <= y) || (EQ_64(x, y)))
#define LT_64(x, y)             (x < (y - TOLERANCE_64))
#define GT_64(x, y)             (x > (y + TOLERANCE_64))

#define EQT(x, y, tolerance)    (fabs(x - y) < tolerance)
#define GTEQT(x, y, tolerance)  ((x >= y) || (EQT(x, y, tolerance)))
#define LTEQT(x, y, tolerance)  ((x <= y) || (EQT(x, y, tolerance)))
#define LTT(x, y, tolerance)    (x < (y - tolerance))
#define GTT(x, y, tolerance)    (x > (y + tolerance))

//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_MATH_DEFINES_H__
//---------------------------------------------------------------------------------------
