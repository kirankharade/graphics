//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_PLANE_H__
#define __C_PLANE_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "MathsEnums.h"
#include "CPoint3f.h"
#include "CVector3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CLine;
//IntersectionList;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CPlane
{
    public:
        
        CPoint3f    m_point;
        CVector3f   m_normal;

    public:
        
        CPlane();

        CPlane(const CPoint3f& point, const CVector3f& normal);
    
        CPlane(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D);
    
        CPlane(const CPoint3f& p1, const CPoint3f& p2, const CPoint3f& p3);

        CPlane(const CPlane& plane);
    
        virtual ~CPlane();
    
        void set(const CPoint3f& point, const CVector3f& normal);
    
        void set(const AReal32& A, const AReal32& B, const AReal32& C, const AReal32& D);

        void set(const CPoint3f& p1, const CPoint3f& p2, const CPoint3f& p3);

        CPoint3f getPoint() const;
       
        CVector3f getNormal() const;

        void getCoefficients(AReal32& A, AReal32& B, AReal32& C, AReal32& D);

        AReal32 getCoefficientD() const;

        bool isIntersecting(const CLine*& pLine) const;
       
        bool isIntersecting(const CLine*& pLine, CPoint3f& intersectionPoint) const;

        CPoint3f closestPoint(const CPoint3f& point) const;

        AReal32 distFromPoint(const CPoint3f& point) const;

        bool isParallel(const CPlane& plane) const;
       
        EPlaneSideQualification getQualification(const CPoint3f& point, const AReal32 tolerance = TOLERANCE_32) const;
       
        bool Intersection(const CPlane& plane1, const CPlane& plane2, CPoint3f& intersectionPoint) const;

        bool Intersection(const CPlane& plane, CPoint3f& intersectionPoint, CVector3f& intersectionlineDir) const;

        //TO DO:
       
        //void getSquarePlaneCorners(const CPoint3f& ptProjected, const AReal32& squareSize, CPoint3f*& corners) const;

        //void getSquarePlaneCorners(const CVector3f& yAxisProjected, const AReal32& squareSize, CPoint3f*& corners) const;
            
        //bool isIntersecting(const CBox* pBox) const;
       
        //bool isIntersecting(const CBox* pBox, IntersectionList*& list) const;
            
        //Assignment operator

        CPlane& operator = (const CPlane& plane);
    
        //Comparison Operators
         
        bool operator == (const CPlane& plane) const;
    
        bool operator != (const CPlane& plane) const;
    
        bool operator < (const CPlane& plane) const;
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_PLANE_H__
//---------------------------------------------------------------------------------------
