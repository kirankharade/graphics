//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CLineSegment2D.h"
#include "CLineSegment.h"
#include "CUtils.h"
#include "MathsSettings.h"
#include "CPlane.h"
#include "CLine.h"
#include "CLineSegment.h"
#include "CRay.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CLineSegment2D::CLineSegment2D()
{
    m_firstPoint = CPoint2f();
    m_secondPoint = CPoint2f();
}
//---------------------------------------------------------------------------------------

CLineSegment2D::CLineSegment2D(const CPoint2f& point, const CVector3f& direction, const AReal32& length)
: m_firstPoint(point)
{
    CVector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

CLineSegment2D::CLineSegment2D(const CPoint2f& firstPoint, const CPoint2f& secondPoint)
: m_firstPoint(firstPoint)
, m_secondPoint(secondPoint)
{
}
//---------------------------------------------------------------------------------------

CLineSegment2D::CLineSegment2D(const CLineSegment2D& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
}
//---------------------------------------------------------------------------------------

CLineSegment2D::CLineSegment2D(const CLineSegment& lineSegment)
{
    m_firstPoint = lineSegment.getFirstPoint();
    m_secondPoint = lineSegment.getSecondPoint();
}
//---------------------------------------------------------------------------------------

CLineSegment2D::~CLineSegment2D()
{
}
//---------------------------------------------------------------------------------------

void CLineSegment2D::set(const CPoint2f& firstPoint, const CVector3f& direction, const AReal32& length)
{
    m_firstPoint = firstPoint;
    CVector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

void CLineSegment2D::set(const CPoint2f& firstPoint, const CPoint2f& secondPoint)
{
    m_firstPoint = firstPoint;
    m_secondPoint = secondPoint;
}
//---------------------------------------------------------------------------------------

CPoint2f CLineSegment2D::getFirstPoint() const
{
    return m_firstPoint;
}
//---------------------------------------------------------------------------------------

CPoint2f CLineSegment2D::getSecondPoint() const
{
    return m_secondPoint;
}
//---------------------------------------------------------------------------------------

CVector3f CLineSegment2D::getDirection() const
{
    CVector3f vec = m_secondPoint - m_firstPoint;
    vec.normalize();
    return vec;
}
//---------------------------------------------------------------------------------------

AReal32 CLineSegment2D::getLength() const
{
    return (m_secondPoint - m_firstPoint).length();
}
//---------------------------------------------------------------------------------------

//Reference: http://paulbourke.net/geometry/lineline2d/

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CLineSegment2D* lineSegment, CPoint2f& intersectionPoint) const
{
    if(!lineSegment)
    {
        return CLineSegment2D::undefinedIntersection;
    }
    CPoint2f p1 = m_firstPoint;
    CPoint2f p2 = m_secondPoint;
    CPoint2f p3 = lineSegment->getFirstPoint();
    CPoint2f p4 = lineSegment->getSecondPoint();

    AReal32 u1 = ((p4.x-p3.x) * (p1.y-p3.y)) - ((p4.y-p3.y) * (p1.x-p3.x));
    AReal32 u2 = ((p2.x-p1.x) * (p1.y-p3.y)) - ((p2.y-p1.y) * (p1.x-p3.x));
    AReal32 d  = ((p4.y-p3.y) * (p2.x-p1.x)) - ((p4.x-p3.x) * (p2.y-p1.y));

    if(EQ_32(d, 0) && EQ_32(u1, 0))
    {
       return CLineSegment2D::CoIncident;
    }
    if(EQ_32(d, 0))
    {
        return CLineSegment2D::NonCoIncidentParallel;
    }

    u1 /= d;
    u2 /= d;

    if(GTEQ_32(u1, 0) || LTEQ_32(u1, 1.0f))
    {
        return CLineSegment2D::IntersectingAtEnds;
    }

    if((u1 >= (AReal32) 0.0 && u1 <= (AReal32) 1.0) &&
       (u2 >= (AReal32) 0.0 && u2 <= (AReal32) 1.0))
    {
       intersectionPoint = m_firstPoint + ((m_secondPoint - m_firstPoint) * u1);
       return CLineSegment2D::Intersecting;
    }
    return CLineSegment2D::NonIntersecting;
}
//---------------------------------------------------------------------------------------

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CLineSegment2D* lineSegment) const
{
    CPoint2f intersectionPoint;
    return classifyByIntersection(lineSegment, intersectionPoint);
}
//---------------------------------------------------------------------------------------

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CLine* line, CPoint2f& intersectionPoint) const
{
    if(!line)
    {
        return CLineSegment2D::undefinedIntersection;
    }
    CPoint2f p1 = m_firstPoint;
    CPoint2f p2 = m_secondPoint;
    CPoint2f p3 = line->getPoint();
    CPoint2f p4 = p3 + (line->getDirection() * 100.0);

    AReal32 u1 = ((p4.x-p3.x) * (p1.y-p3.y)) - ((p4.y-p3.y) * (p1.x-p3.x));
    AReal32 d  = ((p4.y-p3.y) * (p2.x-p1.x)) - ((p4.x-p3.x) * (p2.y-p1.y));

    if(EQ_32(d, 0) && EQ_32(u1, 0))
    {
       return CLineSegment2D::CoIncident;
    }
    if(EQ_32(d, 0))
    {
        return CLineSegment2D::NonCoIncidentParallel;
    }

    u1 /= d;

    if(GTEQ_32(u1, 0) || LTEQ_32(u1, 1.0f))
    {
        return CLineSegment2D::IntersectingAtEnds;
    }

    if((u1 >= (AReal32) 0.0 && u1 <= (AReal32) 1.0))
    {
        intersectionPoint = m_firstPoint + ((m_secondPoint - m_firstPoint) * u1);
       return CLineSegment2D::Intersecting;
    }
    return CLineSegment2D::NonIntersecting;
}
//---------------------------------------------------------------------------------------

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CLine* line) const
{
    CPoint2f intersectionPoint;
    return classifyByIntersection(line, intersectionPoint);
}
//---------------------------------------------------------------------------------------

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CRay* ray, CPoint2f& intersectionPoint) const
{
    if(!ray)
    {
        return CLineSegment2D::undefinedIntersection;
    }
    CPoint2f p3 = ray->getPoint();
    CPoint2f p4 = p3 + (ray->getDirection() * 100000.0); //Create a large segment extending in one direction
    CLineSegment2D ls(p3, p4);
    return classifyByIntersection(&ls, intersectionPoint);
}
//---------------------------------------------------------------------------------------

CLineSegment2D::IntersectionClassification CLineSegment2D::classifyByIntersection(const CRay* ray) const
{
    CPoint2f intersectionPoint;
    return classifyByIntersection(ray, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool CLineSegment2D::passesThrough(const CPoint2f& point, const AReal32 tolerance) const
{
    CPoint3f firstPoint(m_firstPoint.x, m_firstPoint.y, 0);
    CPoint3f secondPoint(m_secondPoint.x, m_secondPoint.y, 0);
    CPoint3f pt(point.x, point.y, 0);

    CLineSegment ls(firstPoint, secondPoint);

    return ls.passesThrough(pt, tolerance);
}
//---------------------------------------------------------------------------------------

CPoint2f CLineSegment2D::closestPoint(const CPoint2f& point) const
{
    CPoint3f firstPoint(m_firstPoint.x, m_firstPoint.y, 0);
    CPoint3f secondPoint(m_secondPoint.x, m_secondPoint.y, 0);
    CPoint3f passedPoint(point.x, point.y, 0);

    CLineSegment ls(firstPoint, secondPoint);

    CPoint3f pt = ls.closestPoint(passedPoint);

    return CPoint2f(pt);
}
//---------------------------------------------------------------------------------------

AReal32 CLineSegment2D::distFromPoint(const CPoint2f& point) const
{
    CPoint2f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CLineSegment2D& CLineSegment2D::operator = (const CLineSegment2D& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CLineSegment2D::operator == (const CLineSegment2D& ls) const
{
    if((m_firstPoint == ls.getFirstPoint()) && (m_secondPoint == ls.getSecondPoint()))
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLineSegment2D::operator != (const CLineSegment2D& lineSegment) const
{
    if( !(*this == lineSegment) )
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLineSegment2D::operator < (const CLineSegment2D& lineSegment) const
{
    CPoint2f thatFirstPoint = lineSegment.getFirstPoint();
    CPoint2f thatSecondPoint = lineSegment.getSecondPoint();

    if(m_firstPoint < thatFirstPoint)
    {
        return true;
    }
    if(m_firstPoint > thatFirstPoint)
    {
        return false;
    }
    if(m_secondPoint < thatSecondPoint)
    {
        return true;
    }
    if(m_secondPoint > thatSecondPoint)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
