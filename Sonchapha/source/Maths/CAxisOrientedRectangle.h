//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_AXIS_ORIENTED_RECTANGLE_H__
#define __C_AXIS_ORIENTED_RECTANGLE_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CVector3f.h"
#include "CMatrix4f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CLine;
class CLineSegment2D;
class CPointIntersectionInfo;
class CPointTriangle;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CAxisOrientedRectangle
{
    protected:
        
        CPoint2f    m_minPoint;
        CPoint2f    m_maxPoint;

    public:
        
        CAxisOrientedRectangle();

        CAxisOrientedRectangle(const CPoint2f& minPoint, const CPoint2f& maxPoint);

        CAxisOrientedRectangle(const CAxisOrientedRectangle& box);
    
        virtual ~CAxisOrientedRectangle();
    
        void setMinCorner(const CPoint2f& minPoint);

        void setMaxCorner(const CPoint2f& maxPoint);

        CPoint2f getMinCorner() const;

        CPoint2f getMaxCorner() const;

        AReal32 getWidth() const;

        AReal32 getHeight() const;

        CPoint2f getCentre() const;

        AReal32 getSurfaceArea() const;

        CPoint2f* getCorners() const;

        CLineSegment2D* getEdges() const;

        CPointTriangle* getTriangles() const;

        //bool isIntersecting(const CLine* pLine) const;

        //CPointIntersectionInfo* getIntersections(const CLine* pLine) const;

        bool isInside(const CPoint2f& point, const AReal32 tolerance = TOLERANCE_32) const;

        //Assignment operator

        virtual CAxisOrientedRectangle& operator = (const CAxisOrientedRectangle& box);
    
        //Comparison Operators
         
        virtual bool operator == (const CAxisOrientedRectangle& box) const;
    
        virtual bool operator != (const CAxisOrientedRectangle& box) const;
    
        virtual bool operator < (const CAxisOrientedRectangle& box) const;
        
    protected:

        void validate();

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_AXIS_ORIENTED_RECTANGLE_H__
//---------------------------------------------------------------------------------------

