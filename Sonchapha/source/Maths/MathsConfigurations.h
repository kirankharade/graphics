//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATHS_CONFIGURATIONS_H__
#define __AN_MATHS_CONFIGURATIONS_H__
//---------------------------------------------------------------------------------------

#include <stdio.h> 

//-------------------------------------------------------------------------
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
    #define _AN_BUILD_MATHS_FOR_WINDOWS_
#endif
//-------------------------------------------------------------------------

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
    /////////////////////////////////////////
    #ifdef AN_MATHS_STATIC_LIBRARY
        #define AN_MATHS_API
    #else
        #ifdef AN_MATHS_EXPORT_API
            #define AN_MATHS_API __declspec(dllexport)
        #else
            #define AN_MATHS_API __declspec(dllimport)
        #endif
    #endif
    
    #ifdef _AN_MATHS_STDCALL_SUPPORTED_
        #define AN_MATHS_CALL_CONVENTION __stdcall
    #else
        #define AN_MATHS_CALL_CONVENTION __cdecl
    #endif
    
     
    //Warnings for deprecation disabled...
    #if defined(_MSC_VER) && (_MSC_VER >= 1400)
        #pragma warning( disable: 4996)
        #pragma warning( disable: 4800)
        #define _CRT_SECURE_NO_DEPRECATE 1
        #define _CRT_NONSTDC_NO_DEPRECATE 1
    #endif
        
    /////////////////////////////////////////
#else

    // Forced exports in gcc for shared libs
    #if (__GNUC__ >= 4) && defined(AN_MATHS_EXPORT_API) && !defined(AN_MATHS_STATIC_LIBRARY) 
        #define AN_MATHS_API __attribute__ ((visibility("default")))
    #else
        #define AN_MATHS_API
    #endif

        #define AN_MATHS_CALL_CONVENTION
        
#endif
//-------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#endif //__AN_MATHS_CONFIGURATIONS_H__
//---------------------------------------------------------------------------------------
