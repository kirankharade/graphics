//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_POLYGON_2DF_H__
#define __C_POLYGON_2DF_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CPointTriangle.h"
#include "CConvexHull2D.h"
#include "CLineSegment2D.h"
#include <vector>

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API CPolygon2D
{

    public:

        enum BoundaryQualification
        {
            InsideBoundary,
            InsideHullBoundary,
            OutsideBoundary,
            OutsideHullBoundary,
            OnBoundary,
            OnHullBoundary,
            UndefinedBoundaryQualification
        };

    private:
        
        CPoint2fList                    m_inputPoints;
        CConvexHull2D                   m_convexHull;
        CPointTriangleList              m_interiorTriangles;
        CPointTriangleList              m_exteriorTriangles;
        CLineSegment2DList              m_validEdges;
        CLineSegment2DList              m_allEdges;
        bool                            m_bIsSelfIntersecting;
        bool                            m_bIsConcave;
        CPoint2f                        m_centroidPoint;

        static const AReal32            ms_exteriorContainerScale;

    public:
        
        CPolygon2D();

        CPolygon2D(const std::vector<CPoint2f>& points);
    
        CPolygon2D(const std::vector<CPoint2i>& points);
    
        CPolygon2D(const CPolygon2D& polygon);
    
        virtual ~CPolygon2D();
    
        void set(const std::vector<CPoint2f>& points);

        void add(const std::vector<CPoint2f>& points);

        void add(const CPoint2f& point);

        void remove(const CPoint2f& point);

        void removeAt(const AIndex32& index);

        bool isSelfIntersecting() const;

        bool isConcave() const;

        bool isInside(const CPoint2i& point) const;
       
        bool isInside(const CPoint2f& point) const;

        bool isInside(const CLineSegment2D& lineSegment) const;

        bool isPolygonOnBothSidesOfSegment(const CLineSegment2D& lineSegment) const;

        bool isIntersectingEdges(const CLineSegment2D& lineSegment) const;

        CPolygon2D::BoundaryQualification qualifyAgainstBoundary(const CPoint2f& point) const;

        CPolygon2D::BoundaryQualification qualifyAgainstBoundary(const CLineSegment2D& point) const;

        CPolygon2D::BoundaryQualification qualifyAgainstBoundary(const CPointTriangle& point) const;

        AReal32 getArea() const;

        AReal32 getPerimeter() const;

        CPoint2f getCentroid() const;

        AUInt32 getPointCount() const;

        const std::vector<CPoint2f>& getPoints() const;

        const CConvexHull2D& getConvexHull() const;
       
        const std::vector<CLineSegment2D>& getAllEdges() const;

        const std::vector<CPointTriangle>& getInteriorTriangles() const;

        std::vector<CPointTriangle> getExteriorTriangles(const AReal32& containerScale) const;

        CPolygon2D getScaled(const AReal32& scaleX, const AReal32& scaleY) const;

        CPolygon2D getRotated(const CPoint2f& rotationCentre, const AReal32& angleDeg) const;

        CPolygon2D getTranslated(const AReal32& dx, const AReal32& dy) const;

        //Assignment operator

        CPolygon2D& operator = (const CPolygon2D& polygon);
    
        //Comparison Operators
         
        bool operator == (const CPolygon2D& rect) const;
    
        bool operator != (const CPolygon2D& rect) const;

    private:

        void update();

        void init();

        void checkIfSelfIntersecting();

        void checkIfConcave();

        void generateAllEdges();

        void findTriangles();

        void findExteriorTrianglesForConcaveCase();

        void findExteriorTrianglesForConvexCase();

        void findInteriorTrianglesForConcaveCase();

        void findInteriorTrianglesForConvexCase();

        void copy(const CPolygon2D& polygon);
        
        bool isSegmentCreatingTwoValidChildrenPolygons(const CLineSegment2D& lineSegment) const;

        bool isSegmentCreatingTwoValidChildrenPolygons(const AIndex32& ev1, const AIndex32& ev2) const;

        void addTrianglesExteriorToConvexHull();

        void getTrianglesExteriorToConvexHull(const AReal32& outerBoundaryOffset, std::vector<CPointTriangle>& triangles) const;

        void findInnerTrianglesBySubDivision(std::vector<CPointTriangle>& triangles);

        bool getValidDivider(AIndex32& idx1, AIndex32& idx2) const;

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_POLYGON_2DF_H__
//---------------------------------------------------------------------------------------
