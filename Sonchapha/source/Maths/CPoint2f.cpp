//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f()
{
    x = 0.0f;
    y = 0.0f;
}
//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f(const AReal32 v[2])
{
    x = v[0];
    y = v[1];
}
//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f(const AReal32& a, const AReal32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f(const CPoint2f& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f(const CPoint2i& point)
{
    x = (AReal32) point.x;
    y = (AReal32) point.y;
}
//---------------------------------------------------------------------------------------

CPoint2f::CPoint2f(const CPoint3f& point)
{
    x = (AReal32) point.x;
    y = (AReal32) point.y;
}
//---------------------------------------------------------------------------------------

void CPoint2f::set(const AReal32& a, const AReal32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

void CPoint2f::set(const CPoint2f& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

AReal32 CPoint2f::dist(const CPoint2f& point) const
{
    return sqrt( (AReal32) (((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y))) );
}
//---------------------------------------------------------------------------------------

//operators

AReal32& CPoint2f::operator [] (const AIndex32& index)
{
    if(0 == index) return x;
    if(1 == index) return y;
    throw "CPoint2f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& CPoint2f::operator [] (const AIndex32& index) const
{
    if(0 == index) return x;
    if(1 == index) return y;
    throw "CPoint2f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

CVector3f CPoint2f::operator - (const CPoint2f& point) const
{
    return CVector3f(x - point.x, y - point.y, 0);
}
//---------------------------------------------------------------------------------------

CPoint2f CPoint2f::operator + (const CPoint2f& point) const
{
    return CPoint2f(x + point.x, y + point.y);
}
//---------------------------------------------------------------------------------------

CPoint2f CPoint2f::operator + (const CVector3f& vec) const
{
    return CPoint2f(x + vec.x, y + vec.y);
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator += (const CPoint2f& point)
{
    x += point.x;
    y += point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator = (const CPoint2f& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator -= (const CVector3f& vec)
{
    x -= vec.x;
    y -= vec.y;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator += (const CVector3f& vec)
{
    x += vec.x;
    y += vec.y;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator = (const CPoint3f& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator = (const CPoint2i& point)
{
    x = (AReal32) point.x;
    y = (AReal32) point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

bool CPoint2f::operator == (const CPoint2f& point) const
{
   if( EQ_32(x, point.x) && EQ_32(y, point.y) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint2f::operator != (const CPoint2f& point) const
{
   if( !EQ_32(x, point.x) && !EQ_32(y, point.y) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint2f::operator < (const CPoint2f& point) const
{
    if( EQ_32(x, point.x) && EQ_32(y, point.y) )
    {
        return false;
    }
    if(x < point.x)      return true;
    else if(x > point.x) return false;
    else return (y < point.y);
}
//---------------------------------------------------------------------------------------

bool CPoint2f::operator > (const CPoint2f& point) const
{
    if( EQ_32(x, point.x) && EQ_32(y, point.y) )
    {
        return false;
    }
    if(x > point.x)      return true;
    else if(x < point.x) return false;
    else return (y > point.y);
}
//---------------------------------------------------------------------------------------

CPoint2f CPoint2f::operator * (const AReal32& scalar) const
{
    return CPoint2f((AReal32)(x * scalar), (AReal32)(y * scalar));
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator *= (const AReal32& scalar)
{
    x = (AReal32) (x * scalar);
    y = (AReal32) (y * scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f CPoint2f::operator / (const AReal32& scalar) const
{
    return CPoint2f((AReal32)(x / scalar), (AReal32)(y / scalar));
}
//---------------------------------------------------------------------------------------

CPoint2f& CPoint2f::operator /= (const AReal32& scalar)
{
    x = (AReal32) (x / scalar);
    y = (AReal32) (y / scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2f CPoint2f::operator - () const
{
    return CPoint2f(-x, -y);
}
//---------------------------------------------------------------------------------------

