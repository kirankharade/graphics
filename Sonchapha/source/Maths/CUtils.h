//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATH_UTILS_H__
#define __AN_MATH_UTILS_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CUtils.h"
#include "MathsStructs.h"
#include <vector>

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_MATHS_API CUtils
{
    public:

        static AReal32 getCornerAngleDeg(const CPoint2f& pa, const CPoint2f& pb, const CPoint2f& pc);

        static AReal32 getCornerAngleDeg(const CPoint3f& pa, const CPoint3f& pb, const CPoint3f& pc);

        static void sortAscending(std::vector<CPoint2f>& points);

        static void sortAscending(std::vector<CPoint3f>& points);

        static void sortDescending(std::vector<CPoint2f>& points);

        static void sortDescending(std::vector<CPoint3f>& points);

        static CPoint2f getCentroid(const std::vector<CPoint2f>& points);

        static CPoint3f getCentroid(const std::vector<CPoint3f>& points);

        static SCoordinateExtents getExtents(const std::vector<CPoint2f>& points);

        static SCoordinateExtents getExtents(const std::vector<CPoint3f>& points);

        static AReal32 GetMax(AReal32 a, AReal32 b, AReal32 c);

        static AReal32 GetMin(AReal32 a, AReal32 b, AReal32 c);

};

//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_MATH_UTILS_H__
//---------------------------------------------------------------------------------------
