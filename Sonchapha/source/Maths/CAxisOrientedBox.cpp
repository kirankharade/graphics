//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CAxisOrientedBox.h"
#include "CBox.h"
#include "CLine.h"
#include "CLineSegment.h"
#include "CPlane.h"
#include "CPointTriangle.h"
#include "IntersectionInfo.h"
#include <vector>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CAxisOrientedBox::CAxisOrientedBox()
: m_minPoint(ORIGIN)
 ,m_maxPoint(ORIGIN)
{
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox::CAxisOrientedBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
: m_minPoint(origin)
 ,m_maxPoint(origin)
{
    m_maxPoint += CVector3f(width, height, depth);
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox::CAxisOrientedBox(const CPoint3f& minPoint, const CPoint3f& maxPoint)
: m_minPoint(minPoint)
 ,m_maxPoint(maxPoint)
{
    validate();
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox::CAxisOrientedBox(const CPoint3f& origin, const AReal32& size)
: m_minPoint(origin)
 ,m_maxPoint(origin)
{
    m_maxPoint = m_minPoint + CVector3f(size, size, size);
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox::CAxisOrientedBox(const CAxisOrientedBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox::~CAxisOrientedBox()
{
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::set(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
{
    m_minPoint = origin;
    m_maxPoint = m_minPoint + CVector3f(width, height, depth);
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::set(const CPoint3f& origin, const AReal32& size)
{
    m_minPoint = origin;
    m_maxPoint = m_minPoint + CVector3f(size, size, size);
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::setMinCorner(const CPoint3f& minPoint)
{
    m_minPoint = minPoint;
    validate();
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::setMaxCorner(const CPoint3f& maxPoint)
{
    m_maxPoint = maxPoint;
    validate();
}
//---------------------------------------------------------------------------------------

CPoint3f CAxisOrientedBox::getMinCorner() const
{
    return m_minPoint;
}
//---------------------------------------------------------------------------------------

CPoint3f CAxisOrientedBox::getMaxCorner() const
{
    return m_maxPoint;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::IsValid() const
{
    return !((m_minPoint == ORIGIN) && (m_maxPoint == ORIGIN));
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedBox::getWidth() const
{
    return (m_maxPoint.x - m_minPoint.x);
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedBox::getHeight() const
{
    return (m_maxPoint.y - m_minPoint.y);
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedBox::getDepth() const
{
    return (m_maxPoint.z - m_minPoint.z);
}
//---------------------------------------------------------------------------------------

CPoint3f CAxisOrientedBox::getCentre() const
{
    return ((m_minPoint + m_maxPoint) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedBox::getVolume() const
{
    return ( (m_maxPoint.x - m_minPoint.x) *
             (m_maxPoint.y - m_minPoint.y) *
             (m_maxPoint.z - m_minPoint.z) );
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedBox::getSurfaceArea() const
{
    AReal32 w = m_maxPoint.x - m_minPoint.x;
    AReal32 h = m_maxPoint.y - m_minPoint.y;
    AReal32 d = m_maxPoint.z - m_minPoint.z;
    return (2.0f * ((w*h) + (h*d) + (d*w)));
}
//---------------------------------------------------------------------------------------

CPoint3f* CAxisOrientedBox::getCorners() const
{
    CPoint3f* pCorners = new CPoint3f[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    pCorners[0] = CPoint3f(x1, y1, z1);
    pCorners[1] = CPoint3f(x1, y2, z1);
    pCorners[2] = CPoint3f(x1, y2, z2);
    pCorners[3] = CPoint3f(x1, y1, z2);
    pCorners[4] = CPoint3f(x2, y1, z1);
    pCorners[5] = CPoint3f(x2, y2, z1);
    pCorners[6] = CPoint3f(x2, y2, z2);
    pCorners[7] = CPoint3f(x2, y1, z2);

    return pCorners;
}
//---------------------------------------------------------------------------------------

CLineSegment* CAxisOrientedBox::getEdges() const
{
    CLineSegment* pEdges = new CLineSegment[12];

    CPoint3f pts[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    pts[0] = CPoint3f(x1, y1, z1);
    pts[1] = CPoint3f(x1, y2, z1);
    pts[2] = CPoint3f(x1, y2, z2);
    pts[3] = CPoint3f(x1, y1, z2);
    pts[4] = CPoint3f(x2, y1, z1);
    pts[5] = CPoint3f(x2, y2, z1);
    pts[6] = CPoint3f(x2, y2, z2);
    pts[7] = CPoint3f(x2, y1, z2);

    pEdges[0]  = CLineSegment(pts[0], pts[1]);
    pEdges[1]  = CLineSegment(pts[1], pts[2]);
    pEdges[2]  = CLineSegment(pts[2], pts[3]);
    pEdges[3]  = CLineSegment(pts[3], pts[0]);

    pEdges[4]  = CLineSegment(pts[4], pts[5]);
    pEdges[5]  = CLineSegment(pts[5], pts[6]);
    pEdges[6]  = CLineSegment(pts[6], pts[7]);
    pEdges[7]  = CLineSegment(pts[7], pts[4]);

    pEdges[8]  = CLineSegment(pts[0], pts[4]);
    pEdges[9]  = CLineSegment(pts[1], pts[5]);
    pEdges[10] = CLineSegment(pts[2], pts[6]);
    pEdges[11] = CLineSegment(pts[3], pts[7]);

    return pEdges;
}
//---------------------------------------------------------------------------------------

CPointTriangle* CAxisOrientedBox::getFaceTriangles() const
{
    CPoint3f c[8];

    //XY-plane through origin
    c[0] = m_minPoint;
    c[1] = m_minPoint + (X_AXIS * getWidth());
    c[2] = m_minPoint + (X_AXIS * getWidth()) + (Y_AXIS * getHeight());
    c[3] = m_minPoint + (Y_AXIS * getHeight());

    //Plane parallel to XY-plane through origin at offset
    CVector3f offset = Z_AXIS * getDepth();

    c[4] = c[0] + offset;
    c[5] = c[1] + offset;
    c[6] = c[2] + offset;
    c[7] = c[3] + offset;

    CPointTriangle* tris = new CPointTriangle[12];

    //Face 1 - 0, 1, 2, 3
    tris[0]  = CPointTriangle(c[0], c[1], c[2]);
    tris[1]  = CPointTriangle(c[2], c[3], c[0]);

    //Face 2 - 4, 5, 6, 7
    tris[2]  = CPointTriangle(c[6], c[5], c[4]);
    tris[3]  = CPointTriangle(c[4], c[7], c[6]);

    //Face 3 - 0, 1, 5, 4
    tris[4]  = CPointTriangle(c[5], c[1], c[0]);
    tris[5]  = CPointTriangle(c[0], c[4], c[5]);

    //Face 4 - 1, 2, 6, 5
    tris[6]  = CPointTriangle(c[6], c[2], c[1]);
    tris[7]  = CPointTriangle(c[1], c[5], c[6]);

    //Face 5 - 2, 3, 7, 6
    tris[8]  = CPointTriangle(c[7], c[3], c[2]);
    tris[9]  = CPointTriangle(c[2], c[6], c[7]);

    //Face 6 - 3, 0, 4, 7
    tris[10] = CPointTriangle(c[4], c[0], c[3]);
    tris[11] = CPointTriangle(c[3], c[7], c[4]);

    return tris;
}
//---------------------------------------------------------------------------------------

//! The normals of these planes pointing outwards of the box

CPlane* An::Maths::CAxisOrientedBox::getPlanes() const
{
    CPlane* planes = new CPlane[6];

    planes[0] = CPlane(m_minPoint, -X_AXIS);
    planes[1] = CPlane(m_minPoint, -Y_AXIS);
    planes[2] = CPlane(m_minPoint, -Z_AXIS);
    planes[3] = CPlane(m_maxPoint,  X_AXIS);
    planes[4] = CPlane(m_maxPoint,  Y_AXIS);
    planes[5] = CPlane(m_maxPoint,  Z_AXIS);

    return planes;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::isIntersecting(const CPlane* pPlane) const
{
    if(!pPlane)
    {
        return false;
    }
    CLineSegment* pEdges = getEdges();
    if(!pEdges)
    {
        return false;
    }
    bool bStatus = false;
    for(AIndex16 i = 0; i < 12; i++)
    {
        bStatus = pEdges->isIntersecting(pPlane);
        if(bStatus)
        {
            break;
        }
    }
    SAFE_ARRAY_CLEANUP(pEdges);
    return bStatus;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CAxisOrientedBox::getIntersections(const CPlane* pPlane) const
{
    if(!pPlane)
    {
        return NULL;
    }
    CLineSegment* pEdges = getEdges();
    if(!pEdges)
    {
        return NULL;
    }

    std::vector<SPointIntersection> hits;
    CPoint3f hitPoint;

    for(AIndex16 i = 0; i < 12; i++)
    {
        if(pEdges->isIntersecting(pPlane, hitPoint))
        {
            SPointIntersection spi;
            spi.m_hitPrimitiveIndex = i;
            spi.m_intersectionPoint = hitPoint;
            hits.push_back(spi);
        }
    }
    SAFE_ARRAY_CLEANUP(pEdges);

    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::isIntersecting(const CLine* pLine) const
{
    if(!pLine)
    {
        return false;
    }
    CPlane facePlanes[6]; //6 planes for 6 faces of the box
    getFacePlanes((CPlane*&) facePlanes);

    CPoint3f intersectionPoint;

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, TOLERANCE_32))
            {
                return true;
            }
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo* CAxisOrientedBox::getIntersections(const CLine* pLine) const
{
    if(!pLine)
    {
        return NULL;
    }
    CPlane facePlanes[6]; //6 planes for 6 faces of the box
    getFacePlanes((CPlane*&) facePlanes);

    CPoint3f intersectionPoint;
    std::vector<SPointIntersection> hits;

    for(AInt16 i = 0; i < 6; i++)
    {
        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
        {
            if(isInside(intersectionPoint, TOLERANCE_32))
            {
                SPointIntersection spi;
                spi.m_hitPrimitiveIndex = i;
                spi.m_intersectionPoint = intersectionPoint;
                hits.push_back(spi);
            }
        }
    }
    AUInt32 intersectionCount = (AUInt32) hits.size();
    if(intersectionCount)
    {
        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
        return pInfo;
    }
    return NULL;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::isInside(const CPoint3f& point, const AReal32 tolerance) const
{
    AReal32 x = point.x;
    AReal32 y = point.y;
    AReal32 z = point.z;

    if( GTEQT(x, m_minPoint.x, tolerance) && LTEQT(x, m_maxPoint.x, tolerance) &&
        GTEQT(y, m_minPoint.y, tolerance) && LTEQT(y, m_maxPoint.y, tolerance) &&
        GTEQT(z, m_minPoint.z, tolerance) && LTEQT(z, m_maxPoint.z, tolerance))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

CBox* CAxisOrientedBox::transform(const CMatrix4f& matrix) const
{
    AReal32 width = getWidth();
    AReal32 height = getHeight();
    AReal32 depth = getDepth();
    CPoint3f boxOrigin = matrix.transform(m_minPoint);
    CVector3f xAxis = matrix.rotate(X_AXIS);
    CVector3f yAxis = matrix.rotate(Y_AXIS);

    return new CBox(boxOrigin, xAxis, yAxis, width, height, depth);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CAxisOrientedBox& CAxisOrientedBox::operator = (const CAxisOrientedBox& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CAxisOrientedBox::operator == (const CAxisOrientedBox& box) const
{
    if((m_minPoint == box.m_minPoint) && (m_maxPoint == box.m_maxPoint))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::operator != (const CAxisOrientedBox& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

//Addition Operators

CAxisOrientedBox CAxisOrientedBox::operator + (const CAxisOrientedBox& box) const
{
    if(m_minPoint == ORIGIN && m_maxPoint == ORIGIN)
    {
        //This is uninitialized box

        CPoint3f min = box.m_minPoint;
        CPoint3f max = box.m_maxPoint;

        return CAxisOrientedBox(min, max);
    }

    AReal32 minX =  m_minPoint.x;
    AReal32 minY =  m_minPoint.y;
    AReal32 minZ =  m_minPoint.z;

    AReal32 maxX =  m_maxPoint.x;
    AReal32 maxY =  m_maxPoint.y;
    AReal32 maxZ =  m_maxPoint.z;

    if(minX > box.m_minPoint.x)    minX = box.m_minPoint.x;
    if(minY > box.m_minPoint.y)    minY = box.m_minPoint.y;
    if(minZ > box.m_minPoint.z)    minZ = box.m_minPoint.z;

    if(maxX < box.m_maxPoint.x)    maxX = box.m_maxPoint.x;
    if(maxY < box.m_maxPoint.y)    maxY = box.m_maxPoint.y;
    if(maxZ < box.m_maxPoint.z)    maxZ = box.m_maxPoint.z;

    CPoint3f min = CPoint3f(minX, minY, minZ);
    CPoint3f max = CPoint3f(maxX, maxY, maxZ);

    return CAxisOrientedBox(min, max);
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox& CAxisOrientedBox::operator += (const CAxisOrientedBox& box)
{
    if(m_minPoint == ORIGIN && m_maxPoint == ORIGIN)
    {
        //This is uninitialized box
        m_minPoint = box.m_minPoint;
        m_maxPoint = box.m_maxPoint;
    }
    else
    {
        AReal32 minX =  m_minPoint.x;
        AReal32 minY =  m_minPoint.y;
        AReal32 minZ =  m_minPoint.z;

        AReal32 maxX =  m_maxPoint.x;
        AReal32 maxY =  m_maxPoint.y;
        AReal32 maxZ =  m_maxPoint.z;

        if(minX > box.m_minPoint.x)     minX = box.m_minPoint.x;
        if(minY > box.m_minPoint.y)     minY = box.m_minPoint.y;
        if(minZ > box.m_minPoint.z)     minZ = box.m_minPoint.z;

        if(maxX < box.m_maxPoint.x)     maxX = box.m_maxPoint.x;
        if(maxY < box.m_maxPoint.y)     maxY = box.m_maxPoint.y;
        if(maxZ < box.m_maxPoint.z)     maxZ = box.m_maxPoint.z;

        m_minPoint = CPoint3f(minX, minY, minZ);
        m_maxPoint = CPoint3f(maxX, maxY, maxZ);
    }

    return *this;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedBox::operator < (const CAxisOrientedBox& box) const
{
    if(m_minPoint < box.m_minPoint)      return true;
    if(m_minPoint > box.m_minPoint)      return false;
    if(m_maxPoint < box.m_maxPoint)      return true;
    if(m_maxPoint > box.m_maxPoint)      return false;

    return false;
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::validate()
{
    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 z1 = m_minPoint.z;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;
    AReal32 z2 = m_maxPoint.z;

    if(x1 > x2)
    {
        m_minPoint.x = x2;
        m_maxPoint.x = x1;
    }
    if(y1 > y2)
    {
        m_minPoint.y = y2;
        m_maxPoint.y = y1;
    }
    if(z1 > z2)
    {
        m_minPoint.z = z2;
        m_maxPoint.z = z1;
    }
}
//---------------------------------------------------------------------------------------

void CAxisOrientedBox::getFacePlanes(CPlane*& facePlanes) const
{
    facePlanes[0] = CPlane(m_minPoint, -X_AXIS);
    facePlanes[1] = CPlane(m_minPoint, -Y_AXIS);
    facePlanes[2] = CPlane(m_minPoint, -Z_AXIS);
    facePlanes[3] = CPlane(m_maxPoint, X_AXIS);
    facePlanes[4] = CPlane(m_maxPoint, Y_AXIS);
    facePlanes[5] = CPlane(m_maxPoint, Z_AXIS);
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox CAxisOrientedBox::CalculateBox(const Maths::CPoint3f* points, const AInt32& pointCount)
{
    AReal32 minX =  FLT_MAX;
    AReal32 minY =  FLT_MAX;
    AReal32 minZ =  FLT_MAX;

    AReal32 maxX =  -FLT_MAX;
    AReal32 maxY =  -FLT_MAX;
    AReal32 maxZ =  -FLT_MAX;

    for(AInt32 i = 0; i < pointCount; i++)
    {
        if(minX > points[i].x)     minX = points[i].x;
        if(minY > points[i].y)     minY = points[i].y;
        if(minZ > points[i].z)     minZ = points[i].z;

        if(maxX < points[i].x)     maxX = points[i].x;
        if(maxY < points[i].y)     maxY = points[i].y;
        if(maxZ < points[i].z)     maxZ = points[i].z;
    }

    return CAxisOrientedBox(CPoint3f(minX, minY, minZ), CPoint3f(maxX, maxY, maxZ));
}
//---------------------------------------------------------------------------------------

CAxisOrientedBox CAxisOrientedBox::CalculateBox(const Maths::CPoint3fList points)
{
    AInt32 pointCount = points.size();

    AReal32 minX =  FLT_MAX;
    AReal32 minY =  FLT_MAX;
    AReal32 minZ =  FLT_MAX;

    AReal32 maxX =  -FLT_MAX;
    AReal32 maxY =  -FLT_MAX;
    AReal32 maxZ =  -FLT_MAX;

    for(AInt32 i = 0; i < pointCount; i++)
    {
        if(minX > points[i].x)     minX = points[i].x;
        if(minY > points[i].y)     minY = points[i].y;
        if(minZ > points[i].z)     minZ = points[i].z;

        if(maxX < points[i].x)     maxX = points[i].x;
        if(maxY < points[i].y)     maxY = points[i].y;
        if(maxZ < points[i].z)     maxZ = points[i].z;
    }

    return CAxisOrientedBox(CPoint3f(minX, minY, minZ), CPoint3f(maxX, maxY, maxZ));
}
//---------------------------------------------------------------------------------------

