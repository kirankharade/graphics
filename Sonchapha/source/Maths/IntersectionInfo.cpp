//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "IntersectionInfo.h"
#include <cstring>

using namespace An;
using namespace An::Maths;
using namespace std;


//---------------------------------------------------------------------------------------

CIntersectionInfo::CIntersectionInfo()
{
    m_intersectionCount = 0;
}
//---------------------------------------------------------------------------------------

AInt32 CIntersectionInfo::getIntersectionCount() const
{
    return m_intersectionCount;
}
//---------------------------------------------------------------------------------------

const SPointIntersection* CPointIntersectionInfo::getIntersections() const
{
    return m_pIntersections;
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo::CPointIntersectionInfo()
:   CIntersectionInfo()
  , m_pIntersections(NULL)
{
}
//---------------------------------------------------------------------------------------

CPointIntersectionInfo::CPointIntersectionInfo(const AIndex32& intersectionCount, const SPointIntersection* pIntersections)
:   CIntersectionInfo()
  , m_pIntersections(NULL)
{
    if(!pIntersections || (0 == intersectionCount))
    {
        m_intersectionCount = 0;
    }
    m_intersectionCount = intersectionCount;
    m_pIntersections = new SPointIntersection[m_intersectionCount];
    memcpy(m_pIntersections, pIntersections, m_intersectionCount * sizeof(SPointIntersection));
}
//---------------------------------------------------------------------------------------

void CPointIntersectionInfo::setIntersections(const AIndex32& intersectionCount, const SPointIntersection* pIntersections)
{
    if(m_pIntersections)
    {
        SAFE_ARRAY_CLEANUP(m_pIntersections);
    }
    m_intersectionCount = intersectionCount;
    m_pIntersections = new SPointIntersection[m_intersectionCount];
    memcpy(m_pIntersections, pIntersections, m_intersectionCount * sizeof(SPointIntersection));
}
//---------------------------------------------------------------------------------------

AInt32 CPointIntersectionInfo::getHitPrimitiveIndex(const AIndex32& index) const
{
    if(index < 0 || (index >= m_intersectionCount) || !m_pIntersections)
    {
        return -1;
    }
    return m_pIntersections[index].m_hitPrimitiveIndex;
}
//---------------------------------------------------------------------------------------

CPoint3f CPointIntersectionInfo::getIntersectionPoint(const AIndex32& index) const
{
    if(index < 0 || (index >= m_intersectionCount) || !m_pIntersections)
    {
        return CPoint3f();
    }
    return m_pIntersections[index].m_intersectionPoint;
}
//---------------------------------------------------------------------------------------

