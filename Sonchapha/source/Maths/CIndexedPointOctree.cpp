//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CBox.h"
#include "CIndexedPointOctree.h"
#include "CRay.h"
#include "CLine.h"
#include "CPlane.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;
/*
//---------------------------------------------------------------------------------------
const static AReal32 DEFAULT_SIZE = 10.0f;
//---------------------------------------------------------------------------------------

CBox::CBox()
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(DEFAULT_SIZE)
 ,m_height(DEFAULT_SIZE)
 ,m_depth(DEFAULT_SIZE)
{
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
     const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(origin)
 ,m_xAxis(xDirection)
 ,m_yAxis(yDirection)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(origin)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
}
//---------------------------------------------------------------------------------------

CBox::CBox(const AReal32& width, const AReal32& height, const AReal32& depth)
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(width)
 ,m_height(height)
 ,m_depth(depth)
{
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size)
: m_origin(origin)
 ,m_xAxis(xDirection)
 ,m_yAxis(yDirection)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
    m_xAxis.normalize();
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CPoint3f& origin, const AReal32& size)
: m_origin(origin)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
}
//---------------------------------------------------------------------------------------

CBox::CBox(const AReal32& size)
: m_origin(ORIGIN)
 ,m_xAxis(X_AXIS)
 ,m_yAxis(Y_AXIS)
 ,m_width(size)
 ,m_height(size)
 ,m_depth(size)
{
}
//---------------------------------------------------------------------------------------

CBox::CBox(const CBox& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;
}
//---------------------------------------------------------------------------------------

CBox::~CBox()
{
}
//---------------------------------------------------------------------------------------

void CBox::set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection,
         const AReal32& width, const AReal32& height, const AReal32& depth)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = width;
    m_height = height;
    m_depth = depth;

    m_xAxis.normalize();
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::set(const CPoint3f& origin, const CVector3f& xDirection, const CVector3f& yDirection, const AReal32& size)
{
    m_origin = origin;
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_width = size;
    m_height = size;
    m_depth = size;

    m_xAxis.normalize();
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setOrigin(const CPoint3f& origin)
{
    m_origin = origin;
}
//---------------------------------------------------------------------------------------

void CBox::setDirections(const CVector3f& xDirection, const CVector3f& yDirection)
{
    m_xAxis = xDirection;
    m_yAxis = yDirection;
    m_xAxis.normalize();
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setXDirection(const CVector3f& xDirection)
{
    m_xAxis = xDirection;
    m_xAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setYDirection(const CVector3f& yDirection)
{
    m_yAxis = yDirection;
    m_yAxis.normalize();
}
//---------------------------------------------------------------------------------------

void CBox::setSize(const AReal32& size)
{
    m_width = size;
    m_height = size;
    m_depth = size;
}
//---------------------------------------------------------------------------------------

void CBox::setWidth(const AReal32& width)
{
    m_width = width;
}
//---------------------------------------------------------------------------------------

void CBox::setHeight(const AReal32& height)
{
    m_height = height;
}
//---------------------------------------------------------------------------------------

void CBox::setDepth(const AReal32& depth)
{
    m_depth = depth;
}
//---------------------------------------------------------------------------------------

CPoint3f CBox::getOrigin() const
{
    return m_origin;
}
//---------------------------------------------------------------------------------------

CVector3f CBox::getXDirection() const
{
    return m_xAxis;
}
//---------------------------------------------------------------------------------------

CVector3f CBox::getYDirection() const
{
    return m_yAxis;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getWidth() const
{
    return m_width;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getHeight() const
{
    return m_height;
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getDepth() const
{
    return m_depth;
}
//---------------------------------------------------------------------------------------

CPoint3f CBox::getCentre() const
{
    CVector3f zAxis = m_xAxis.cross(m_yAxis);
    zAxis.normalize();

    CPoint3f p1 = m_origin + (m_xAxis * m_width);
    CPoint3f p2 = p1 + (m_yAxis * m_height);
    CPoint3f p3 = p2 + (zAxis * m_depth);

    return ((m_origin+ p3) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getVolume() const
{
    return (m_width * m_height * m_depth);
}
//---------------------------------------------------------------------------------------

AReal32 CBox::getSurfaceArea() const
{
    return (2.0f * ((m_width * m_height) + (m_height * m_depth) + (m_width * m_depth)));
}
//---------------------------------------------------------------------------------------

void CBox::getCorners(CPoint3f*& corners) const
{
}
//---------------------------------------------------------------------------------------

void CBox::getEdges(CLineSegment*& edges) const
{
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CPlane* pPlane) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLine* pLine) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLine* pLine, CPoint3f& intersectionPoint) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLineSegment* pLineSegment) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CLineSegment* pLineSegment, CPoint3f& intersectionPoint) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CRay* pRay) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isIntersecting(const CRay* pRay, CPoint3f& intersectionPoint) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::isInside(const CPoint3f& point) const
{
    return false;
}
//---------------------------------------------------------------------------------------

CBox CBox::transform(const CMatrix4f& matrix) const
{
    return *this;
}
//---------------------------------------------------------------------------------------

//Assignment operator

CBox& CBox::operator = (const CBox& box)
{
    m_origin = box.m_origin;
    m_xAxis = box.m_xAxis;
    m_yAxis = box.m_yAxis;
    m_width = box.m_width;
    m_height = box.m_height;
    m_depth = box.m_depth;

    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CBox::operator == (const CBox& box) const
{
    if((m_origin == box.m_origin) &&
       (m_xAxis == box.m_xAxis) &&
       (m_yAxis == box.m_yAxis) &&
       EQ_32(m_width, box.m_width) &&
       EQ_32(m_height, box.m_height) &&
       EQ_32(m_depth, box.m_depth))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CBox::operator != (const CBox& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

bool CBox::operator < (const CBox& box) const
{
    if(m_origin < box.m_origin)      return true;
    if(m_origin > box.m_origin)      return false;
    if(m_xAxis < box.m_xAxis)        return true;
    if(m_xAxis > box.m_xAxis)        return false;
    if(m_yAxis < box.m_yAxis)        return true;
    if(m_yAxis > box.m_yAxis)        return false;

    if(LT_32(m_width, box.m_width))   return true;
    if(GT_32(m_width, box.m_width))   return false;
    if(LT_32(m_height, box.m_height)) return true;
    if(GT_32(m_height, box.m_height)) return false;
    if(LT_32(m_depth, box.m_depth))   return true;
    if(GT_32(m_depth, box.m_depth))   return false;

    return false;
}
//---------------------------------------------------------------------------------------
*/
