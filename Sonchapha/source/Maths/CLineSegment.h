//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_LINE_SEGMENT_H__
#define __C_LINE_SEGMENT_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CVector3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CLineSegment
{
    protected:
        
        CPoint3f    m_firstPoint;
        CPoint3f    m_secondPoint;

    public:
        
        CLineSegment();

        CLineSegment(const CPoint3f& firstPoint, const CVector3f& direction, const AReal32& length);
    
        CLineSegment(const CPoint3f& firstPoint, const CPoint3f& secondPoint);
    
        CLineSegment(const CLineSegment& lineSegment);
    
        virtual ~CLineSegment();
    
        void set(const CPoint3f& firstPoint, const CVector3f& direction, const AReal32& length);
    
        virtual void set(const CPoint3f& firstPoint, const CPoint3f& secondPoint);

        CPoint3f getFirstPoint() const;
       
        CPoint3f getSecondPoint() const;
       
        CVector3f getDirection() const;

        AReal32 getLength() const;

        bool isIntersecting(const CPlane* pPlane) const;
       
        bool isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const;

        bool passesThrough(const CPoint3f& point, const AReal32 tolerance = TOLERANCE_32) const;
       
        CPoint3f closestPoint(const CPoint3f& point) const;
       
        AReal32 distFromPoint(const CPoint3f& point) const;

        //Assignment operator

        CLineSegment& operator = (const CLineSegment& lineSegment);
    
        //Comparison Operators
         
        bool operator == (const CLineSegment& lineSegment) const;
    
        bool operator != (const CLineSegment& lineSegment) const;
    
        bool operator < (const CLineSegment& lineSegment) const;
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_LINE_SEGMENT_H__
//---------------------------------------------------------------------------------------

