//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CConverter.h"

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CPoint2f CConverter::convertTo2f(const CPoint2i& point)
{
    return CPoint2f((AReal32) point.x, (AReal32) point.y);
}
//---------------------------------------------------------------------------------------

CPoint2i CConverter::convertTo2i(const CPoint2f& point)
{
    return CPoint2i((AInt32) point.x, (AInt32) point.y);
}
//---------------------------------------------------------------------------------------

CPoint2f CConverter::convertTo2f(const CPoint3f& point)
{
    return CPoint2f((AReal32) point.x, (AReal32) point.y);
}
//---------------------------------------------------------------------------------------

CPoint2i CConverter::convertTo2i(const CPoint3f& point)
{
    return CPoint2i((AInt32) point.x, (AInt32) point.y);
}
//---------------------------------------------------------------------------------------

CPoint3f CConverter::convertTo3f(const CPoint2f& point)
{
    return CPoint3f((AReal32) point.x, (AReal32) point.y, 0);
}
//---------------------------------------------------------------------------------------

CPoint3f CConverter::convertTo3f(const CPoint2i& point)
{
    return CPoint3f((AReal32) point.x, (AReal32) point.y, 0);
}
//---------------------------------------------------------------------------------------

std::vector<CPoint2f> CConverter::convertTo2f(const std::vector<CPoint2i>& points)
{
    std::vector<CPoint2f> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint2f((AReal32) points[i].x, (AReal32) points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<CPoint2f> CConverter::convertTo2f(const std::vector<CPoint3f>& points)
{
    std::vector<CPoint2f> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint2f((AReal32) points[i].x, (AReal32) points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<CPoint2i> CConverter::convertTo2i(const std::vector<CPoint2f>& points)
{
    std::vector<CPoint2i> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint2i((AInt32) points[i].x, (AInt32) points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<CPoint2i> CConverter::convertTo2i(const std::vector<CPoint3f>& points)
{
    std::vector<CPoint2i> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint2i((AInt32) points[i].x, (AInt32) points[i].y);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<CPoint3f> CConverter::convertTo3f(const std::vector<CPoint2f>& points)
{
    std::vector<CPoint3f> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint3f((AReal32) points[i].x, (AReal32) points[i].y, 0);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------

std::vector<CPoint3f> CConverter::convertTo3f(const std::vector<CPoint2i>& points)
{
    std::vector<CPoint3f> out;
    AUInt32 count = points.size();

    if(count > 0)
    {
        out.resize(count);
        for(AUInt32 i = 0; i < count; i++)
        {
            out[i] = CPoint3f((AReal32) points[i].x, (AReal32) points[i].y, 0);
        }
    }

    return out;
}
//---------------------------------------------------------------------------------------



