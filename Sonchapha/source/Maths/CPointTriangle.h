//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_TRIANGLE_H__
#define __C_TRIANGLE_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class CLineSegment;
class CLine;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CPointTriangle
{
    public:
        
        CPoint3f m_vtx[3];

    public:
        
        CPointTriangle();
    
        CPointTriangle(const CPoint3f v[3]);
    
        CPointTriangle(const CPoint3f& a, const CPoint3f& b, const CPoint3f& c);
    
        CPointTriangle(const CPointTriangle& triangle);
    
        void set(const CPoint3f& a, const CPoint3f& b, const CPoint3f& c);
    
        void set(const CPointTriangle& triangle);
    
        CVector3f normal() const;
    
        AReal32 area() const;

        CPoint3f circumCenter() const;

        void reverseOrientation();

        bool isInside(const CPoint3f& point) const;

        bool isIntersecting(const CLineSegment* lineSegment) const;
    
        bool isIntersecting(const CLine* line) const;

        CLineSegment edge(const AIndex32& i) const;
    
        //Index Operators
    
        CPoint3f& operator [] (const AIndex16& i);
    
        const CPoint3f& operator [] (const AIndex32& i) const;
    
        //assignment operator

        CPointTriangle& operator = (const CPointTriangle triangle);
    
        //Comparison Operators
         
        bool operator == (const CPointTriangle& triangle) const;
    
        bool operator != (const CPointTriangle& triangle) const;
};
//---------------------------------------------------------------------------------------
//Exports specifiers for STL containers

#ifdef _AN_BUILD_MATHS_FOR_WINDOWS_
template class AN_MATHS_API std::allocator<CPointTriangle>;
template class AN_MATHS_API std::vector<CPointTriangle>;
#endif

typedef std::vector<CPointTriangle> CPointTriangleList;
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_TRIANGLE_H__
//---------------------------------------------------------------------------------------

