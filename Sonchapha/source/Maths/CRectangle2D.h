//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_RECTANGLE_2D_H__
#define __C_RECTANGLE_2D_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2i.h"
#include "CPoint2f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------
//Forward declarations
class CRectangle2Di;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CRectangle2D
{
    private:
        
        AReal32    m_left;
        AReal32    m_right;
        AReal32    m_top;
        AReal32    m_bottom;

        AReal32   m_zAxisRotationDeg;

    public:
        
        CRectangle2D();

        CRectangle2D(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom);
    
        CRectangle2D(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom);

        CRectangle2D(const CPoint2f& reference, const AReal32& width, const AReal32& height);
    
        CRectangle2D(const CPoint2i& reference, const AInt32& width, const AInt32& height);

        CRectangle2D(const CRectangle2D& rect);
    
        virtual ~CRectangle2D();
    
        void set(const AReal32& left, const AReal32& right, const AReal32& top, const AReal32& bottom);

        void set(const CPoint2f& reference, const AReal32& width, const AReal32& height);
    
        void set(const AInt32& left, const AInt32& right, const AInt32& top, const AInt32& bottom);

        void set(const CPoint2i& reference, const AInt32& width, const AInt32& height);

        AReal32 getLeft() const;
       
        AReal32 getRight() const;
       
        AReal32 getTop() const;
       
        AReal32 getBottom() const;
       
        CPoint2f getLeftTop() const;

        CPoint2f getLeftBottom() const;

        CPoint2f getRightBottom() const;

        CPoint2f getRightTop() const;

        AReal32 getArea() const;
       
        AReal32 getPerimeter() const;
       
        AReal32 getWidth() const;
       
        void setWidth(const AReal32& width);
       
        AReal32 getHeight() const;

        void setHeight(const AReal32& height);
       
        AReal32 getZAxisRotation() const;

        void setZAxisRotation(const AReal32& zRotationDeg);

        bool isInside(const CPoint2i& point) const;
       
        bool isInside(const CPoint2f& point) const;
       

        //Assignment operator

        CRectangle2D& operator = (const CRectangle2D& rect);
    
        //Comparison Operators
         
        bool operator == (const CRectangle2D& rect) const;
    
        bool operator != (const CRectangle2D& rect) const;

private:

    void init();

    void checkValidity();
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_RECTANGLE_2D_H__
//---------------------------------------------------------------------------------------
