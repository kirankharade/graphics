//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_VECTOR_3F_H__
#define __C_VECTOR_3F_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

#define X_AXIS CVector3f(1, 0, 0)
#define Y_AXIS CVector3f(0, 1, 0)
#define Z_AXIS CVector3f(0, 0, 1)  

//---------------------------------------------------------------------------------------

class AN_MATHS_API CVector3f
{
    public:
        
        union 
        {
            AReal32 v[3];

            struct 
            {
                AReal32 x;
                AReal32 y;
                AReal32 z;
            };
        };

    public:
        
        CVector3f();
    
        CVector3f(const AReal32 v[3]);
    
        CVector3f(const AReal32& a, const AReal32& b, const AReal32& c);
    
        CVector3f(const CVector3f& vec);
    
        void set(const AReal32& a, const AReal32& b, const AReal32& c);
    
        void set(const CVector3f& vec);
    
        void normalize();
    
        CVector3f unitVec() const;
    
        AReal32 magnitude() const;
    
        AReal32 length() const;
    
        AReal32 squareLength() const;
    
        CVector3f cross(const CVector3f& v) const;
    
        AReal32 dot(const CVector3f& v) const;
        
        AReal32 angleDeg(const CVector3f& v) const;

        AReal32 angleRad(const CVector3f& v) const;

        CVector3f reverse() const;
        
        bool isNull() const;
        
        //Index Operators
    
        AReal32& operator [] (const AIndex32& index);
    
        const AReal32& operator [] (const AIndex32& index) const;

        //assignment operator

        CVector3f& operator = (const CVector3f& v);
    
        //Comparison Operators
         
        bool operator == (const CVector3f& v) const;
    
        bool operator != (const CVector3f& v) const;
    
        bool operator < (const CVector3f& v) const;
        
        bool operator > (const CVector3f& v) const;
        
        //Summation and subtraction operators
    
        CVector3f operator + (const CVector3f& v) const;
    
        CVector3f& operator += (const CVector3f& v);
    
        CVector3f operator - (const CVector3f& v) const;
    
        CVector3f& operator -= (const CVector3f& v);
        
        //Operators with scalar arguments
    
        CVector3f operator * (const AReal32& scalar) const;
    
        CVector3f& operator *= (const AReal32& scalar);
    
        CVector3f operator / (const AReal32& scalar) const;
    
        CVector3f& operator /= (const AReal32& scalar);
    
        CVector3f operator - () const;

};
//---------------------------------------------------------------------------------------
typedef AN_MATHS_API std::vector<CVector3f>   CVector3fList;
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_VECTOR_3F_H__
//---------------------------------------------------------------------------------------
