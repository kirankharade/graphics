//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_MATH_SETTINGS_H__
#define __AN_MATH_SETTINGS_H__
//---------------------------------------------------------------------------------------
#include "MathsEnums.h"
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class CSettings
{
    public:

    static ECoordSystemHandedness s_eCoordSysOrientation;
    static EScreenConventionPosYAxis s_e2DYAxisConvention;

    static ECoordSystemHandedness& CoordSysOrientation();
    static EScreenConventionPosYAxis& YAxis2DConvention();
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_MATH_SETTINGS_H__
//---------------------------------------------------------------------------------------
