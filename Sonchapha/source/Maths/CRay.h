//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_RAY_H__
#define __C_RAY_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CVector3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CRay
{
    protected:
        
        CPoint3f    m_point;
        CVector3f   m_direction;

    public:
        
        CRay();

        CRay(const CPoint3f& point, const CVector3f& direction);
    
        CRay(const CPoint3f& firstPoint, const CPoint3f& secondPoint);
    
        CRay(const CRay& ray);
    
        virtual ~CRay();
    
        void set(const CPoint3f& point, const CVector3f& direction);

        void set(const CPoint3f& firstPoint, const CPoint3f& secondPoint);

        CPoint3f getPoint() const;

        CVector3f getDirection() const;

        bool isIntersecting(const CPlane* pPlane) const;
       
        bool isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const;

        bool passesThrough(const CPoint3f& point, const AReal32 tolerance = TOLERANCE_32) const;
       
        CPoint3f closestPoint(const CPoint3f& point) const;
       
        AReal32 distFromPoint(const CPoint3f& point) const;

        //Assignment operator

        CRay& operator = (const CRay& ray);
    
        //Comparison Operators
         
        bool operator == (const CRay& ray) const;
    
        bool operator != (const CRay& ray) const;
    
        bool operator < (const CRay& ray) const;
        
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_RAY_H__
//---------------------------------------------------------------------------------------

