//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CAxisOrientedRectangle.h"
#include "CBox.h"
#include "CLine.h"
#include "CLineSegment2D.h"
#include "CPointTriangle.h"
#include "IntersectionInfo.h"
#include "CConverter.h"
#include <vector>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CAxisOrientedRectangle::CAxisOrientedRectangle()
: m_minPoint(ORIGIN)
 ,m_maxPoint(ORIGIN)
{
}
//---------------------------------------------------------------------------------------

CAxisOrientedRectangle::CAxisOrientedRectangle(const CPoint2f& minPoint, const CPoint2f& maxPoint)
: m_minPoint(minPoint)
 ,m_maxPoint(maxPoint)
{
    validate();
}
//---------------------------------------------------------------------------------------

CAxisOrientedRectangle::CAxisOrientedRectangle(const CAxisOrientedRectangle& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
}
//---------------------------------------------------------------------------------------

CAxisOrientedRectangle::~CAxisOrientedRectangle()
{
}
//---------------------------------------------------------------------------------------

void CAxisOrientedRectangle::setMinCorner(const CPoint2f& minPoint)
{
    m_minPoint = minPoint;
    validate();
}
//---------------------------------------------------------------------------------------

void CAxisOrientedRectangle::setMaxCorner(const CPoint2f& maxPoint)
{
    m_maxPoint = maxPoint;
    validate();
}
//---------------------------------------------------------------------------------------

CPoint2f CAxisOrientedRectangle::getMinCorner() const
{
    return m_minPoint;
}
//---------------------------------------------------------------------------------------

CPoint2f CAxisOrientedRectangle::getMaxCorner() const
{
    return m_maxPoint;
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedRectangle::getWidth() const
{
    return (m_maxPoint.x - m_minPoint.x);
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedRectangle::getHeight() const
{
    return (m_maxPoint.y - m_minPoint.y);
}
//---------------------------------------------------------------------------------------

CPoint2f CAxisOrientedRectangle::getCentre() const
{
    return ((m_minPoint + m_maxPoint) * 0.5f);
}
//---------------------------------------------------------------------------------------

AReal32 CAxisOrientedRectangle::getSurfaceArea() const
{
    AReal32 w = m_maxPoint.x - m_minPoint.x;
    AReal32 h = m_maxPoint.y - m_minPoint.y;
    return (w*h);
}
//---------------------------------------------------------------------------------------

CPoint2f* CAxisOrientedRectangle::getCorners() const
{
    CPoint2f* pCorners = new CPoint2f[4];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;

    pCorners[0] = CPoint2f(x1, y1);
    pCorners[1] = CPoint2f(x2, y1);
    pCorners[2] = CPoint2f(x2, y2);
    pCorners[3] = CPoint2f(x1, y2);

    return pCorners;
}
//---------------------------------------------------------------------------------------

CLineSegment2D* CAxisOrientedRectangle::getEdges() const
{
    CLineSegment2D* pEdges = new CLineSegment2D[4];

    CPoint2f pts[8];

    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;

    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;


    pts[0] = CPoint2f(x1, y1);
    pts[1] = CPoint2f(x2, y1);
    pts[2] = CPoint2f(x2, y2);
    pts[3] = CPoint2f(x1, y2);

    pEdges[0]  = CLineSegment2D(pts[0], pts[1]);
    pEdges[1]  = CLineSegment2D(pts[1], pts[2]);
    pEdges[2]  = CLineSegment2D(pts[2], pts[3]);
    pEdges[3]  = CLineSegment2D(pts[3], pts[0]);

    return pEdges;
}
//---------------------------------------------------------------------------------------

CPointTriangle* CAxisOrientedRectangle::getTriangles() const
{
    CPoint3f c[4];

    //XY-plane through origin
    c[0] = CConverter::convertTo3f(m_minPoint);
    c[1] = CConverter::convertTo3f(m_minPoint + (X_AXIS * getWidth()));
    c[2] = CConverter::convertTo3f(m_minPoint + (X_AXIS * getWidth()) + (Y_AXIS * getHeight()));
    c[3] = CConverter::convertTo3f(m_minPoint + (Y_AXIS * getHeight()));

    CPointTriangle* tris = new CPointTriangle[2];

    //Face 1 - 0, 1, 2, 3
    tris[0]  = CPointTriangle(CConverter::convertTo3f(c[0]), CConverter::convertTo3f(c[1]), CConverter::convertTo3f(c[2]));
    tris[1]  = CPointTriangle(CConverter::convertTo3f(c[2]), CConverter::convertTo3f(c[3]), CConverter::convertTo3f(c[0]));

    return tris;
}
//---------------------------------------------------------------------------------------


bool CAxisOrientedRectangle::isInside(const CPoint2f& point, const AReal32 tolerance) const
{
    AReal32 x = point.x;
    AReal32 y = point.y;

    if( GTEQT(x, m_minPoint.x, tolerance) && LTEQT(x, m_maxPoint.x, tolerance) &&
        GTEQT(y, m_minPoint.y, tolerance) && LTEQT(y, m_maxPoint.y, tolerance))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

//Assignment operator

CAxisOrientedRectangle& CAxisOrientedRectangle::operator = (const CAxisOrientedRectangle& box)
{
    m_minPoint = box.m_minPoint;
    m_maxPoint = box.m_maxPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CAxisOrientedRectangle::operator == (const CAxisOrientedRectangle& box) const
{
    if((m_minPoint == box.m_minPoint) && (m_maxPoint == box.m_maxPoint))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedRectangle::operator != (const CAxisOrientedRectangle& box) const
{
    return !(*this == box);
}
//---------------------------------------------------------------------------------------

bool CAxisOrientedRectangle::operator < (const CAxisOrientedRectangle& box) const
{
    if(m_minPoint < box.m_minPoint)      return true;
    if(m_minPoint > box.m_minPoint)      return false;
    if(m_maxPoint < box.m_maxPoint)      return true;
    if(m_maxPoint > box.m_maxPoint)      return false;
    return false;
}
//---------------------------------------------------------------------------------------

void CAxisOrientedRectangle::validate()
{
    AReal32 x1 = m_minPoint.x;
    AReal32 y1 = m_minPoint.y;
    AReal32 x2 = m_maxPoint.x;
    AReal32 y2 = m_maxPoint.y;

    if(x1 > x2)
    {
        m_minPoint.x = x2;
        m_maxPoint.x = x1;
    }
    if(y1 > y2)
    {
        m_minPoint.y = y2;
        m_maxPoint.y = y1;
    }
}
//---------------------------------------------------------------------------------------

//bool CAxisOrientedRectangle::isIntersecting(const CLine* pLine) const
//{
//    if(!pLine)
//    {
//        return false;
//    }
//    CPlane facePlanes[6]; //6 planes for 6 faces of the box
//    getFacePlanes((CPlane*&) facePlanes);
//
//    CPoint2f intersectionPoint;
//
//    for(AInt16 i = 0; i < 6; i++)
//    {
//        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
//        {
//            if(isInside(intersectionPoint, TOLERANCE_32))
//            {
//                return true;
//            }
//        }
//    }
//    return false;
//}
////---------------------------------------------------------------------------------------
//
//CPointIntersectionInfo* CAxisOrientedRectangle::getIntersections(const CLine* pLine) const
//{
//    if(!pLine)
//    {
//        return NULL;
//    }
//    CPlane facePlanes[6]; //6 planes for 6 faces of the box
//    getFacePlanes((CPlane*&) facePlanes);
//
//    CPoint2f intersectionPoint;
//    std::vector<SPointIntersection> hits;
//
//    for(AInt16 i = 0; i < 6; i++)
//    {
//        if(pLine->isIntersecting(&facePlanes[i], intersectionPoint))
//        {
//            if(isInside(intersectionPoint, TOLERANCE_32))
//            {
//                SPointIntersection spi;
//                spi.m_hitPrimitiveIndex = i;
//                spi.m_intersectionPoint = intersectionPoint;
//                hits.push_back(spi);
//            }
//        }
//    }
//    AUInt32 intersectionCount = (AUInt32) hits.size();
//    if(intersectionCount)
//    {
//        CPointIntersectionInfo* pInfo = new CPointIntersectionInfo(intersectionCount, &hits[0]);
//        return pInfo;
//    }
//    return NULL;
//}
//---------------------------------------------------------------------------------------

