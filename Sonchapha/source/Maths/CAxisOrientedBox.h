//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_AXIS_ORIENTED_BOX_H__
#define __C_AXIS_ORIENTED_BOX_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include "CVector3f.h"
#include "CMatrix4f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
class CLine;
class CRay;
class CLineSegment;
class CBox;
class CPointIntersectionInfo;
class CPointTriangle;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CAxisOrientedBox
{
    protected:
        
        CPoint3f    m_minPoint;
        CPoint3f    m_maxPoint;

    public:
        
        CAxisOrientedBox();

        CAxisOrientedBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);
    
        CAxisOrientedBox(const CPoint3f& minPoint, const CPoint3f& maxPoint);
    
        CAxisOrientedBox(const CPoint3f& origin, const AReal32& size);

        CAxisOrientedBox(const CAxisOrientedBox& box);
    
        virtual ~CAxisOrientedBox();
    
        void set(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);
    
        void set(const CPoint3f& origin, const AReal32& size);

        void setMinCorner(const CPoint3f& minPoint);

        void setMaxCorner(const CPoint3f& maxPoint);

        CPoint3f getMinCorner() const;

        CPoint3f getMaxCorner() const;

        bool IsValid() const;

        AReal32 getWidth() const;

        AReal32 getHeight() const;

        AReal32 getDepth() const;

        CPoint3f getCentre() const;

        AReal32 getVolume() const;

        AReal32 getSurfaceArea() const;

        CPoint3f* getCorners() const;

        CLineSegment* getEdges() const;

        CPointTriangle* getFaceTriangles() const;

        CPlane* getPlanes() const;

        bool isIntersecting(const CPlane* pPlane) const;
       
        CPointIntersectionInfo* getIntersections(const CPlane* pPlane) const;

        bool isIntersecting(const CLine* pLine) const;

        CPointIntersectionInfo* getIntersections(const CLine* pLine) const;

        bool isInside(const CPoint3f& point, const AReal32 tolerance = TOLERANCE_32) const;

        CBox* transform(const CMatrix4f& matrix) const;

        //Assignment operator

        CAxisOrientedBox& operator = (const CAxisOrientedBox& box);
    
        //Comparison Operators
         
        bool operator == (const CAxisOrientedBox& box) const;
    
        bool operator != (const CAxisOrientedBox& box) const;
    
        bool operator < (const CAxisOrientedBox& box) const;
  
        //Addition operators

        CAxisOrientedBox operator + (const CAxisOrientedBox& box) const;

        CAxisOrientedBox& operator += (const CAxisOrientedBox& box);

        static CAxisOrientedBox CalculateBox(const Maths::CPoint3f* points, const AInt32& pointCount);

        static CAxisOrientedBox CalculateBox(const Maths::CPoint3fList points);

    protected:

        void validate();

        void getFacePlanes(CPlane*& facePlanes) const;

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_AXIS_ORIENTED_BOX_H__
//---------------------------------------------------------------------------------------

