//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __AN_INTERSECTION_INFO_H__
#define __AN_INTERSECTION_INFO_H__
//---------------------------------------------------------------------------------------
#include "MathsEnums.h"
#include "CPoint3f.h"
//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IIntersectionInfo
{
    public:

        virtual AInt32 getIntersectionCount() const = 0;

        virtual ~IIntersectionInfo() {}
};
//---------------------------------------------------------------------------------------

class CIntersectionInfo : public IIntersectionInfo
{
    public:

        CIntersectionInfo();

        virtual ~CIntersectionInfo() {}

        AInt32 getIntersectionCount() const;

    protected:

        AInt32 m_intersectionCount;
};
//---------------------------------------------------------------------------------------

struct SPointIntersection
{
    AInt32      m_hitPrimitiveIndex;
    CPoint3f    m_intersectionPoint;
};
//---------------------------------------------------------------------------------------

class CPointIntersectionInfo : public CIntersectionInfo
{
    public:

        CPointIntersectionInfo();

        virtual ~CPointIntersectionInfo() {}

        CPointIntersectionInfo(const AIndex32& intersectionCount, const SPointIntersection* pIntersections);

        const SPointIntersection* getIntersections() const;

        AIndex32 getHitPrimitiveIndex(const AIndex32& index) const;

        CPoint3f getIntersectionPoint(const AIndex32& index) const;

        void setIntersections(const AIndex32& intersectionCount, const SPointIntersection* pIntersections);

    protected:

        SPointIntersection* m_pIntersections;
};
//---------------------------------------------------------------------------------------

AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__AN_INTERSECTION_INFO_H__
//---------------------------------------------------------------------------------------
