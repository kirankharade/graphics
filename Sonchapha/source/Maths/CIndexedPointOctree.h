//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_OCTREE_H__
#define __C_OCTREE_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"

//---------------------------------------------------------------------------------------
AN_CONTAINERS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
class CLine;
class CRay;
class CLineSegment;
//---------------------------------------------------------------------------------------

/*
class AN_MATHS_API COctree : public IBSPTree
{
    protected:
        
        CPoint3f    m_origin;
        AInt32      m_depth;

    public:
        
        COctree();

        virtual ~COctree();
    
        COctreeNode* add(CGeoEntity* entity);
    
        COctreeNode* root();

        AInt32 entityCount() const;

        COctreeNode* search(const CGeoEntity* entity);

        CAxisOrientedBox boundingBox() const;

        std::vector<COctreeNode*> isIntersecting(const CPlane* pPlane) const;

        std::vector<COctreeNode*> isIntersecting(const CLine* pLine) const;

        std::vector<COctreeNode*> isIntersecting(const CRay* pRay) const;

};
*/
//---------------------------------------------------------------------------------------
AN_CONTAINERS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_OCTREE_H__
//---------------------------------------------------------------------------------------

