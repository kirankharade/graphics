//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CPointTriangle.h"
#include "MathsSettings.h"
#include "CPlane.h"
#include "CLineSegment.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

An::Maths::CPointTriangle::CPointTriangle()
{
    m_vtx[0] = CPoint3f();
    m_vtx[1] = CPoint3f();
    m_vtx[2] = CPoint3f();
}
//---------------------------------------------------------------------------------------

An::Maths::CPointTriangle::CPointTriangle(const CPoint3f v[3])
{
    m_vtx[0] = v[0];
    m_vtx[1] = v[1];
    m_vtx[2] = v[2];
}
//---------------------------------------------------------------------------------------

An::Maths::CPointTriangle::CPointTriangle(const CPoint3f& a, const CPoint3f& b, const CPoint3f& c)
{
    m_vtx[0] = a;
    m_vtx[1] = b;
    m_vtx[2] = c;
}
//---------------------------------------------------------------------------------------

An::Maths::CPointTriangle::CPointTriangle(const CPointTriangle& triangle)
{
    m_vtx[0] = triangle.m_vtx[0];
    m_vtx[1] = triangle.m_vtx[1];
    m_vtx[2] = triangle.m_vtx[2];
}
//---------------------------------------------------------------------------------------

void An::Maths::CPointTriangle::set(const CPoint3f& a, const CPoint3f& b, const CPoint3f& c)
{
    m_vtx[0] = a;
    m_vtx[1] = b;
    m_vtx[2] = c;
}
//---------------------------------------------------------------------------------------

void An::Maths::CPointTriangle::set(const CPointTriangle& triangle)
{
    m_vtx[0] = triangle.m_vtx[0];
    m_vtx[1] = triangle.m_vtx[1];
    m_vtx[2] = triangle.m_vtx[2];
}
//---------------------------------------------------------------------------------------

CVector3f An::Maths::CPointTriangle::normal() const
{
    CVector3f v = ((m_vtx[1] - m_vtx[0]).cross(m_vtx[2] - m_vtx[0]));
    v.normalize();
    return v;
}
//---------------------------------------------------------------------------------------

AReal32 An::Maths::CPointTriangle::area() const
{
    CVector3f v = ((m_vtx[1] - m_vtx[0]).cross(m_vtx[2] - m_vtx[0]));
    return (AReal32) (0.5 * v.magnitude());
}
//---------------------------------------------------------------------------------------

CPoint3f An::Maths::CPointTriangle::circumCenter() const
{
    return (m_vtx[0] + m_vtx[1] + m_vtx[2])/3.0;
}
//---------------------------------------------------------------------------------------

void An::Maths::CPointTriangle::reverseOrientation()
{
    CPoint3f pt = m_vtx[0];
    m_vtx[0] = m_vtx[2];
    m_vtx[2] = pt;
}
//---------------------------------------------------------------------------------------

bool An::Maths::CPointTriangle::isInside(const CPoint3f& point) const
{
    CVector3f n = normal();
    CVector3f vtmp = (m_vtx[1] - m_vtx[0]).cross(point - m_vtx[0]);
    AReal32 d = vtmp.dot(n);

    if(d < 0)
    {
        return false;
    }
    else
    {
        vtmp = (m_vtx[2] - m_vtx[1]).cross(point - m_vtx[1]);
        d = vtmp.dot(n);
        if(d < 0)
        {
            return false;
        }
        else
        {
            vtmp = (m_vtx[0] - m_vtx[2]).cross(point - m_vtx[2]);
            if(d < 0)
            {
                return false;
            }
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool An::Maths::CPointTriangle::isIntersecting(const CLineSegment* lineSegment) const
{
    return false;
}
//---------------------------------------------------------------------------------------

bool An::Maths::CPointTriangle::isIntersecting(const CLine* line) const
{
    return false;
}
//---------------------------------------------------------------------------------------

CLineSegment An::Maths::CPointTriangle::edge(const AIndex32& i) const
{
    if(i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::edge []";
    }
    if(i == 0)
    {
        return CLineSegment(m_vtx[0], m_vtx[1]);
    }
    if(i == 1)
    {
        return CLineSegment(m_vtx[1], m_vtx[2]);
    }
    return CLineSegment(m_vtx[2], m_vtx[0]);
}
//---------------------------------------------------------------------------------------

CPoint3f& An::Maths::CPointTriangle::operator [](const AIndex16& i)
{
    if(i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::operator []";
    }
    return m_vtx[i];
}
//---------------------------------------------------------------------------------------

const CPoint3f& An::Maths::CPointTriangle::operator [](const AIndex32& i) const
{
    if(i > 2 || i < 0)
    {
        throw "Invalid index: CTriangle::operator []";
    }
    return m_vtx[i];
}
//---------------------------------------------------------------------------------------

CPointTriangle& An::Maths::CPointTriangle::operator =(const CPointTriangle triangle)
{
    m_vtx[0] = triangle.m_vtx[0];
    m_vtx[1] = triangle.m_vtx[1];
    m_vtx[2] = triangle.m_vtx[2];

    return *this;
}
//---------------------------------------------------------------------------------------

bool An::Maths::CPointTriangle::operator ==(const CPointTriangle& triangle) const
{
    if( (m_vtx[0] != triangle.m_vtx[0]) && (m_vtx[0] != triangle.m_vtx[1]) && (m_vtx[0] != triangle.m_vtx[2]) )
    {
        return false;
    }
    if( (m_vtx[1] != triangle.m_vtx[0]) && (m_vtx[1] != triangle.m_vtx[1]) && (m_vtx[1] != triangle.m_vtx[2]) )
    {
        return false;
    }
    if( (m_vtx[2] != triangle.m_vtx[0]) && (m_vtx[2] != triangle.m_vtx[1]) && (m_vtx[2] != triangle.m_vtx[2]) )
    {
        return false;
    }

    CVector3f thisNormal = normal();
    CVector3f thatNormal = triangle.normal();
    AReal32 dot = thisNormal.dot(thatNormal);
    if(!EQ_32(dot, 1.0f))
    {
        return false;
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool An::Maths::CPointTriangle::operator !=(const CPointTriangle& triangle) const
{
    return !(*this == triangle);
}
//---------------------------------------------------------------------------------------

