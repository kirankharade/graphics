//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_POINT_2I_H__
#define __C_POINT_2I_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_MATHS_API CPoint2i
{
    public:
        
        union 
        {
            AInt32 v[2];

            struct 
            {
                AInt32 x;
                AInt32 y;
            };
        };


    public:
        
        CPoint2i();
    
        CPoint2i(const AInt32 v[2]);
    
        CPoint2i(const AInt32& a, const AInt32& b);
    
        CPoint2i(const CPoint2i& point);
    
        void set(const AInt32& a, const AInt32& b);
    
        void set(const CPoint2i& point);
    
        AReal32 dist(const CPoint2i& point) const;
    
        //Index Operators
    
        AInt32& operator [] (const AIndex32& i);
    
        const AInt32& operator [] (const AIndex32& i) const;
    
        //assignment operator

        CPoint2i& operator = (const CPoint2i& point);
    
        CPoint2i& operator = (const CPoint3f& point);
    
        //Comparison Operators
         
        bool operator == (const CPoint2i& point) const;
    
        bool operator != (const CPoint2i& point) const;
    
        bool operator < (const CPoint2i& point) const;
        
        //Operators with scalar arguments
    
        CPoint2i operator * (const AReal32& scalar) const;
    
        CPoint2i& operator *= (const AReal32& scalar);
    
        CPoint2i operator / (const AReal32& scalar) const;
    
        CPoint2i& operator /= (const AReal32& scalar);
    
        CPoint2i operator - () const;

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_POINT_2I_H__
//---------------------------------------------------------------------------------------

