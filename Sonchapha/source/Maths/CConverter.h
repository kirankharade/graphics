//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_CONVERTER_H__
#define __C_CONVERTER_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2f.h"
#include "CPoint3f.h"
#include "CRay.h"
#include "CLineSegment.h"
#include "CPlane.h"
#include "CPointTriangle.h"
#include <cfloat>
#include <cmath>
#include <vector>

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

class AN_MATHS_API CConverter
{
    public:

        static CPoint2f convertTo2f(const CPoint2i& point);

        static CPoint2f convertTo2f(const CPoint3f& point);

        static CPoint2i convertTo2i(const CPoint2f& point);

        static CPoint2i convertTo2i(const CPoint3f& point);

        static CPoint3f convertTo3f(const CPoint2f& point);

        static CPoint3f convertTo3f(const CPoint2i& point);

        static std::vector<CPoint2f> convertTo2f(const std::vector<CPoint2i>& points);

        static std::vector<CPoint2f> convertTo2f(const std::vector<CPoint3f>& points);

        static std::vector<CPoint2i> convertTo2i(const std::vector<CPoint2f>& points);

        static std::vector<CPoint2i> convertTo2i(const std::vector<CPoint3f>& points);

        static std::vector<CPoint3f> convertTo3f(const std::vector<CPoint2f>& points);

        static std::vector<CPoint3f> convertTo3f(const std::vector<CPoint2i>& points);

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_CONVERTER_H__
//---------------------------------------------------------------------------------------
