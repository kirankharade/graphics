//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint3f.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CPoint3f::CPoint3f()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
}
//---------------------------------------------------------------------------------------

CPoint3f::CPoint3f(const AReal32 v[3])
{
    x = v[0];
    y = v[1];
    z = v[2];
}
//---------------------------------------------------------------------------------------

CPoint3f::CPoint3f(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

CPoint3f::CPoint3f(const CPoint3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}
//---------------------------------------------------------------------------------------

void CPoint3f::set(const AReal32& a, const AReal32& b, const AReal32& c)
{
    x = a;
    y = b;
    z = c;
}
//---------------------------------------------------------------------------------------

void CPoint3f::set(const CPoint3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}
//---------------------------------------------------------------------------------------

CVector3f CPoint3f::positionVector() const
{
    return CVector3f(x, y, z);
}
//---------------------------------------------------------------------------------------

AReal32 CPoint3f::dist(const CPoint3f& point) const
{
    return sqrt( ((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y)) + ((z - point.z)*(z - point.z)) );
}
//---------------------------------------------------------------------------------------

CVector3f CPoint3f::vectorTo(const CPoint3f& point) const
{
    return CVector3f(point.x - x, point.y - y, point.z - z);
}
//---------------------------------------------------------------------------------------

CVector3f CPoint3f::vectorFrom(const CPoint3f& point) const
{
    return CVector3f(x - point.x, y - point.y, z - point.z);
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::offsetAlongUnitDir(const CVector3f& dir, const AReal32& offsetDist) const
{
    CVector3f v = dir;
    v.normalize();
    return CPoint3f( (x + (v.x * offsetDist)), (y + (v.y * offsetDist)), (z + (v.z * offsetDist)) );
}
//---------------------------------------------------------------------------------------

//operators

AReal32& CPoint3f::operator [] (const AIndex32& index)
{
    if(0 == index) return x;
    if(1 == index) return y;
    if(2 == index) return z;
    throw "CPoint3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AReal32& CPoint3f::operator [] (const AIndex32& index) const
{
    if(0 == index) return x;
    if(1 == index) return y;
    if(2 == index) return z;
    throw "CPoint3f::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator = (const CPoint3f& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
    return *this;
}
//---------------------------------------------------------------------------------------

bool CPoint3f::operator == (const CPoint3f& point) const
{
   if( EQ_32(x, point.x) && EQ_32(y, point.y) && EQ_32(z, point.z) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint3f::operator != (const CPoint3f& point) const
{
   if( (!EQ_32(x, point.x)) || (!EQ_32(y, point.y)) || (!EQ_32(z, point.z)) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint3f::operator < (const CPoint3f& point) const
{
    if(EQ_32(x, point.x) && EQ_32(y, point.y) && EQ_32(z, point.z))
    {
        return false;
    }
    if(x < point.x)      return true;
    else if(x > point.x) return false;
    else if(y < point.y) return true;
    else if(y > point.y) return false;
    else return (z < point.z);
}
//---------------------------------------------------------------------------------------

bool CPoint3f::operator > (const CPoint3f& point) const
{
    if( (!(*this < point)) && (*this != point))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

CVector3f CPoint3f::operator - (const CPoint3f& point) const
{
    return CVector3f(x - point.x, y - point.y, z - point.z);
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator + (const CPoint3f& point) const
{
    return CPoint3f(x + point.x, y + point.y, z + point.z);
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator - (const CVector3f& vec) const
{
    return CPoint3f(x - vec.x, y - vec.y, z - vec.z);
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator + (const CVector3f& vec) const
{
    return CPoint3f(x + vec.x, y + vec.y, z + vec.z);
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator += (const CVector3f& vec)
{
    x += vec.x;
    y += vec.y;
    z += vec.z;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator -= (const CVector3f& vec)
{
    x -= vec.x;
    y -= vec.y;
    z -= vec.z;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator += (const CPoint3f& point)
{
    x += point.x;
    y += point.y;
    z += point.z;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator * (const AReal32& scalar) const
{
    return CPoint3f( x * scalar, y * scalar, z * scalar);
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator *= (const AReal32& scalar)
{
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator / (const AReal32& scalar) const
{
    return CPoint3f( x / scalar, y / scalar, z / scalar);
}
//---------------------------------------------------------------------------------------

CPoint3f& CPoint3f::operator /= (const AReal32& scalar)
{
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint3f CPoint3f::operator - () const
{
    return CPoint3f(-x, -y, -z);
}
//---------------------------------------------------------------------------------------

