//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CPoint2i.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CPoint2i::CPoint2i()
{
    x = 0;
    y = 0;
}
//---------------------------------------------------------------------------------------

CPoint2i::CPoint2i(const AInt32 v[2])
{
    x = v[0];
    y = v[1];
}
//---------------------------------------------------------------------------------------

CPoint2i::CPoint2i(const AInt32& a, const AInt32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

CPoint2i::CPoint2i(const CPoint2i& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

void CPoint2i::set(const AInt32& a, const AInt32& b)
{
    x = a;
    y = b;
}
//---------------------------------------------------------------------------------------

void CPoint2i::set(const CPoint2i& point)
{
    x = point.x;
    y = point.y;
}
//---------------------------------------------------------------------------------------

AReal32 CPoint2i::dist(const CPoint2i& point) const
{
    return sqrt( (AReal32) (((x - point.x)*(x - point.x)) + ((y - point.y)*(y - point.y))) );
}
//---------------------------------------------------------------------------------------

//operators

AInt32& CPoint2i::operator [] (const AIndex32& index)
{
    if(0 == index) return x;
    if(1 == index) return y;
    throw "CPoint2i::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

const AInt32& CPoint2i::operator [] (const AIndex32& index) const
{
    if(0 == index) return x;
    if(1 == index) return y;
    throw "CPoint2i::Operator [] : Invalid index passed.";
}
//---------------------------------------------------------------------------------------

CPoint2i& CPoint2i::operator = (const CPoint2i& point)
{
    x = point.x;
    y = point.y;
    return *this;
}
//---------------------------------------------------------------------------------------
   
CPoint2i& CPoint2i::operator = (const CPoint3f& point)
{
    x = (AInt32) point.x;
    y = (AInt32) point.y;
    return *this;
}
//---------------------------------------------------------------------------------------

bool CPoint2i::operator == (const CPoint2i& point) const
{
   if( (x == point.x) && (y == point.y) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint2i::operator != (const CPoint2i& point) const
{
   if( (x != point.x) && (y != point.y) )
   {
       return true;
   }
   return false;
}
//---------------------------------------------------------------------------------------

bool CPoint2i::operator < (const CPoint2i& point) const
{
   if( (x == point.x) && (y == point.y) )
    {
        return false;
    }
    if(x < point.x)      return true;
    else if(x > point.x) return false;
    else return (y < point.y);
}
//---------------------------------------------------------------------------------------

CPoint2i CPoint2i::operator * (const AReal32& scalar) const
{
    return CPoint2i((AInt32)(x * scalar), (AInt32)(y * scalar));
}
//---------------------------------------------------------------------------------------

CPoint2i& CPoint2i::operator *= (const AReal32& scalar)
{
    x = (AInt32) (x * scalar);
    y = (AInt32) (y * scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2i CPoint2i::operator / (const AReal32& scalar) const
{
    return CPoint2i((AInt32)(x / scalar), (AInt32)(y / scalar));
}
//---------------------------------------------------------------------------------------

CPoint2i& CPoint2i::operator /= (const AReal32& scalar)
{
    x = (AInt32) (x / scalar);
    y = (AInt32) (y / scalar);
    return *this;
}
//---------------------------------------------------------------------------------------

CPoint2i CPoint2i::operator - () const
{
    return CPoint2i(-x, -y);
}
//---------------------------------------------------------------------------------------

