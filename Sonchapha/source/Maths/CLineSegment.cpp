//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "CLineSegment.h"
#include "MathsSettings.h"
#include "CPlane.h"
#include "CLine.h"
#include <cfloat>
#include <cmath>

using namespace An;
using namespace An::Maths;

//---------------------------------------------------------------------------------------

CLineSegment::CLineSegment()
{
    m_firstPoint = CPoint3f();
    m_secondPoint = CPoint3f();
}
//---------------------------------------------------------------------------------------

CLineSegment::CLineSegment(const CPoint3f& point, const CVector3f& direction, const AReal32& length)
: m_firstPoint(point)
{
    CVector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

CLineSegment::CLineSegment(const CPoint3f& firstPoint, const CPoint3f& secondPoint)
: m_firstPoint(firstPoint)
, m_secondPoint(secondPoint)
{
}
//---------------------------------------------------------------------------------------

CLineSegment::CLineSegment(const CLineSegment& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
}
//---------------------------------------------------------------------------------------

CLineSegment::~CLineSegment()
{
}
//---------------------------------------------------------------------------------------

void CLineSegment::set(const CPoint3f& firstPoint, const CVector3f& direction, const AReal32& length)
{
    m_firstPoint = firstPoint;
    CVector3f dir = direction;
    dir.normalize();
    m_secondPoint = m_firstPoint + (dir * length);
}
//---------------------------------------------------------------------------------------

void CLineSegment::set(const CPoint3f& firstPoint, const CPoint3f& secondPoint)
{
    m_firstPoint = firstPoint;
    m_secondPoint = secondPoint;
}
//---------------------------------------------------------------------------------------

CPoint3f CLineSegment::getFirstPoint() const
{
    return m_firstPoint;
}
//---------------------------------------------------------------------------------------

CPoint3f CLineSegment::getSecondPoint() const
{
    return m_secondPoint;
}
//---------------------------------------------------------------------------------------

CVector3f CLineSegment::getDirection() const
{
    CVector3f vec = m_secondPoint - m_firstPoint;
    vec.normalize();
    return vec;
}
//---------------------------------------------------------------------------------------

AReal32 CLineSegment::getLength() const
{
    return (m_secondPoint - m_firstPoint).length();
}
//---------------------------------------------------------------------------------------

bool CLineSegment::isIntersecting(const CPlane* pPlane) const
{
    CPoint3f intersectionPoint;
    return isIntersecting(pPlane, intersectionPoint);
}
//---------------------------------------------------------------------------------------

bool CLineSegment::isIntersecting(const CPlane* pPlane, CPoint3f& intersectionPoint) const
{
    CLine line(m_firstPoint, m_secondPoint);

    if(false == line.isIntersecting(pPlane, intersectionPoint))
    {
        return false;
    }
    
    CVector3f v1 = m_secondPoint - m_firstPoint;
    CVector3f v2 = intersectionPoint - m_firstPoint;

    AReal32 u = v1.dot(v2) / v1.dot(v1);

    if((u >= (-TOLERANCE_32)) && (u <= (1.0f + TOLERANCE_32)))
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLineSegment::passesThrough(const CPoint3f& point, const AReal32 tolerance) const
{
    CLine line(m_firstPoint, m_secondPoint);

    if(line.passesThrough(point, tolerance))
    {
        AReal32 d1 = (m_firstPoint - point).length();
        AReal32 d2 = (m_secondPoint - point).length();
        AReal32 d = (m_secondPoint - m_firstPoint).length();
        if(EQ_32(d1+d2, d))
        {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------------------

CPoint3f CLineSegment::closestPoint(const CPoint3f& point) const
{
    CLine line(m_firstPoint, m_secondPoint);

    CPoint3f closestPt = line.closestPoint(point);
    if(point.dist(closestPt) > point.dist(m_firstPoint))
    {
        closestPt = m_firstPoint;
    }
    if(point.dist(closestPt) > point.dist(m_secondPoint))
    {
        closestPt = m_secondPoint;
    }
    return closestPt;
}
//---------------------------------------------------------------------------------------

AReal32 CLineSegment::distFromPoint(const CPoint3f& point) const
{
    CPoint3f closestPt = closestPoint(point);
    return closestPt.dist(point);
}
//---------------------------------------------------------------------------------------

//Assignment operator

CLineSegment& CLineSegment::operator = (const CLineSegment& lineSegment)
{
    m_firstPoint = lineSegment.m_firstPoint;
    m_secondPoint = lineSegment.m_secondPoint;
    return *this;
}
//---------------------------------------------------------------------------------------

//Comparison Operators

bool CLineSegment::operator == (const CLineSegment& ls) const
{
    if((m_firstPoint == ls.getFirstPoint()) && (m_secondPoint == ls.getSecondPoint()))
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLineSegment::operator != (const CLineSegment& lineSegment) const
{
    if( !(*this == lineSegment) )
    {
       return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLineSegment::operator < (const CLineSegment& lineSegment) const
{
    CPoint3f thatFirstPoint = lineSegment.getFirstPoint();
    CPoint3f thatSecondPoint = lineSegment.getSecondPoint();

    if(m_firstPoint < thatFirstPoint)
    {
        return true;
    }
    if(m_firstPoint > thatFirstPoint)
    {
        return false;
    }
    if(m_secondPoint < thatSecondPoint)
    {
        return true;
    }
    if(m_secondPoint > thatSecondPoint)
    {
        return false;
    }
    return false;
}
//---------------------------------------------------------------------------------------
