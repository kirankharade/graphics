//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_MATRIX_4F_H__
#define __C_MATRIX_4F_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CVector3f.h"
#include "CPoint3f.h"
#include "MathsEnums.h"

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_MATHS_API CMatrix4f
{
    public:
        
        union 
        {
            AReal32 m[16];

            struct 
            {
                AReal32 e[4][4];
            };
        };

    private:

        CVector3f m_scale;

        bool m_bAbsoluteScaleRecoverable;

    public:

        CMatrix4f(const ABool& bSetIdentity = true);
    
        CMatrix4f(const AReal32* v, const bool bScaleRecoverable = true);
    
        CMatrix4f(const CMatrix4f& matrix);
    
        void set(const AReal32* v, const bool bScaleRecoverable = true);
    
        void set(const CMatrix4f& matrix);
    
        void setIdentity();
    
        void setZero();

        bool isIdentity() const;
    
        bool isZero() const;

        const AReal32* ptr() const;

        AReal32* ptr();

        CMatrix4f inverse() const;

        CMatrix4f transpose() const;

        AReal32 determinent() const;

        //Rotations

        void resetRotation();

        CMatrix4f getRotationMatrix() const;

        CVector3f getRotationDeg() const;

        void setRotationDeg(const AReal32& dx, const AReal32& dy, const AReal32& dz);

        void setRotationDeg(const CVector3f& d);

        void setXRotationDeg(const AReal32& dx);

        void setYRotationDeg(const AReal32& dy);

        void setZRotationDeg(const AReal32& dz);

        //Scaling

        void resetScaling();

        CVector3f getScaleValues();

        void setScaleValues(const AReal32& sx, const AReal32& sy, const AReal32& sz);

        void setScaleValues(const CVector3f& s);

        void scaleMatrix(const AReal32& sx, const AReal32& sy, const AReal32& sz);

        void scaleMatrix(const CVector3f& s);

        void scaleMatrixX(const AReal32& sx);

        void scaleMatrixY(const AReal32& sy);

        void scaleMatrixZ(const AReal32& sz);

        //Translation

        void resetTranslation();

        CVector3f getTranslation() const;

        void setTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz);

        void setTranslation(const CVector3f& t);

        void setXTranslation(const AReal32& tx);

        void setYTranslation(const AReal32& ty);

        void setZTranslation(const AReal32& tz);

        void addTranslation(const AReal32& tx, const AReal32& ty, const AReal32& tz);

        void addTranslation(const CVector3f& t);

        //Rotation about arbritrary axis

	    CMatrix4f  RotateDegAboutVectorThroughOrigin(const CVector3f& vec, const float& angleDeg);

	    CMatrix4f  RotateDegAboutVectorThroughPoint(const CVector3f& vec, const CPoint3f& point, const float& angleDeg);

        //Camera related transformation matrix

        CMatrix4f perspectiveProjectionFieldOfViewRHS(const AReal32& fovDeg, const AReal32& aspectRatio, const AReal32& near, const AReal32& far);

        CMatrix4f perspectiveProjectionFieldOfViewLHS(const AReal32& fovDeg, const AReal32& aspectRatio, const AReal32& near, const AReal32& far);

        CMatrix4f perspectiveProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

        CMatrix4f perspectiveProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

        CMatrix4f orthographicProjectionLHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

        CMatrix4f orthographicProjectionRHS(const AReal32& viewVolumeWidth, const AReal32& viewVolumeHeight, const AReal32& near, const AReal32& far);

        CMatrix4f viewMatrixLHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector);

        CMatrix4f viewMatrixRHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector);

        CMatrix4f cameraLookAtRHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector);

        CMatrix4f cameraLookAtLHS(const CPoint3f& camPosition, const CPoint3f& camTarget, const CVector3f& upVector);

        const CMatrix4f& cameraLookInDirection(const CPoint3f& camPosition, const CVector3f& camDirection, const CVector3f& upVector);

        //Transformations for vector alignments

        CMatrix4f  AlignVecToPositiveZ(const CVector3f& vec);

        CMatrix4f  RotateVecToPlaneXZ(const CVector3f& vec);

        //Transformation methods

        //Only rotates the vector. Does not apply scaling.
        CVector3f rotate(const CVector3f& vec) const;

        //Only rotates the point. Does not apply scaling.
        CPoint3f rotate(const CPoint3f& point) const;

        CVector3f rotateAndScale(const CVector3f& vec) const;

        CPoint3f rotateAndScale(const CPoint3f& point) const;

        CPoint3f translate(const CPoint3f& point) const;

        CPoint3f transform(const CPoint3f& point) const;

        //Operators
    
        AReal32& operator () (const AIndex32& rowIndex, const AIndex32& colIndex);
    
        const AReal32& operator () (const AIndex32& rowIndex, const AIndex32& colIndex) const;
    
        //Index Operators
    
        AReal32& operator [] (const AIndex32& index);
    
        const AReal32& operator [] (const AIndex32& index) const;

        //assignment operator

        CMatrix4f& operator = (const CMatrix4f& matrix);
    
        //Comparison Operators
         
        bool operator == (const CMatrix4f& matrix) const;
    
        bool operator != (const CMatrix4f& matrix) const;
    
        bool operator < (const CMatrix4f& matrix) const;
        
        //Summation and subtraction operators
    
        CMatrix4f operator + (const CMatrix4f& matrix) const;
    
        CMatrix4f& operator += (const CMatrix4f& matrix);
    
        CMatrix4f operator - (const CMatrix4f& matrix) const;
    
        CMatrix4f& operator -= (const CMatrix4f& matrix);
        
        CMatrix4f operator * (const CMatrix4f& matrix) const;
    
        CMatrix4f& operator *= (const CMatrix4f& matrix);
        
        CVector3f  operator* (const CVector3f& vec) const;

        CPoint3f  operator* (const CPoint3f& point) const;

        CMatrix4f operator * (const AReal32& scalar) const;
    
        CMatrix4f& operator *= (const AReal32& scalar);
    
        CMatrix4f operator / (const AReal32& scalar) const;
    
        CMatrix4f& operator /= (const AReal32& scalar);
    
        CMatrix4f operator - () const;

    protected:

        void add(const AReal32* ptr);

        void subtract(const AReal32* ptr);

        CVector3f recoverScale() const;
};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_MATRIX_4F_H__
//---------------------------------------------------------------------------------------
