//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_BOUNDING_BOX_H__
#define __C_BOUNDING_BOX_H__
//---------------------------------------------------------------------------------------

#include "MathsIncludes.h"
#include "CAxisOrientedBox.h"
#include <vector>

//---------------------------------------------------------------------------------------
AN_MATHS_START_NAMESPACE
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
//Forward declarations
class CPlane;
class CLine;
class CRay;
//---------------------------------------------------------------------------------------

class AN_MATHS_API CBoundingBox: public CAxisOrientedBox
{
    protected:

        bool    m_bIsInitialized;

    public:

        CBoundingBox();

        CBoundingBox(const CPoint3f& origin, const AReal32& width, const AReal32& height, const AReal32& depth);

        CBoundingBox(const CPoint3f& minPoint, const CPoint3f& maxPoint);

        CBoundingBox(const std::vector<CPoint3f>& points);

        CBoundingBox(const CPoint3f* points, const AUInt32 numPoints);

        CBoundingBox(const CPoint3f& origin, const AReal32& size);

        CBoundingBox(const CBoundingBox& box);

        virtual ~CBoundingBox();

        void Add(const std::vector<CPoint3f>& points);

        void Add(const CPoint3f* points, const AUInt32 numPoints);

        void Add(const CPoint3f& point);

        //Assignment operator

        CBoundingBox& operator = (const CBoundingBox& box);

        //Addition Operators

        CBoundingBox operator + (const CBoundingBox& box);

        CBoundingBox& operator += (const CBoundingBox& box);

        //Comparison Operators

        bool operator == (const CBoundingBox& box) const;

        bool operator != (const CBoundingBox& box) const;

        bool operator < (const CBoundingBox& box) const;

};
//---------------------------------------------------------------------------------------
AN_MATHS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__C_BOUNDING_BOX_H__
//---------------------------------------------------------------------------------------

