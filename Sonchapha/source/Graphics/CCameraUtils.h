//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_CAMERA_UTILS_H__
#define __C_CAMERA_UTILS_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CMatrix4f.h"
#include "../Maths/CPlane.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class IScene;
class CCamera;
class CCoordinateSystem;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CCameraUtils
{

public:

    static Maths::CPoint3f ModelToScreenCoords(CCamera* cam, const Maths::CPoint3f& point);

    static Maths::CPoint3f ScreenToModelCoords(CCamera* cam, const Maths::CPoint3f& point);

    static float CalculateSceneExtent(CCamera* cam, 
                                    const Maths::CPoint3f& sceneCentre, 
                                    const float sceneBoundRadius,
                                    const bool useToAspectRatio = false);

    static void StandardViewParameters(EPresetViewType viewType,
                                    const float viewExtent,
                                    const Maths::CPoint3f& target,
                                    Maths::CPoint3f& pos,
                                    Maths::CVector3f& up);

    static float GetCameraResetParameters(CCamera* cam, 
                                    const Maths::CPoint3f& sceneCentre, 
                                    const float sceneBoundRadius,
                                    const EPresetViewType viewType,
                                    CCoordinateSystem* pCoordSystem, 
                                    Maths::CPoint3f& target, 
                                    Maths::CPoint3f& pos,
                                    Maths::CVector3f& up);

    static float GetProjectedTranslationAlongVector(CCamera* cam, 
                                    const Maths::CPoint3f& dirBasePt,
                                    const Maths::CPoint3f& dirEndPt,
                                    const Maths::CPoint3f& lastMousePosition,
                                    const Maths::CPoint3f& currMousePosition);

    static bool UnProject(const Maths::CPoint3f& point, 
                                    const Maths::CMatrix4f& modelView, 
                                    const Maths::CMatrix4f& projection, 
                                    const int viewportX, 
                                    const int viewportY, 
                                    const int viewportWidth, 
                                    const int viewportHeight,
                                    Maths::CPoint3f& outPoint);


    static bool Project(const Maths::CPoint3f& point, 
                                    const Maths::CMatrix4f& modelView, 
                                    const Maths::CMatrix4f& projection, 
                                    const int viewportX, 
                                    const int viewportY, 
                                    const int viewportWidth, 
                                    const int viewportHeight,
                                    Maths::CPoint3f& outPoint);

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_CAMERA_UTILS_H__
//---------------------------------------------------------------------------------------
