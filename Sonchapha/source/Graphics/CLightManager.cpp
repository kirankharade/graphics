//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CLightManager.h"
#include "CLight.h"
#include "CColor.h"
#include "IViewport.h"
#include "Containers/CUniqueItemMap.h"
#include "IFactory.h"
#include "IGraphics.h"
#include <string>

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

typedef CUniqueItemMap<AInt16, CLight*>  CLightList;

//---------------------------------------------------------------------------------------

CLightManager::CLightManager(IViewport* viewport)
: m_pViewport(viewport)
{
    m_pListImpl = new CLightList();
}
//---------------------------------------------------------------------------------------

CLightManager::~CLightManager()
{
    EmptyList();
    SAFE_CLEANUP(m_pListImpl);
}
//---------------------------------------------------------------------------------------

void CLightManager::EmptyList()
{
    CLightList* pList = (CLightList*) m_pListImpl;
    int listCount = pList->size();

    for(int i = 0; i < listCount; i++)
    {
        CLight* pLight = pList->GetByIndex(i);
        if(pLight)
        {
            pLight->Disable();
            SAFE_CLEANUP(pLight);
        }
    }
    pList->clear();
}
//---------------------------------------------------------------------------------------

AUInt16 CLightManager::MaxLightCount() const
{
    return IGraphics::Gfx()->MaxLightCount();
}
//---------------------------------------------------------------------------------------

CLight* CLightManager::AddLight()
{
    CLight* pLight = new CLight();
    AInt32 index = Add(pLight);
    if(index == -1)
    {
        SAFE_CLEANUP(pLight);
        return nullptr;
    }
    pLight->SetIndex(index);
    return pLight;
}
//---------------------------------------------------------------------------------------

CLight* CLightManager::AddLight(const CColor& ambient, const CColor& diffuse, 
                                const CColor& specular, const Maths::CPoint3f& position, 
                                const Maths::CVector3f& direction)
{
    CLight* pLight = new CLight(ambient, diffuse, specular, position, direction);
    AInt32 index = Add(pLight);
    if(index == -1)
    {
        SAFE_CLEANUP(pLight);
        return nullptr;
    }
    pLight->SetIndex(index);
    return pLight;
}
//---------------------------------------------------------------------------------------

CLight* CLightManager::AddSimilarLight(const CLight*& light)
{
    CLight* pLight = new CLight(*light);
    AInt32 index = Add(pLight);
    if(index == -1)
    {
        SAFE_CLEANUP(pLight);
        return nullptr;
    }
    pLight->SetIndex(index);
    return pLight;
}
//---------------------------------------------------------------------------------------

bool CLightManager::Remove(const AUInt16 lightIndex)
{
    CLightList* pList = (CLightList*) m_pListImpl;
    CLight* pLight = pList->find(lightIndex);
    if(nullptr == pLight)
    {
        return false;
    }
    bool bStatus = IGraphics::Gfx()->UnsetLight(pLight);
    if(bStatus)
    {
        pList->remove(pLight->Index());
        SAFE_CLEANUP(pLight);
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLightManager::Remove(CLight* light)
{
    if(light == nullptr)
    {
        return false;
    }
    CLightList* pList = (CLightList*) m_pListImpl;
    if(!pList->exists(light->Index()))
    {
        return false;
    }
    bool bStatus = IGraphics::Gfx()->UnsetLight(light);
    if(bStatus)
    {
        pList->remove(light->Index());
        SAFE_CLEANUP(light);
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CLightManager::CanAddLight() const
{
    CLightList* pList = (CLightList*) m_pListImpl;
    if(pList->size() < MaxLightCount())
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

AInt32 CLightManager::GetAvailableIndex() const
{
    if(!CanAddLight())
    {
        return -1;
    }
    CLightList* pList = (CLightList*) m_pListImpl;
    for(AUInt32 i = 0; i < MaxLightCount(); i++)
    {
        if(nullptr == pList->GetByIndex(i))
        {
            return i;
        }
    }
    return -1;
}
//---------------------------------------------------------------------------------------

AInt32 CLightManager::Add(CLight* pLight)
{
    if(!CanAddLight() || pLight == nullptr)
    {
        return -1;
    }
    AInt32 index = GetAvailableIndex();
    if(index != -1)
    {
        pLight->SetIndex((AInt16) index);

        char s[50];
        sprintf(s, "%2d", index);
        std::string str = "Light_" + std::string(s);
        pLight->SetName(str);

        if(IGraphics::Gfx()->SetLight(pLight))
        {
            CLightList* pList = (CLightList*) m_pListImpl;
            pList->add(index, pLight);
            pLight->SetManager(this);
            return index;
        }
    }
    return -1;
}
//---------------------------------------------------------------------------------------

CLight* CLightManager::GetLight(const AInt16 index)
{
    CLightList* pList = (CLightList*) m_pListImpl;
    return pList->find(index);
}
//---------------------------------------------------------------------------------------

CLight* CLightManager::GetLight(const std::string& name)
{
    CLightList* pList = (CLightList*) m_pListImpl;
    for(AUInt32 i = 0; i < MaxLightCount(); i++)
    {
        CLight* pLight = pList->GetByIndex(i);
        if(nullptr == pLight && pLight->Name() == name)
        {
            return pLight;
        }
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

AUInt16 CLightManager::CurrentCount() const
{
    CLightList* pList = (CLightList*) m_pListImpl;
    return pList->size();
}
//---------------------------------------------------------------------------------------

bool CLightManager::EnableLight(CLight* pLight)
{
    if(pLight == nullptr)
    {
        return false;
    }
    CLightList* pList = (CLightList*) m_pListImpl;
    if(false == pList->exists(pLight->Index()))
    {
        return false;
    }
    return IGraphics::Gfx()->EnableLight(pLight);
}
//---------------------------------------------------------------------------------------

bool CLightManager::DisableLight(CLight* pLight)
{
    if(pLight == nullptr)
    {
        return false;
    }
    CLightList* pList = (CLightList*) m_pListImpl;
    if(false == pList->exists(pLight->Index()))
    {
        return false;
    }
    return IGraphics::Gfx()->DisableLight(pLight);
}
//---------------------------------------------------------------------------------------

bool CLightManager::EnableLight(const std::string& name)
{
    return EnableLight(GetLight(name));
}
//---------------------------------------------------------------------------------------

bool CLightManager::DisableLight(const std::string& name)
{
    return DisableLight(GetLight(name));
}
//---------------------------------------------------------------------------------------

bool CLightManager::EnableLight(const AInt16 index)
{
    return EnableLight(GetLight(index));
}
//---------------------------------------------------------------------------------------

bool CLightManager::DisableLight(const AInt16 index)
{
    return DisableLight(GetLight(index));
}
//---------------------------------------------------------------------------------------

bool CLightManager::EnableAll()
{
    CLightList* pList = (CLightList*) m_pListImpl;
    for(AUInt32 i = 0; i < MaxLightCount(); i++)
    {
        if( false == EnableLight(pList->GetByIndex(i)) )
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CLightManager::DisableAll()
{
    CLightList* pList = (CLightList*) m_pListImpl;
    for(AUInt32 i = 0; i < MaxLightCount(); i++)
    {
        if( false == DisableLight(pList->GetByIndex(i)) )
        {
            return false;
        }
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CLightManager::IsEnabled(const CLight* pLight) const
{
    if(pLight != nullptr)
    {
        return IGraphics::Gfx()->IsLightEnabled(pLight->Index());
    }
    return true;
}
//---------------------------------------------------------------------------------------

void CLightManager::EnableLighting()
{
    IGraphics::Gfx()->EnableLighting();
}
//---------------------------------------------------------------------------------------

void CLightManager::DisableLighting()
{
    IGraphics::Gfx()->DisableLighting();
}
//---------------------------------------------------------------------------------------
