//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "CColor.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CVector3f.h"
#include "CColor.h"
#include "CMaterial.h"
//---------------------------------------------------------------------------------------
#ifndef __DEFAULTS_H__
#define __DEFAULTS_H__
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

struct AN_GRAPHICS_API Defaults
{

public:

    static AReal32 PointSize;

    static AReal32 LineWidth;

    static CColor Color;

    static CColor BackGroundColor;

    static CColor MaterialAmbient;

    static CColor MaterialDiffuse;

    static CColor MaterialEmissive;

    static CColor MaterialSpecular;

    static AReal32 MaterialShininess;

    static CColor LightAmbient;

    static CColor LightDiffuse;

    static CColor LightSpecular;

    static Maths::CPoint3f LightPosition;

    static Maths::CVector3f LightSpotDirection;

    static AReal32  LightSpotCutOffAngle;

    static AReal32  LightSpotExponent;

    static AReal32  LightConstantAttenuation;

    static AReal32  LightLinearAttenuation;

    static AReal32  LightQuadraticAttenuation;

    static EPresetViewType Default3DPresetView;


    static CColor Defaults::FrontMaterialAmbient;

    static CColor Defaults::FrontMaterialDiffuse;

    static CColor Defaults::FrontMaterialEmissive;

    static CColor Defaults::FrontMaterialSpecular;

    static AReal32 Defaults::FrontMaterialShininess;


    static CColor Defaults::BackMaterialAmbient;

    static CColor Defaults::BackMaterialDiffuse;

    static CColor Defaults::BackMaterialEmissive;

    static CColor Defaults::BackMaterialSpecular;

    static AReal32 Defaults::BackMaterialShininess;

    static CMaterial Defaults::FrontMaterial;

    static CMaterial Defaults::BackMaterial;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__DEFAULTS_H__
//---------------------------------------------------------------------------------------

