//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_STRING_UTILS_H__
#define __C_STRING_UTILS_H__
//---------------------------------------------------------------------------------------
#include <string>
#include "../GraphicsIncludes.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class CStringUtils
{

public:

   static char ToUpper(const char& c);

   static char ToLower(const char& c);

   static bool StartsWith(const std::string& src, const std::string& search, const bool& bIgnoreCase = true, const bool& bIgnoreStartingSpaces = true);

   static bool EndsWith(const std::string& src, const std::string& search, const bool& bIgnoreCase = true, const bool& bIgnoreEndingSpaces = true);

   static bool Contains(const std::string& src, const std::string& search);

   static void ToWideCharArray(const char*& s, wchar_t*& ws, const unsigned int& size);

   static void ToWideString(const std::string& s, std::wstring& ws);

   static std::wstring ToWideString(const std::string& s);

   static std::string ToString(const AIndex64& num);

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_STRING_UTILS_H__
//---------------------------------------------------------------------------------------
