//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_TEXTURE_MANAGER_H__
#define __I_TEXTURE_MANAGER_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsDefines.h"
#include "../GraphicsSTLExports.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ITexture;
class IGraphics;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API ITextureManager
{

public:

    ITextureManager(IGraphics* graphics);

    virtual ~ITextureManager();

    IGraphics* Graphics() const;

    virtual bool Add(ITexture* texture);

    virtual bool Exists(const AUInt32 openGLId) const;

    virtual bool Exists(ITexture* texture) const;

    virtual void Remove(ITexture* texture, const bool deleteData);

    virtual void Remove(const AUInt32 openGLId, const bool deleteData);

    virtual void Update(ITexture* texture) = 0;

    virtual AUInt32 Count() const;

protected:

    void*       m_pTextureCollection;
    IGraphics*  m_pGraphics;

protected:

    virtual void DeleteAllTextures();

    virtual void RemoveFromGraphics(const AUInt32 openGLId) = 0;

    void SetOpenGLId(ITexture* texture, AUInt32 oglId);

    AUInt32 GetOpenGLId(ITexture* texture);


    friend class ITexture;
    friend class IGraphics;
};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_TEXTURE_MANAGER_H__
//---------------------------------------------------------------------------------------
