//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "CImage.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CImage::CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
        const AUByte* imageData, EPixelFormat pixelFormat, EImageOriginType originType)
: m_width(width)
, m_height(height)
, m_depth(depth)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(pixelFormat)
, m_originType(originType)
, m_byteCount(0)
{
    Allocate();
    Copy(imageData);
}
//---------------------------------------------------------------------------------------

CImage::CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
        const CColor& color, EPixelFormat pixelFormat, EImageOriginType originType)
: m_width(width)
, m_height(height)
, m_depth(depth)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(pixelFormat)
, m_originType(originType)
, m_byteCount(0)
{
    Allocate();
    SetColor(color);
}
//---------------------------------------------------------------------------------------

CImage::CImage(const AUInt32 width, const AUInt32 height, const AUInt32 depth, 
        EPixelFormat pixelFormat, EImageOriginType originType)
: m_width(width)
, m_height(height)
, m_depth(depth)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(pixelFormat)
, m_originType(originType)
, m_byteCount(0)
{
    Allocate();
    SetColor(CColor(0.0f, 0.0f, 0.0f, 0.0f));
}
//---------------------------------------------------------------------------------------

CImage::CImage(const AUInt32 width, const AUInt32 height, EPixelFormat pixelFormat, EImageOriginType originType)
: m_width(width)
, m_height(height)
, m_depth(1)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(pixelFormat)
, m_originType(originType)
, m_byteCount(0)
{
    Allocate();
    SetColor(CColor(0.0f, 0.0f, 0.0f, 0.0f));
}
//---------------------------------------------------------------------------------------

CImage::CImage(const std::string& fileName)
: m_width(0)
, m_height(0)
, m_depth(1)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(EPixelFormat_R8G8B8A8)
, m_originType(EImageOriginType_BottomLeft)
, m_byteCount(0)
{
    //Get metadata from the image file
    //Allocate();
}
//---------------------------------------------------------------------------------------

CImage::CImage(const CImage& image)
: m_width(image.m_width)
, m_height(image.m_height)
, m_depth(image.m_depth)
, m_pData(nullptr)
, m_bIsDirty(true)
, m_pixelformat(image.m_pixelformat)
, m_originType(image.m_originType)
, m_byteCount(image.m_byteCount)
{
    Allocate();
    Copy(image.Data());
}
//---------------------------------------------------------------------------------------

CImage::~CImage()
{
    SAFE_CLEANUP(m_pData);
}
//---------------------------------------------------------------------------------------

AUInt32 CImage::Width() const
{
    return m_width;
}
//---------------------------------------------------------------------------------------

AUInt32 CImage::Depth() const
{
    return m_depth;
}
//---------------------------------------------------------------------------------------

AUInt32 CImage::Height() const
{
    return m_height;
}
//---------------------------------------------------------------------------------------

const AUByte* CImage::Data() const
{
    return m_pData;
}
//---------------------------------------------------------------------------------------

AUByte* CImage::Data()
{
    return m_pData;
}
//---------------------------------------------------------------------------------------

bool CImage::IsDirty() const
{
    return m_bIsDirty;
}
//---------------------------------------------------------------------------------------

void CImage::SetDirty(bool bDirty)
{
    m_bIsDirty = bDirty;
}
//---------------------------------------------------------------------------------------

EPixelFormat CImage::PixelFormat() const
{
    return m_pixelformat;
}
//---------------------------------------------------------------------------------------

EImageOriginType CImage::OriginType() const
{
    return m_originType;
}
//---------------------------------------------------------------------------------------

void CImage::Allocate()
{
    unsigned int sizeInBytes = 4;

    switch(m_pixelformat)
    {
    case EPixelFormat_R8G8B8A8:
        sizeInBytes = 4;
        break;

    case EPixelFormat_B8G8R8A8:
        sizeInBytes = 4;
        break;

    case EPixelFormat_R8G8B8:
        sizeInBytes = 3;
        break;

    case EPixelFormat_B8G8R8:
        sizeInBytes = 3;
        break;

    default:
        break;
    }

    unsigned int numPixels = m_width * m_height * m_depth;

    SAFE_CLEANUP(m_pData);

    if(numPixels == 0)
    {
        return;
    }

    m_byteCount = numPixels * sizeInBytes;
    m_pData = new AUByte[m_byteCount];
}
//---------------------------------------------------------------------------------------

void CImage::SetColor(const CColor& color)
{
    if(nullptr == m_pData)
    {
        return;
    }

    AUByte r = color.RedAsByte();
    AUByte g = color.GreenAsByte();
    AUByte b = color.BlueAsByte();
    AUByte a = color.AlphaAsByte();

    unsigned int numPixels = m_width * m_height * m_depth;

    if(m_pixelformat == EPixelFormat_R8G8B8A8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 4 * i;
            m_pData[offset + 0] = r;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = b;
            m_pData[offset + 3] = a;
        }
    }
    else if(m_pixelformat == EPixelFormat_B8G8R8A8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 4 * i;
            m_pData[offset + 0] = b;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = r;
            m_pData[offset + 3] = a;
        }
    }
    else if(m_pixelformat == EPixelFormat_R8G8B8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 3 * i;
            m_pData[offset + 0] = r;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = b;
        }
    }
    else if(m_pixelformat == EPixelFormat_B8G8R8)
    {
        for(unsigned int i = 0; i < numPixels; i++)
        {
            unsigned int offset = 3 * i;
            m_pData[offset + 0] = b;
            m_pData[offset + 1] = g;
            m_pData[offset + 2] = r;
        }
    }
    else
    {
        //KK: Can also use memset...
        for(unsigned int i = 0; i < m_byteCount; i++)
        {
            m_pData[i] = 0;
        }
    }
}
//---------------------------------------------------------------------------------------

void CImage::Copy(const AUByte* data)
{
    if(nullptr == data)
    {
        return;
    }
    try
    {
        memcpy(m_pData, data, m_byteCount);
    }
    catch(...)
    {
        //Throw some error
    }
}
//---------------------------------------------------------------------------------------

