//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CGraphicsUtils.h"
#include "FileUtils/CStringUtils.h"
#include "CLayer2D.h"
#include "CCamera.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CLayer2D::CLayer2D(IViewport* viewport)
    : ILayer(viewport)
{
    m_name = std::string("2DLayer-") + CStringUtils::ToString(m_id);
}
//---------------------------------------------------------------------------------------

CLayer2D::~CLayer2D()
{
}
//---------------------------------------------------------------------------------------

void CLayer2D::Render()
{
}
//---------------------------------------------------------------------------------------

void CLayer2D::Resize(const AUInt32 width, const AUInt32 height)
{
}
//---------------------------------------------------------------------------------------

