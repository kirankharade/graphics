//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CVector3f.h"
#include "CMaterial.h"
#include "Defaults.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

AReal32 Defaults::PointSize = 1.0;

AReal32 Defaults::LineWidth = 1.0;

CColor Defaults::Color = CColor(EPredefinedColor::DarkGray);

CColor Defaults::BackGroundColor = CColor(EPredefinedColor::SkyBlue);

CColor Defaults::MaterialAmbient = CColor(EPredefinedColor::Black);

CColor Defaults::MaterialDiffuse = CColor(EPredefinedColor::LightSkyBlue);

CColor Defaults::MaterialEmissive = CColor(EPredefinedColor::Black);

CColor Defaults::MaterialSpecular = CColor(EPredefinedColor::White);

AReal32 Defaults::MaterialShininess = 80.0f;

CColor Defaults::LightAmbient = CColor(1.0f, 1.0f, 1.0f, 1.0f);

CColor Defaults::LightDiffuse = CColor(1.0f, 1.0f, 1.0f, 1.0f);

CColor Defaults::LightSpecular = CColor(1.0f, 1.0f, 1.0f, 1.0f);

Maths::CPoint3f Defaults::LightPosition = CPoint3f();

Maths::CVector3f Defaults::LightSpotDirection = CVector3f(0, 0, -1);

AReal32  Defaults::LightSpotCutOffAngle = 180.0;

AReal32  Defaults::LightSpotExponent = 0.0;

AReal32  Defaults::LightConstantAttenuation = 1.0;

AReal32  Defaults::LightLinearAttenuation = 0.0;

AReal32  Defaults::LightQuadraticAttenuation = 0.0;

EPresetViewType Defaults::Default3DPresetView = EPresetView_Front;

CColor Defaults::FrontMaterialAmbient = CColor(0.0f, 0.0f, 0.0f);

CColor Defaults::FrontMaterialDiffuse = CColor(0.0f, 0.0f, 0.0f);

CColor Defaults::FrontMaterialEmissive = CColor(0.0f, 0.5f, 0.9f);

CColor Defaults::FrontMaterialSpecular = CColor(1.0f, 1.0f, 1.0f);

AReal32 Defaults::FrontMaterialShininess = 80.0f;

CColor Defaults::BackMaterialAmbient = CColor(0.0f, 0.0f, 0.0f);

CColor Defaults::BackMaterialDiffuse = CColor(0.0f, 0.0f, 0.0f);

CColor Defaults::BackMaterialEmissive = CColor(0.7f, 0.5f, 0.3f);

CColor Defaults::BackMaterialSpecular = CColor(1.0f, 1.0f, 1.0f);

AReal32 Defaults::BackMaterialShininess = 60.0f;

CMaterial Defaults::FrontMaterial = CMaterial(Defaults::FrontMaterialAmbient,
                                              Defaults::FrontMaterialDiffuse,
                                              Defaults::FrontMaterialEmissive,
                                              Defaults::FrontMaterialSpecular,
                                              Defaults::FrontMaterialShininess
                                              );

CMaterial Defaults::BackMaterial = CMaterial(Defaults::BackMaterialAmbient,
                                             Defaults::BackMaterialDiffuse,
                                             Defaults::BackMaterialEmissive,
                                             Defaults::BackMaterialSpecular,
                                             Defaults::BackMaterialShininess
                                             );


//---------------------------------------------------------------------------------------

