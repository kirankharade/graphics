//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CMaterial.h"
#include "Defaults.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CMaterial::CMaterial()
    : m_ambient(Defaults::MaterialAmbient),
      m_diffuse(Defaults::MaterialDiffuse),
      m_emissive(Defaults::MaterialEmissive),
      m_specular(Defaults::MaterialSpecular),
      m_shininess(Defaults::MaterialShininess)
{
}
//---------------------------------------------------------------------------------------

CMaterial::CMaterial(const CColor& ambient, const CColor& diffuse, const CColor& emissive, 
                     const CColor& specular, const AReal32& shininess)
    : m_ambient(ambient),
      m_diffuse(diffuse),
      m_emissive(emissive),
      m_specular(specular),
      m_shininess(shininess)
{
}
//---------------------------------------------------------------------------------------

CMaterial::CMaterial(const CColor& diffuse)
    : m_ambient(Defaults::MaterialAmbient),
    m_diffuse(diffuse),
    m_emissive(Defaults::MaterialEmissive),
    m_specular(Defaults::MaterialSpecular),
    m_shininess(Defaults::MaterialShininess)
{
}
//---------------------------------------------------------------------------------------

CMaterial::CMaterial(const CMaterial& material)
    : m_ambient(material.m_ambient),
    m_diffuse(material.m_diffuse),
    m_emissive(material.m_emissive),
    m_specular(material.m_specular),
    m_shininess(material.m_shininess)
{
}
//---------------------------------------------------------------------------------------

CMaterial::~CMaterial( )
{
}
//---------------------------------------------------------------------------------------

CColor CMaterial::Ambient() const
{
    return m_ambient;
}
//---------------------------------------------------------------------------------------

void CMaterial::SetAmbient(const CColor& ambient)
{
    m_ambient = ambient;
}
//---------------------------------------------------------------------------------------

CColor CMaterial::Diffuse() const
{
    return m_diffuse;
}
//---------------------------------------------------------------------------------------

void CMaterial::SetDiffuse(const CColor& diffuse)
{
    m_diffuse = diffuse;
}
//---------------------------------------------------------------------------------------

CColor CMaterial::Emissive() const
{
    return m_emissive;
}
//---------------------------------------------------------------------------------------

void CMaterial::SetEmissive(const CColor& emissive)
{
    m_emissive = emissive;
}
//---------------------------------------------------------------------------------------

CColor CMaterial::Specular() const
{
    return m_specular;
}
//---------------------------------------------------------------------------------------

void CMaterial::SetSpecular(const CColor& specular)
{
    m_specular = specular;
}
//---------------------------------------------------------------------------------------

AReal32 CMaterial::Shininess() const
{
    return m_shininess;
}
//---------------------------------------------------------------------------------------

void CMaterial::SetShininess(const AReal32 shininess)
{
    m_shininess = shininess;
}
//---------------------------------------------------------------------------------------

CMaterial& CMaterial::operator = (const CMaterial& material)
{
    m_ambient   = material.m_ambient;
    m_diffuse   = material.m_diffuse;
    m_emissive  = material.m_emissive;
    m_specular  = material.m_specular;
    m_shininess = material.m_shininess;

    return *this;
}
//---------------------------------------------------------------------------------------

bool CMaterial::operator == (const CMaterial& material) const
{
    return ( (m_ambient == material.m_ambient) &&
             (m_diffuse == material.m_diffuse) &&
             (m_emissive == material.m_emissive) &&
             (m_specular == material.m_specular) &&
             (fabs(m_shininess - material.m_shininess) < 1.0f)
           );
}
//---------------------------------------------------------------------------------------

bool CMaterial::operator != (const CMaterial& material) const
{
    return !(*this == material);
}
//---------------------------------------------------------------------------------------

