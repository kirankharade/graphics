//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#include "../IGraphics.h"
//---------------------------------------------------------------------------------------
#ifndef __C_FIXED_FUNCTION_GRAPHICS_H__
#define __C_FIXED_FUNCTION_GRAPHICS_H__
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IDrawManager;
class CMaterial;
class IViewport;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CFixedFunctionGraphics : public IGraphics
{

public:

    CFixedFunctionGraphics();

    virtual ~CFixedFunctionGraphics();

    virtual EOpenGLEngineType Type() const;

	virtual void DefaultSetUp(IViewport* viewport);

    virtual IDrawManager* DrawManager();

	virtual AUInt16 MaxLightCount() const;

	virtual bool SetLight(CLight* light);

	virtual bool UnsetLight(CLight* light);

	virtual bool EnableLight(CLight* light);

	virtual bool DisableLight(CLight* light);

	virtual bool IsLightEnabled(const AInt16 index) const;

	virtual void EnableLighting();

	virtual void DisableLighting();

	virtual void ApplyMaterialToFrontFace(const CMaterial* material);

	virtual void ApplyMaterialToBackFace(const CMaterial* material);

    virtual void ApplyTransformation(const ETransformationType type, const Maths::CMatrix4f& matrix);

    virtual void SetViewport(const IViewport* viewport);

    virtual void SetAmbientLight(const CColor& ambientColor);


protected:

	void DefaultLightAndMaterial(IViewport* pViewport);

protected:

    IDrawManager*       m_pDrawManager;
    Maths::CMatrix4f    m_cameraViewMatrix;
    Maths::CMatrix4f    m_projectionMatrix;
    Maths::CMatrix4f    m_modelWorldMatrix;
    CMaterial*          m_pFrontFaceMaterial;
    CMaterial*          m_pBackFaceMaterial;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __C_FIXED_FUNCTION_GRAPHICS_H__
//---------------------------------------------------------------------------------------

