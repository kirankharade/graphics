//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CFixedFunctionGraphics.h"
#include "CFixedFunctionDrawManager.h"
#include "CFixedFunctionBackgroundPainter.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "../../Maths/CPoint3f.h"
#include "../../Maths/CPoint2f.h"
#include "../CBackGround.h"
#include "../Scene/I3DGeometry.h"
#include "../Scene/C3DMesh.h"
#include "../CBackGround.h"
#include "../IViewport.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CFixedFunctionBackgroundPainter::CFixedFunctionBackgroundPainter(IGraphics* graphics, CFixedFunctionDrawManager* drawManager)
    : m_pGraphics(graphics)
     ,m_pDrawManager(drawManager)
{
}
//---------------------------------------------------------------------------------------

CFixedFunctionBackgroundPainter::~CFixedFunctionBackgroundPainter()
{
    m_pGraphics = nullptr;
}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::SetUp() const
{
   glShadeModel(GL_SMOOTH);
   glClearDepth(1.0f);

   glEnable(GL_BLEND);
   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   //glEnable(GL_NORMALIZE);

   //glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, 1);

   //glPolygonMode(GL_FRONT, GL_FILL);

   glDisable(GL_CULL_FACE);

   glEnable(GL_DEPTH_TEST);
   glDepthFunc(GL_LEQUAL);

   glClear(GL_DEPTH_BUFFER_BIT);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::Draw(const CBackGround* pBackGround) const
{
    SetUp();

    if(!pBackGround)
    {
        DrawSingleColoredBackGround(Defaults::BackGroundColor);
    }
    else if(EBackGroundType_Default == pBackGround->Type())
    {
        DrawSingleColoredBackGround(Defaults::BackGroundColor);
    }
    EBackGroundType bgType = pBackGround->Type();

    if(bgType == EBackGroundType_Colored)
    {
        DrawColoredBackGround(pBackGround);
    }
    else if(bgType == EBackGroundType_Textured)
    {
        DrawTexturedBackGround(pBackGround);
    }
    else if(bgType == EBackGroundType_TextureBlendedWithColor)
    {
        DrawColoredTextureBlendedBackGround(pBackGround);
    }
}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::DrawSingleColoredBackGround(const CColor& color) const
{
    SColorRGBAf c = color.ToRGBAf();
    glClearColor(c.r, c.g, c.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::DrawColoredBackGround(const CBackGround* pBackGround) const
{

    /*
    Screen is divided into 4 quads, each of which further is divided into 2 triangles.
    Following diagram shows the indexing of the 9 vertices for the colored background.

    *-------*-------*
    |6      |7      |8
    |       |       |
    *-------*-------*
    |3      |4      |5
    |       |       |
    *-------*-------*
    0       1       2

    */

    EBackGroundColorType colorType = pBackGround->ColoringType();
    IViewport* vp = pBackGround->Viewport();
    AReal32 w = (AReal32) vp->Width();
    AReal32 h = (AReal32) vp->Height();
    AReal32 z = -1.0;
    AReal32 hw = 0.5f * w;
    AReal32 hh = 0.5f * h;

    CPoint3f vertices[9] = 
    {
        CPoint3f(0,   0, z),
        CPoint3f(hw,  0, z),
        CPoint3f(w,   0, z),
        CPoint3f(0,  hh, z),
        CPoint3f(hw, hh, z),
        CPoint3f(w,  hh, z),
        CPoint3f(0,   h, z),
        CPoint3f(hw,  h, z),
        CPoint3f(w,   h, z)
    };

    AInt32 indices8Triangles[24] = 
    {
        0, 1, 4,
        4, 3, 0,
        1, 2, 4,
        2, 5, 4,
        4, 5, 8,
        8, 7, 4,
        3, 4, 6,
        4, 7, 6
    };

    AInt32 indices4Triangles[12] = 
    {
        0, 4, 6,
        2, 4, 0,
        8, 4, 2,
        6, 4, 8
    };

    AInt32 indices2Triangles[6] = 
    {
        0, 2, 8,
        8, 6, 0,
    };

    CVector3f normals[9] = 
    {
        CVector3f(0, 0, 1), CVector3f(0, 0, 1), CVector3f(0, 0, 1),
        CVector3f(0, 0, 1), CVector3f(0, 0, 1), CVector3f(0, 0, 1),
        CVector3f(0, 0, 1), CVector3f(0, 0, 1), CVector3f(0, 0, 1)
    };

    CColor colors[9] = 
    {
        CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor),
        CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor),
        CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor), CColor(Defaults::BackGroundColor)
    };

    const CColor* c = pBackGround->Colors();

    switch(colorType)
    {
        case EBackGroundColorType_DefaultColor:
        {
            DrawSingleColoredBackGround(Defaults::BackGroundColor);
            return;
        }
        case EBackGroundColorType_SingleColor:
        {
            DrawSingleColoredBackGround(c[0]);
            return;
        }
        case EBackGroundColorType_TwoColorsVerticalGradient:
        {
            colors[0] = c[0];
            colors[1] = c[0];
            colors[2] = c[0];
            colors[3] = CColor::Average(c[0], c[1]);
            colors[4] = CColor::Average(c[0], c[1]);
            colors[5] = CColor::Average(c[0], c[1]);
            colors[6] = c[1];
            colors[7] = c[1];
            colors[8] = c[1];
            break;
        }
        case EBackGroundColorType_TwoColorsHorizontalGradient:
        {
            colors[0] = c[0];
            colors[1] = CColor::Average(c[0], c[1]);
            colors[2] = c[1];
            colors[3] = c[0];
            colors[4] = CColor::Average(c[0], c[1]);
            colors[5] = c[1];
            colors[6] = c[0];
            colors[7] = CColor::Average(c[0], c[1]);
            colors[8] = c[1];
            break;
        }
        case EBackGroundColorType_ThreeColorsVerticalGradient:
        {
            colors[0] = c[0];
            colors[1] = c[0];
            colors[2] = c[0];
            colors[3] = c[1];
            colors[4] = c[1];
            colors[5] = c[1];
            colors[6] = c[2];
            colors[7] = c[2];
            colors[8] = c[2];
            break;
        }
        case EBackGroundColorType_ThreeColorsHorizontalGradient:
        {
            colors[0] = c[0];
            colors[1] = c[1];
            colors[2] = c[2];
            colors[3] = c[0];
            colors[4] = c[1];
            colors[5] = c[2];
            colors[6] = c[0];
            colors[7] = c[1];
            colors[8] = c[2];
            break;
        }
        case EBackGroundColorType_FourCornersColorsGradient:
        {
            colors[0] = c[0];
            colors[1] = CColor::Average(c[0], c[1]);
            colors[2] = c[1];
            colors[3] = CColor::Average(c[0], c[3]);
            colors[4] = CColor::Average(c[0], c[1], c[2], c[3]);
            colors[5] = CColor::Average(c[1], c[2]);
            colors[6] = c[3];
            colors[7] = CColor::Average(c[2], c[3]);
            colors[8] = c[2];
            break;
        }
        case EBackGroundColorType_FiveColorsCornersAndCentreGradient:
        {
            colors[0] = c[0];
            colors[1] = CColor::Average(c[0], c[1]);
            colors[2] = c[1];
            colors[3] = CColor::Average(c[0], c[3]);
            colors[4] = c[4];
            colors[5] = CColor::Average(c[1], c[2]);
            colors[6] = c[3];
            colors[7] = CColor::Average(c[2], c[3]);
            colors[8] = c[2];
            break;
        }
        default:
        {
            DrawSingleColoredBackGround(Defaults::BackGroundColor);
            return;
        }
    }

    float colors3f[36] = 
    {
        colors[0].Red(), colors[0].Green(), colors[0].Blue(), 1.0f,
        colors[1].Red(), colors[1].Green(), colors[1].Blue(), 1.0f,
        colors[2].Red(), colors[2].Green(), colors[2].Blue(), 1.0f,
        colors[3].Red(), colors[3].Green(), colors[3].Blue(), 1.0f,
        colors[4].Red(), colors[4].Green(), colors[4].Blue(), 1.0f,
        colors[5].Red(), colors[5].Green(), colors[5].Blue(), 1.0f,
        colors[6].Red(), colors[6].Green(), colors[6].Blue(), 1.0f,
        colors[7].Red(), colors[7].Green(), colors[7].Blue(), 1.0f,
        colors[8].Red(), colors[8].Green(), colors[8].Blue(), 1.0f
    };

    GLenum err = glGetError();

    //m_pDrawManager->DefaultSetUp();

    //Apply ortographic 2D projection

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0,(GLsizei) w, (GLsizei) h, 0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    GLboolean* clippingPlanesStatus = m_pDrawManager->DisableClipping();

    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_LIGHTING_BIT);
    {
        glDisable(GL_LIGHTING);

        glEnableClientState(GL_VERTEX_ARRAY); 
        glVertexPointer(3 /*vertex size*/, GL_FLOAT, 0 /*stride*/, (float*)vertices); 
        glEnableClientState(GL_NORMAL_ARRAY); 
        glNormalPointer(GL_FLOAT, 0 /*stride*/, (float*)normals);
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4 /* color size */, GL_FLOAT, 0 /*stride*/, (float*)colors3f);

        if(colorType == EBackGroundColorType_TwoColorsVerticalGradient ||
           colorType == EBackGroundColorType_TwoColorsHorizontalGradient||
           colorType == EBackGroundColorType_ThreeColorsVerticalGradient||
           colorType == EBackGroundColorType_ThreeColorsHorizontalGradient)
        {
            glDrawElements(GL_TRIANGLES, (GLsizei) (8 /* 8 triangles*/ * 3), GL_UNSIGNED_INT, indices8Triangles);
        }
        else if(colorType == EBackGroundColorType_FiveColorsCornersAndCentreGradient)
        {
            glDrawElements(GL_TRIANGLES, (GLsizei) (4 /* 4 triangles*/ * 3), GL_UNSIGNED_INT, indices4Triangles);
        }
        else
        {
            glDrawElements(GL_TRIANGLES, (GLsizei) (2 /* 2 triangles*/ * 3), GL_UNSIGNED_INT, indices2Triangles);
        }

        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
    }
    glPopAttrib();
    m_pDrawManager->EnableClipping(clippingPlanesStatus, true);

    //Reset projection to identity
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::DrawTexturedBackGround(const CBackGround* pBackGround) const
{
    //Corrected upto here ...
    EBackGroundTextureType texLayoutType = pBackGround->TextureLayoutType();
    /*
    CPoint2f texCoords[9] = 
    {
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f(),
        CPoint2f()
    };

    //Texture Coords for stretching
    for(int i = 0; i < 9; i++)
    {
        texCoords[i].x = vertices[i].x / w;
        texCoords[i].y = vertices[i].y / h;
    }

    //Get texture image width and height for repeat and centre layout
    AReal32 tw = texture->Width();
    AReal32 th = texture->Height();

    //Repeat/tiled layout
    for(int i = 0; i < 9; i++)
    {
        texCoords[i].x = vertices[i].x / tw;
        texCoords[i].y = vertices[i].y / th;
    }
    */

    switch(texLayoutType)
    {
        case EBackGroundTextureType_Centred:
        {
            break;
        }
        case EBackGroundTextureType_Scaled:
        {
            break;
        }
        case EBackGroundTextureType_Tiled:
        {
            break;
        }
        default:
        {
            break;
        }
    }
    //For time being till textures are implemented
    DrawSingleColoredBackGround(Defaults::BackGroundColor);
}
//---------------------------------------------------------------------------------------

void CFixedFunctionBackgroundPainter::DrawColoredTextureBlendedBackGround(const CBackGround* pBackGround) const
{
    EBackGroundColorType colorType = pBackGround->ColoringType();
    EBackGroundTextureType texLayoutType = pBackGround->TextureLayoutType();

    //For time being till textures are implemented
    DrawSingleColoredBackGround(Defaults::BackGroundColor);
}
//---------------------------------------------------------------------------------------

