//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_FACTORY_H__
#define __I_FACTORY_H__
//---------------------------------------------------------------------------------------

#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "SGLContextParams.h"

//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IWindow;
class IGraphics;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IFactory
{

protected:

    IFactory(){};

public:

    virtual ~IFactory(){};

    virtual IWindow* Window(const SGLContextParams& contextParams, WindowID windowID) const = 0;

	virtual IWindow* Window(const SGLContextParams& contextParams, int width, int height) const = 0;

    virtual IWindow* Window(WindowID windowID) const = 0;

    //Get Factory instance
    static IFactory* Factory();

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //__I_FACTORY_H__
//---------------------------------------------------------------------------------------

