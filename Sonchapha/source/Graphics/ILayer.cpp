//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/CUtils.h"
#include "CGraphicsUtils.h"
#include "FileUtils/CStringUtils.h"
#include "ILayer.h"
#include "Scene/IScene.h"
#include "CCamera.h"
#include "CViewport.h"
#include "Scene/ISceneNode.h"
#include <map>

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

typedef std::map<ESceneNodeType, std::vector<ISceneNode*>> NodeMap;
typedef std::map<ESceneNodeType, std::vector<ISceneNode*>>::iterator NodeMapIterator;

//---------------------------------------------------------------------------------------

ILayer::ILayer(IViewport* viewport)
        : m_pViewport(viewport)
         ,m_id(CGraphicsUtils::GetUniqueObjectID())
         ,m_name(std::string("Layer"))
         ,m_pRenderRegister(nullptr)
         ,m_pScene(nullptr)
{
    m_name = std::string("Layer-") + CStringUtils::ToString(m_id);
    m_pRenderRegister = new std::map<ESceneNodeType, std::vector<ISceneNode*>>();
}
//---------------------------------------------------------------------------------------

ILayer::~ILayer()
{
    SAFE_CLEANUP(m_pRenderRegister);
}
//---------------------------------------------------------------------------------------

std::string ILayer::Name() const
{
    return m_name;
}
//---------------------------------------------------------------------------------------

AIndex64 ILayer::Id() const
{
    return m_id;
}
//---------------------------------------------------------------------------------------

void ILayer::RegisterNodeToRender(ISceneNode* node, ESceneNodeType nodeType)
{
    if(!node)
    {
        return;
    }

    NodeMap* m = (NodeMap*) (m_pRenderRegister);
    if(!m)
    {
        return;
    }
    NodeMapIterator itr = m->find(nodeType);
    if(itr == m->end())
    {
        std::vector<ISceneNode*> v;
        m->insert(std::make_pair(nodeType, v));
    }
    else
    {
        (itr->second).push_back(node);
    }

}
//---------------------------------------------------------------------------------------

const std::vector<ISceneNode*>& ILayer::GetRenderableNodes(ESceneNodeType nodeType) const
{
    NodeMap* m = (NodeMap*) (m_pRenderRegister);
    NodeMapIterator itr = m->find(nodeType);
    if(itr == m->end())
    {
        std::vector<ISceneNode*> v;
        m->insert(std::make_pair(nodeType, v));
    }
    itr = m->find(nodeType);
    return (itr->second);
}
//---------------------------------------------------------------------------------------

IScene* ILayer::Scene()
{
    return m_pScene;
}
//---------------------------------------------------------------------------------------

void ILayer::SetScene(IScene* scene)
{
    m_pScene = scene;
}
//---------------------------------------------------------------------------------------

void ILayer::PreRenderOperations()
{
    if(m_pScene)
    {
        m_pScene->RegisterToRender(this);
    }
}
//---------------------------------------------------------------------------------------

void ILayer::PostRenderOperations()
{
}
//---------------------------------------------------------------------------------------
