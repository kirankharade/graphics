//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C3DSceneNode.h"
#include "C3DScene.h"
#include "IScene.h"
#include "../ILayer.h"
#include "../IGraphics.h"
#include "../IFactory.h"
#include "CSceneNodeCollection.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C3DSceneNode::C3DSceneNode(C3DSceneNode* parentNode, C3DScene* scene)
    : ISceneNode(parentNode, scene)
    ,m_centre(CPoint3f())
    ,m_boundingBox(CAxisOrientedBox())
    ,m_absoluteMat(CMatrix4f())
    ,m_relativeMat(CMatrix4f())
{
}
//---------------------------------------------------------------------------------------

C3DSceneNode::~C3DSceneNode()
{
}
//---------------------------------------------------------------------------------------

bool C3DSceneNode::AddChild(ISceneNode* pChildNode)
{
    if(nullptr == pChildNode)
    {
        return false;
    }
    C3DSceneNode* node = dynamic_cast<C3DSceneNode*> (pChildNode);
    if(nullptr == node)
    {
        return false;
    }
    return ISceneNode::AddChild(node);
}
//---------------------------------------------------------------------------------------

void C3DSceneNode::Render()
{
    IGraphics* g = IGraphics::Gfx();
    if(g)
    {
        g->ApplyTransformation(ETransformationType_ModelWorld, AbsoluteTransformation());
    }
}
//---------------------------------------------------------------------------------------

void C3DSceneNode::RegisterForRendering(ILayer* pLayer)
{
    ISceneNode::RegisterForRendering(pLayer);
}
//---------------------------------------------------------------------------------------

Maths::CPoint3f C3DSceneNode::Centre() const
{
    return m_centre;
}
//---------------------------------------------------------------------------------------

const Maths::CAxisOrientedBox& C3DSceneNode::Bounds()
{
    UpdateBounds();
    return m_boundingBox;
}
//---------------------------------------------------------------------------------------

//KK: Add rotation and translation as memebers

const CMatrix4f& C3DSceneNode::AbsoluteTransformation() const
{
    return m_absoluteMat;
}
//---------------------------------------------------------------------------------------

const Maths::CMatrix4f& C3DSceneNode::RelativeTransformation() const
{
    return m_relativeMat;
}
//---------------------------------------------------------------------------------------

void C3DSceneNode::SetRelativeTransformation(const Maths::CMatrix4f& mat)
{
    m_relativeMat = mat;
}
//---------------------------------------------------------------------------------------

void C3DSceneNode::CalculateAbsoluteTransformation()
{
    C3DSceneNode* parent = dynamic_cast<C3DSceneNode*> (m_pParent);
    if(parent != nullptr)
    {
        m_absoluteMat = m_relativeMat * AbsoluteTransformation();
    }
}
//---------------------------------------------------------------------------------------

void C3DSceneNode::UpdateBounds()
{
    if(m_pNodeCollection)
    {
        AUInt32 count = m_pNodeCollection->Count();
        for(AUInt32 i = 0; i < count; i++)
        {
            C3DSceneNode* node = dynamic_cast<C3DSceneNode*> ((*m_pNodeCollection)[i]);
            if(node)
            {
                node->UpdateBounds();
                const CAxisOrientedBox& box = node->Bounds();
                if(box.IsValid())
                {
                    this->m_boundingBox += box;
                }
            }
        }
    }
}
//---------------------------------------------------------------------------------------
