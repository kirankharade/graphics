//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_2D_SCENE_NODE_H__
#define __C_2D_SCENE_NODE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "ISceneNode.h"
#include "../../Maths/CPoint2f.h"
#include "../../Maths/CAxisOrientedRectangle.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class C2DScene;
class ILayer;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API C2DSceneNode : public ISceneNode
{

public:

    C2DSceneNode(C2DSceneNode* parentNode, C2DScene* scene);

    virtual ~C2DSceneNode();

    virtual void Render();

    virtual void RegisterForRendering(ILayer* pLayer);

    Maths::CPoint2f Centre() const;

    const Maths::CAxisOrientedRectangle& Bounds() const;

protected:

    void RegisterSelf(ILayer* pLayer);

protected:

    Maths::CPoint2f                  m_centre;
    Maths::CAxisOrientedRectangle    m_boundingBox;

    friend class IScene;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_2D_SCENE_NODE_H__
//---------------------------------------------------------------------------------------

