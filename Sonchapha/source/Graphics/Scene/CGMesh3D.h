//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __C_G_MESH_H__
#define __C_G_MESH_H__
//---------------------------------------------------------------------------------------
#include "IGeometry3D.h"
#include "../GraphicsIncludes.h"
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API CGMesh3D : public IGeometry3D
{

public:

   CGMesh3D()
   {
      m_vertices.clear();
      m_normals.clear();
      m_indices.clear();
      m_name = "";
   }

   virtual ~CGMesh3D()
   {
      m_vertices.clear();
      m_normals.clear();
      m_indices.clear();
   }

   virtual EDrawableType Type() const { return EDrawableType_Mesh3D; }

   std::vector<Maths::CPoint3f>& Vertices() { return m_vertices; }

   const std::vector<Maths::CPoint3f>& Vertices() const { return m_vertices; }

   std::vector<Maths::CVector3f>& Normals() { return m_normals; }

   const std::vector<Maths::CVector3f>& Normals() const { return m_normals; }

   std::vector<int>& Indices() { return m_indices; }

   const std::vector<int>& Indices() const { return m_indices; }

   int TriangleCount() const { return m_indices.size() / 3; }

private:

   Maths::CPoint3fList         m_vertices;
   Maths::CVector3fList        m_normals;
   std::vector<int>            m_indices;

};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __C_G_MESH_H__
//---------------------------------------------------------------------------------------


