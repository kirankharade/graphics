//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "C2DScene.h"
#include "IScene.h"

//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

C2DScene* C2DScene::Create()
{
    C2DScene* scene = new C2DScene();
    scene->CheckInitialization();
    return scene;
}
//---------------------------------------------------------------------------------------

C2DScene::C2DScene()
    : IScene()
{
}
//---------------------------------------------------------------------------------------

C2DScene::~C2DScene()
{
}
//---------------------------------------------------------------------------------------

Maths::CPoint2f C2DScene::Centre() const
{
    return m_centre;
}
//---------------------------------------------------------------------------------------

const Maths::CAxisOrientedRectangle& C2DScene::Bounds() const
{
    return m_boundingBox;
}
//---------------------------------------------------------------------------------------

