//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "../GraphicsIncludes.h"
#include "../../Base/Namespaces.h"
#include "../../Maths/CUtils.h"
#include "IScene.h"
#include "ISceneNode.h"
#include "CSceneNodePool.h"
#include <map>
#include <string>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;
using namespace std;

//---------------------------------------------------------------------------------------
typedef map<AIndex64, ISceneNode*, std::less<AIndex64>, std::allocator<std::pair<const AIndex64, ISceneNode*>>> IdNodeMap;
typedef map<string, ISceneNode*, std::less<string>, std::allocator<std::pair<const string, ISceneNode*>>> NameNodeMap;
typedef map<AIndex64, ISceneNode*, std::less<AIndex64>, std::allocator<std::pair<const AIndex64, ISceneNode*>>>::iterator IdNodeMapIterator;
typedef map<string, ISceneNode*, std::less<string>, std::allocator<std::pair<const string, ISceneNode*>>>::iterator NameNodeMapIterator;
//---------------------------------------------------------------------------------------

CSceneNodePool::CSceneNodePool(IScene* scene)
    :m_pScene(scene)
    ,m_pIdNodeMap(nullptr)
    ,m_pNameNodeMap(nullptr)
{
    m_pIdNodeMap = new map<AIndex64, ISceneNode*>();
    m_pNameNodeMap = new map<string, ISceneNode*>();
}
//---------------------------------------------------------------------------------------

CSceneNodePool::~CSceneNodePool()
{
    if(m_pIdNodeMap)
    {
        ((IdNodeMap*) m_pIdNodeMap)->clear();
        SAFE_CLEANUP(m_pIdNodeMap);
    }
    if(m_pNameNodeMap)
    {
        ((NameNodeMap*) m_pNameNodeMap)->clear();
        SAFE_CLEANUP(m_pNameNodeMap);
    }
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Add(ISceneNode* pChildNode)
{
    if(pChildNode)
    {
        if(Exists(pChildNode->Name()))
        {
            return false;
        }
        if(Exists(pChildNode->Id()))
        {
            return false;
        }
        string name = pChildNode->Name();
        AIndex64 id = pChildNode->Id();

        ((NameNodeMap*) m_pNameNodeMap)->insert(std::make_pair(name, pChildNode));
        ((IdNodeMap*) m_pIdNodeMap)->insert(std::make_pair(id, pChildNode));

        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Detach(ISceneNode* pChildNode)
{
    if(!pChildNode)
    {
        return false;
    }
    NameNodeMapIterator itrn = ((NameNodeMap*) m_pNameNodeMap)->find(pChildNode->Name());
    if(itrn != ((NameNodeMap*) m_pNameNodeMap)->end())
    {
        ((NameNodeMap*) m_pNameNodeMap)->erase(itrn);
    }
    IdNodeMapIterator itri = ((IdNodeMap*) m_pIdNodeMap)->find(pChildNode->Id());
    if(itri != ((IdNodeMap*) m_pIdNodeMap)->end())
    {
        ((IdNodeMap*) m_pIdNodeMap)->erase(itri);
    }
    return true;
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Detach(const std::string& name)
{
    return Detach(Search(name));
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Detach(const AIndex64& id)
{
    return Detach(Search(id));
}
//---------------------------------------------------------------------------------------

ISceneNode* CSceneNodePool::Search(const std::string& name)
{
    NameNodeMapIterator itr = ((NameNodeMap*) m_pNameNodeMap)->find(name);
    if(itr != ((NameNodeMap*) m_pNameNodeMap)->end())
    {
        return itr->second;
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

ISceneNode* CSceneNodePool::Search(const AIndex64& id)
{
    IdNodeMapIterator itr = ((IdNodeMap*) m_pIdNodeMap)->find(id);
    if(itr != ((IdNodeMap*) m_pIdNodeMap)->end())
    {
        return itr->second;
    }
    return nullptr;
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Exists(const std::string& name) const
{
    NameNodeMapIterator itr = ((NameNodeMap*) m_pNameNodeMap)->find(name);
    if(itr != ((NameNodeMap*) m_pNameNodeMap)->end())
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------

bool CSceneNodePool::Exists(const AIndex64& id) const
{
    IdNodeMapIterator itr = ((IdNodeMap*) m_pIdNodeMap)->find(id);
    if(itr != ((IdNodeMap*) m_pIdNodeMap)->end())
    {
        return true;
    }
    return false;
}
//---------------------------------------------------------------------------------------
