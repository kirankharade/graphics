//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_SCENE_NODE_H__
#define __I_SCENE_NODE_H__
//---------------------------------------------------------------------------------------
#include "../GraphicsIncludes.h"
#include "../GraphicsSTLExports.h"
#include "IDrawable.h"
#include "../CMaterial.h"
#include <string>
#include <vector>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class ISceneNode;
class IScene;
class CSceneNodeCollection;
class ILayer;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API ISceneNode
{

public:

    virtual ~ISceneNode();

    virtual void Render() = 0;

    IScene* Scene() const;

    ISceneNode* Parent() const;

    virtual bool AddChild(ISceneNode* pChildNode);

    bool DetachChild(ISceneNode* pChildNode);

    bool DetachChild(const std::string& nodeName);

    bool DetachChild(const AIndex64& nodeId);

    void RemoveFromScene();

    bool IsChildNode(const std::string& name) const;

    bool IsChildNode(const AIndex64& id) const;

    ISceneNode* Child(const std::string& name);

    ISceneNode* Child(const AIndex64& id);

    bool IsDescendentNode(const std::string& name) const;

    bool IsDescendentNode(const AIndex64& id) const;

    AUInt32 ChildrenCount() const;

    CSceneNodeCollection*& Children();

    const CSceneNodeCollection* Children() const;

    const std::string& Name() const;

    void SetName(const std::string& name);

    AIndex64 Id() const;

    void SetMaterial(CMaterial*& material);

    CMaterial* Material();

    const CMaterial* Material() const;

    bool Visible() const;

    void SetVisible(const bool bVisible);

    virtual void RegisterForRendering(ILayer* pLayer);

protected:

    ISceneNode(ISceneNode* parentNode, IScene* scene);

    static void SetParent(ISceneNode* node, ISceneNode* parent);

    void SetScene(IScene* scene);

    void SetSceneAffiliation(IScene* scene);

    void SetSceneAffiliation(IScene* scene, const bool detachFromParent);

    void RemoveFromScene(const bool detachFromParent);

    void RegisterSelf(ILayer* pLayer);

protected:

    CSceneNodeCollection*       m_pNodeCollection;
    ISceneNode*                 m_pParent;
    IScene*                     m_pScene;
    std::string                 m_name;
    AIndex64                    m_id;
    bool                        m_bIsVisible;
    CMaterial*                  m_pMaterial;

    friend class IScene;
    friend class CSceneNodeCollection;
};
//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif //  __I_SCENE_NODE_H__
//---------------------------------------------------------------------------------------

