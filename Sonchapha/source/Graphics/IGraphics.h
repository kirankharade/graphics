//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
#ifndef __I_GRAPHICS_H__
#define __I_GRAPHICS_H__
//---------------------------------------------------------------------------------------
#include "GraphicsIncludes.h"
#include "GraphicsDefines.h"
#include "SGLContextParams.h"
#include "GraphicsEnums.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CMatrix4f.h"
#include "CColor.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_START_NAMESPACE
//---------------------------------------------------------------------------------------
class IDrawManager;
class CLight;
class CMaterial;
class IViewport;
class IFactory;
//class IFontManager;
//class IEffectManager;
//---------------------------------------------------------------------------------------

class AN_GRAPHICS_API IGraphics
{

public:

    IGraphics() {}

    virtual ~IGraphics() {}

    virtual EOpenGLEngineType Type() const = 0;

	virtual void DefaultSetUp(IViewport* pViewport) = 0;

    virtual IDrawManager* DrawManager() = 0;

	virtual AUInt16 MaxLightCount() const = 0;

	virtual bool SetLight(CLight* light) = 0;

	virtual bool UnsetLight(CLight* light) = 0;

	virtual bool EnableLight(CLight* light) = 0;

	virtual bool DisableLight(CLight* light) = 0;

	virtual bool IsLightEnabled(const AInt16 index) const = 0;

	virtual void EnableLighting() = 0;

	virtual void DisableLighting() = 0;

	virtual void ApplyMaterialToFrontFace(const CMaterial* material) = 0;

	virtual void ApplyMaterialToBackFace(const CMaterial* material) = 0;

    virtual void ApplyTransformation(const ETransformationType type, const Maths::CMatrix4f& matrix) = 0;

    virtual void SetViewport(const IViewport* viewport) = 0;

    virtual void SetAmbientLight(const CColor& ambientColor) = 0;

    //virtual IEffectManager* EffectManager() = 0;

    static void InitializeGraphics(const EWindowingSystemType windowingSystemType, 
                                   const EOpenGLEngineType engineType);
    static void InitializeGraphics(WindowID windowID, 
                                   const EWindowingSystemType windowingSystemType, 
                                   const EOpenGLEngineType engineType);
    static void InitializeGraphics(WindowID windowID, 
                                   SGLContextParams& contextParams, 
                                   const EWindowingSystemType windowingSystemType, 
                                   const EOpenGLEngineType engineType);

    static IGraphics* Gfx();

    static IFactory* Factory();

protected:

    static void GenerateGraphics(const EOpenGLEngineType engineType);
    static void GenerateFactory(WindowID windowID, const EWindowingSystemType windowingSystemType);
    static void GenerateFactory(WindowID windowID, SGLContextParams& contextParams, const EWindowingSystemType windowingSystemType);

    static EWindowingSystemType    m_sWindowingSystem;
    static EOpenGLEngineType       m_sEngineType;
    static IGraphics*              m_spGraphics;
    static bool                    m_sInitialized;

};

//---------------------------------------------------------------------------------------
AN_GRAPHICS_END_NAMESPACE
//---------------------------------------------------------------------------------------
#endif // __I_GRAPHICS_H__
//---------------------------------------------------------------------------------------

