//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "CEventProcessor.h"
#include "SEvent.h"
#include "CViewport.h"
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CEventProcessor::CEventProcessor(CViewport* viewport)
    :m_pViewport(viewport)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnEvent(const SEvent& ev)
{
    //Set here the defalt actions on the events
	if(ev.type == EEventType_Paint)
	{
		OnPaint();
	}
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnPaint()
{
	if(m_pViewport)
	{
		m_pViewport->Render();
	}
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnLeftButtonDoubleClick(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnLeftButtonUp(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnLeftButtonDown(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnRightButtonDoubleClick(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnRightButtonDown(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnRightButtonUp(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMiddleButtonDoubleClick(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMiddleButtonDown(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMiddleButtonUp(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMouseMove(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMouseWheel(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMouseHover(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMouseLeave(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnMouseEnter(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnKey(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------

void CEventProcessor::OnSize(const SEvent& ev)
{
}
//---------------------------------------------------------------------------------------


