//Copyright(c) xxxx
//---------------------------------------------------------------------------------------

#include <iostream>
#include "GraphicsIncludes.h"
#include "../Base/Namespaces.h"
#include "../Maths/MathsEnums.h"
#include "../Maths/CMatrix4f.h"
#include "../Maths/CPoint3f.h"
#include "../Maths/CVector3f.h"
#include "CCamera.h"
#include "CCameraUtils.h"
#include "CViewport.h"
#include "IEventProcessor.h"
#include "CEventProcessor.h"
#include "CLayer3D.h"
#include "CLayer2D.h"
#include "CBackGround.h"
#include "CCameraFrustum.h"
#include "SEvent.h"
#include "Defaults.h"
//#include <mutex>
//---------------------------------------------------------------------------------------

using namespace An;
using namespace An::Maths;
using namespace An::Graphics;

//---------------------------------------------------------------------------------------

CCamera::CCamera(IViewport* vp)
:  m_pViewport(vp)
 , m_bOrthogonal(true)
 , m_position(CPoint3f(0, 0, -100.0))
 , m_target(ORIGIN)
 , m_upVector(Y_AXIS)
 , m_near(1.0f)
 , m_far(1000.0f)
 , m_fovRad((float) DEG_TO_RAD64(35.0))
 , m_sceneHeight(500.0f)
 , m_sceneWidth(750.0f)
 , m_aspectRatio(1.5f)
 , m_pViewFrustum(nullptr)
 , m_fitViewMargin(1.05f)
 , m_handedness(ECoordSystemRightHanded)
{
	Initialize();
}
//---------------------------------------------------------------------------------------

CCamera::~CCamera()
{
    SAFE_CLEANUP(m_pViewFrustum);
}
//---------------------------------------------------------------------------------------

void CCamera::Initialize()
{
    int screenWidth  = m_pViewport->Width();
    int screenHeight = m_pViewport->Height();
    m_aspectRatio = ( (float) screenWidth ) / ( (float) screenHeight );

    m_pViewFrustum = new CCameraFrustum();

    // Recalculate upvector if the it is same as viewdir
    CVector3f viewdir = m_target - m_position;
    viewdir.normalize();
    m_upVector.normalize();

    float dot = viewdir.dot(m_upVector);
    if(EQ_32(dot, 1.f))
    {
        m_upVector.x += 0.5f;
    }

    RecalculateProjectionMatrix();
    RecalculateViewMatrix();
}
//---------------------------------------------------------------------------------------

ECoordSystemHandedness CCamera::CoordSystemConvention() const
{
    return m_handedness;
}
//---------------------------------------------------------------------------------------

void CCamera::SetLeftHanded()
{
    m_handedness = ECoordSystemLeftHanded;
}
//---------------------------------------------------------------------------------------

void CCamera::SetRightHanded()
{
    m_handedness = ECoordSystemRightHanded;
}
//---------------------------------------------------------------------------------------

bool CCamera::IsLeftHanded() const
{
    return (m_handedness == ECoordSystemLeftHanded);
}
//---------------------------------------------------------------------------------------

bool CCamera::IsRightHanded() const
{
    return (m_handedness == ECoordSystemRightHanded);
}
//---------------------------------------------------------------------------------------

void CCamera::SetPerspectiveProjection()
{
    m_bOrthogonal = false;
}
//---------------------------------------------------------------------------------------

void CCamera::SetOrthographicsProjection()
{
    m_bOrthogonal = true;
}
//---------------------------------------------------------------------------------------

bool CCamera::IsPerspectiveProjection() const
{
    return (m_bOrthogonal == false);
}
//---------------------------------------------------------------------------------------

bool CCamera::IsOrthographicsProjection() const
{
    return (m_bOrthogonal == true);
}
//---------------------------------------------------------------------------------------

void CCamera::RecalculateAspectRatio(const int screenHeight, const int screenWidth)
{
    m_sceneHeight = (float) screenHeight;
    m_sceneWidth = (float) screenWidth;
    m_aspectRatio = ( (float) screenWidth ) / ( (float) screenHeight );
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetProjectionMatrix(const CMatrix4f& projection, bool isOrthogonal)
{
    m_bOrthogonal = isOrthogonal;
    m_pViewFrustum->ProjectionMatrix() = projection;
}
//---------------------------------------------------------------------------------------

const CMatrix4f& CCamera::ProjectionMatrix() const
{
    return m_pViewFrustum->ProjectionMatrix();
}
//---------------------------------------------------------------------------------------

const CMatrix4f& CCamera::ViewMatrix() const
{
    return m_pViewFrustum->ViewMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetViewAffectorMatrix(const CMatrix4f& affector)
{
    m_affectorMatrix = affector;
}
//---------------------------------------------------------------------------------------

const CMatrix4f& CCamera::ViewAffectorMatrix() const
{
    return m_affectorMatrix;
}
//---------------------------------------------------------------------------------------

void CCamera::SetPositionTargetAndUpVector(const CPoint3f& pos, const CPoint3f& target, const CVector3f& up)
{
    m_position = pos;
    m_target = target;
    m_upVector = up;
    RecalculateViewMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetPosition(const CPoint3f& pos)
{
    m_position = pos;
    RecalculateViewMatrix();
}
//---------------------------------------------------------------------------------------

const CPoint3f& CCamera::Position() const
{
    return m_position;
}
//---------------------------------------------------------------------------------------

void CCamera::SetTarget(const CPoint3f& target)
{
    m_target = target;
    RecalculateViewMatrix();
}
//---------------------------------------------------------------------------------------

const CPoint3f& CCamera::Target() const
{
    return m_target;
}
//---------------------------------------------------------------------------------------

CVector3f CCamera::ViewDirection() const
{
    CVector3f v = m_target - m_position;
    v.normalize();
    return v;
}
//---------------------------------------------------------------------------------------

const CVector3f CCamera::HorizontalDirection() const
{
    CVector3f v = m_target - m_position;
    v.normalize();
    CVector3f up = m_upVector;
    up.normalize();
    return v.cross(up);
}
//---------------------------------------------------------------------------------------

void CCamera::SetUpVector(const CVector3f& vec)
{
    m_upVector = vec;
    RecalculateViewMatrix();
}
//---------------------------------------------------------------------------------------

const CVector3f& CCamera::UpVector() const
{
    return m_upVector;
}
//---------------------------------------------------------------------------------------

float CCamera::Near() const 
{
    return m_near;
}
//---------------------------------------------------------------------------------------

float CCamera::Far() const 
{
    return m_far;
}
//---------------------------------------------------------------------------------------

float CCamera::AspectRatio() const 
{
    return m_aspectRatio;
}
//---------------------------------------------------------------------------------------

float CCamera::FieldOfViewAngle() const 
{
    return m_fovRad;
}
//---------------------------------------------------------------------------------------

float CCamera::ViewHeight() const 
{
    return m_sceneHeight;
}
//---------------------------------------------------------------------------------------

void CCamera::SetNear(const float near)
{
    m_near = near;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetFar(const float far)
{
    m_far = far;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetAspectRatio(const float f)
{
    m_aspectRatio = f;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetAspectRatio(const int screenHeight, const int screenWidth)
{
    RecalculateAspectRatio(screenHeight, screenWidth);
}
//---------------------------------------------------------------------------------------

void CCamera::SetFieldOfViewAngle(const float f)
{
    m_fovRad = f;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetNearFarAndViewHeight(const float near, const float far, const float viewHeight)
{
    m_near = near;
    m_far = far;
    m_sceneHeight = viewHeight;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::SetViewHeight(float height, const bool bConsiderAspectRatio)
{
    if( height == 0.0f )
    {
        return;
    }
    if(bConsiderAspectRatio)
    {
        float minSide = (float) ( ViewportHeight() > ViewportWidth() ? ViewportWidth() : ViewportHeight() );
        float factor = ( ViewportHeight() ) / minSide;
        height *= factor;
    }

    m_sceneHeight = height;
    RecalculateProjectionMatrix();
}
//---------------------------------------------------------------------------------------

void CCamera::RecalculateProjectionMatrix()
{
    if(m_aspectRatio <= TOLERANCE_32)
    {
        m_aspectRatio = 1.5f;
    }
    m_sceneWidth = (m_sceneHeight * m_aspectRatio);

    CMatrix4f m;

    if(m_handedness == ECoordSystemLeftHanded)
    {
        if(m_bOrthogonal)
        {
            m = m.orthographicProjectionLHS(m_sceneWidth, m_sceneHeight, m_near, m_far);
        }
        else
        {
            m = m.perspectiveProjectionFieldOfViewLHS((float) RAD_TO_DEG64(m_fovRad), m_aspectRatio, m_near, m_far);
        }
    }
    else
    {
        if(m_bOrthogonal)
        {
            m = m.orthographicProjectionRHS(m_sceneWidth, m_sceneHeight, m_near, m_far);
        }
        else
        {
            m = m.perspectiveProjectionFieldOfViewRHS((float) RAD_TO_DEG64(m_fovRad), m_aspectRatio, m_near, m_far);
        }
    }

    m_pViewFrustum->ProjectionMatrix() =  m;
}
//---------------------------------------------------------------------------------------

const CCameraFrustum* CCamera::ViewFrustum() const
{
    return m_pViewFrustum;
}
//---------------------------------------------------------------------------------------

void CCamera::SanitizeUpVector()
{
    CVector3f v = m_target - m_position;
    v.normalize();

    CVector3f u = m_upVector;
    u.normalize();

    AReal32 d = v.dot(u);
    if(EQ_32(d, 1.0f))
    {
        //If they are in same direction,...god save us! or perturb the upVector slightly...
        u.x += 0.3f;
        m_upVector = u;
    }
}
//---------------------------------------------------------------------------------------

void CCamera::RecalculateViewMatrix()
{
    SanitizeUpVector();

    CMatrix4f m;
    if(m_handedness == ECoordSystemLeftHanded)
    {
        m_position.z = - m_position.z;

        m = m.cameraLookAtLHS(m_position, m_target, m_upVector);
    }
    else
    {
        m = m.cameraLookAtRHS(m_position, m_target, m_upVector);
    }
    m_pViewFrustum->ViewMatrix() = m;
}
//---------------------------------------------------------------------------------------

int CCamera::ViewportHeight() const
{
    return m_pViewport->Height();
}
//---------------------------------------------------------------------------------------

int CCamera::ViewportWidth() const
{
    return m_pViewport->Width();
}
//---------------------------------------------------------------------------------------

int CCamera::ViewportXPosition() const
{
    return 0;
}
//---------------------------------------------------------------------------------------

int CCamera::ViewportYPosition() const
{
    return 0;
}
//---------------------------------------------------------------------------------------

void CCamera::InitializeForScene(const CPoint3f& sceneCentre, const float sceneBoundRadius)
{
    float radius = sceneBoundRadius;
    if (EQ_32(0.0f, radius))
    {
        radius = 100.0f;
    }
    CVector3f up;
    CPoint3f target, pos;
    float sceneExtent = CCameraUtils::GetCameraResetParameters(this, sceneCentre, radius, Defaults::Default3DPresetView, nullptr, target, pos, up);

    SetPositionTargetAndUpVector(pos, target, up);
    SetNearFarAndViewHeight(0.1f, sceneExtent * 10.0f, sceneExtent);
}
//---------------------------------------------------------------------------------------

void CCamera::FitToScene(const CPoint3f& sceneCentre, const float sceneBoundRadius, bool bSmoothTransitions)
{
    float radius = sceneBoundRadius;
    if (EQ_32(0.0f, radius))
    {
        radius = 100.0f;
    }

    float sceneExtent = CCameraUtils::CalculateSceneExtent(this, sceneCentre, radius);
    if (bSmoothTransitions)
    {
        //Smooth transitions
    }
    else
    {
        SetNearFarAndViewHeight(0.1f, sceneExtent * 10.0f, sceneExtent);
    }
}
//---------------------------------------------------------------------------------------

void CCamera::FitToSceneWithPresetView(const CPoint3f& sceneCentre, const float sceneBoundRadius, EPresetViewType viewType,
                                       bool bSmoothTransitions)
{
    float radius = sceneBoundRadius;
    if (EQ_32(0.0f, radius))
    {
        radius = 100.0f;
    }

    CVector3f up;
    CPoint3f target, pos;
    float sceneExtent = CCameraUtils::GetCameraResetParameters(this, sceneCentre, radius, viewType, nullptr, target, pos, up);

    if (bSmoothTransitions)
    {
        //Smooth transitions
    }
    else
    {
        SetPositionTargetAndUpVector(pos, target, up);
        SetNearFarAndViewHeight(0.1f, sceneExtent * 10.0f, sceneExtent);
    }
}
//---------------------------------------------------------------------------------------

void CCamera::SetForRendering()
{
    //Set the view and projection matrices to the graphics system
    //Set the view matrix based on camera parameters target, position and up-vector
    //Check for the up-vector being the same as the view direction

    CPoint3f position = Position();
    CVector3f viewDirection = m_target - position;
    viewDirection.normalize();
    CVector3f up = m_upVector;
    up.normalize();

    float dot = up.dot(viewDirection);
    if (EQ_32(dot, 1.0f))
    {
        up.z += 0.7f;
    }
    up.normalize();

    RecalculateViewMatrix();
    RecalculateProjectionMatrix();

    //TO DO: Set the projection and view matrices to the graphics system
}
//---------------------------------------------------------------------------------------

void CCamera::SetFitViewMargin(const float viewMargin)
{
    m_fitViewMargin = viewMargin;
}
//---------------------------------------------------------------------------------------

