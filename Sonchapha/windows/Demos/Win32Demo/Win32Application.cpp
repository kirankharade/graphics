#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <WinUser.h>
#include <WTypes.h>

#include "Win32Application.h"
#include "Graphics/Scene/C3DScene.h"
#include "Graphics/Scene/C3DGeometrySceneNode.h"
#include "Graphics/IViewport.h"
#include "Graphics/CLayer3D.h"
#include "Graphics/Scene/C3DMesh.h"
#include "Graphics/FileUtils/CIOSTL.h"
#include "Graphics/CBackGround.h"
#include "Graphics/CColor.h"
#include "Graphics/CCamera.h"

using namespace An::Graphics;
using namespace An::Maths;

static C3DScene* scene = nullptr;

//------------------------------------------------------------

void Win32Application()
{
    MSG Msg;
    while( GetMessage(&Msg, NULL, 0, 0) )
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }
}
//------------------------------------------------------------

void CreateScene(IViewport* vp)
{
    scene = C3DScene::Create();
    vp->Layer3D()->SetScene(scene);

    //C3DGeometrySceneNode* node = new C3DGeometrySceneNode(nullptr, scene);
    
    //std::string file = "D:/cylinder_mesh.stl";
    //std::string file = "D:/Anant/data/stl/ship_ascii.stl";
    //std::string file = "D:/Anant/data/stl/magnolia.stl";
    //C3DMesh* mesh = CIOSTL::Read(file);

    //float a = 25.0f;

    //CPoint3f* v = new CPoint3f[3];
    //v[0] = CPoint3f(a, a, 0);
    //v[1] = CPoint3f(2.0f*a, a, 0);
    //v[2] = CPoint3f(2.0f*a, 2.0f*a, 0);

    //CVector3f* n = new CVector3f[3];
    //n[0] = CVector3f(0, 0, 1);
    //n[1] = CVector3f(0, 0, 1);
    //n[2] = CVector3f(0, 0, 1);

    //int* indices = new int[3];
    //indices[0] = 0;
    //indices[1] = 1;
    //indices[2] = 2;

    //CColor* colors = new CColor[3];
    //colors[0] = CColor(EPredefinedColor::Brown);
    //colors[1] = CColor(EPredefinedColor::Red);
    //colors[2] = CColor(EPredefinedColor::DarkMagenta);
    //colors[0].SetAlpha(1);
    //colors[1].SetAlpha(1);
    //colors[2].SetAlpha(1);

    //C3DMesh* mesh2 = new C3DMesh(v, 3, n, 3, colors, 3, nullptr, 0, indices, 3);

    //mesh2->SetUseVertexColors(true);

    //node->AddDrawable(mesh);
    //node->AddDrawable(mesh2);

    //scene->BaseNode()->AddChild(node);

    vp->Layer3D()->InitializeForScene();
}
//------------------------------------------------------------

void SetBackground(IViewport* vp)
{
    if(vp)
    {
        CBackGround* bg = vp->BackGround();
        if(bg)
        {
            bg->SetType(EBackGroundType_Colored);

            bg->SetColoringType(EBackGroundColorType_FourCornersColorsGradient);

            bg->SetColors(CColor(EPredefinedColor::Black), 
                CColor(EPredefinedColor::Yellow), 
                CColor(EPredefinedColor::ForestGreen), 
                CColor(EPredefinedColor::DarkBlue), 
                CColor(EPredefinedColor::Orange));
        }
    }
}
//------------------------------------------------------------
