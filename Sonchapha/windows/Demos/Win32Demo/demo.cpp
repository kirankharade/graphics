// demo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Graphics/GraphicsIncludes.h"
#include "Graphics/IFactory.h"
#include "Graphics/IGraphics.h"
#include "Graphics/IWindow.h"
#include "Graphics/SGLContextParams.h"
#include "Maths/MathsIncludes.h"
#include "Maths/CPoint3f.h"
#include "Graphics/CViewport.h"
#include "Win32Application.h"
#include <iostream>
#include <windows.h>
#include <WinUser.h>
#include <WTypes.h>

using namespace An;
using namespace An::Graphics;
using namespace An::Maths;
using namespace std;

//------------------------------------------------------------


int _tmain(int argc, _TCHAR* argv[])
{
    IGraphics::InitializeGraphics(EWindowingSystemType_Win32, EOpenGLEngineType_FixedFunction);

    IFactory* factory = IGraphics::Factory();
    if(factory)
    {
        SGLContextParams params = SGLContextParams();
        params.m_openGLEngineType = EOpenGLEngineType_FixedFunction;

        IWindow* window = factory->Window(params, 800, 700);
        IViewport* vp = CViewport::New(window);
        if(window)
        {
            IViewport* vp = window->Viewport();
            if(vp)
            {
                SetBackground(vp);
                CreateScene(vp);
            }
            window->SetVisible(true);
        }
    }

    Win32Application();

    return 0;
}
//------------------------------------------------------------

