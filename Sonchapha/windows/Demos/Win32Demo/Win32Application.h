#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <WinUser.h>
#include <WTypes.h>

#include "Graphics/IViewport.h"

using namespace An::Graphics;
using namespace An::Maths;

void Win32Application();
void CreateScene(IViewport* vp);
void SetBackground(IViewport* vp);


