
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

public ref class Material
{

public:

    Material(void* unmanagedPtr);

    ~Material(void);

    !Material(void);

public:

    Material(System::Drawing::Color ambient, 
		System::Drawing::Color diffuse, 
		System::Drawing::Color emissive, 
		System::Drawing::Color specular, 
		float shininess);

    Material(System::Drawing::Color diffuse);

    Material(Material^ material);

    virtual property System::Drawing::Color Ambient 
    {
        System::Drawing::Color get(void);
        void set(System::Drawing::Color);
    }

    virtual property System::Drawing::Color Diffuse 
    {
        System::Drawing::Color get(void);
        void set(System::Drawing::Color);
    }

    virtual property System::Drawing::Color Specular 
    {
        System::Drawing::Color get(void);
        void set(System::Drawing::Color);
    }

    virtual property System::Drawing::Color Emissive 
    {
        System::Drawing::Color get(void);
        void set(System::Drawing::Color);
    }

    virtual property float Shininess 
    {
        float get(void);
        void set(float);
    }

    Material^ operator = (Material^ material);

    static bool operator == (Material^ left, Material^ right);

    static bool operator != (Material^ left, Material^ right);

   void* UnmanagedPtr();

protected:

    void*  m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
