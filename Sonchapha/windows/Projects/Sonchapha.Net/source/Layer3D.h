
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;
ref class Viewport;

public ref class Layer3D
{

public:

    Layer3D(void* unmanagedPtr, Viewport^ parentViewport);

    ~Layer3D(void);

    !Layer3D(void);

public:

   property Viewport^ View
   {
      Viewport^ get(void);
   }

    void FitToScene();

    void InitializeForScene();

    void* UnmanagedPtr();

protected:

    Viewport^                           m_viewport;
    void*                               m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
