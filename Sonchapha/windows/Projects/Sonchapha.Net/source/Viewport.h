
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

ref class Layer2D;
ref class Layer3D;
ref class BackGround;
ref class Window;

public ref class Viewport
{

public:

    Viewport(void* unmanagedPtr, Window^ parentWindow);

    Viewport();

    ~Viewport();

    !Viewport();

public:

   property Window^ ParentWindow
   {
      Window^ get(void);
   }

   property Layer2D^ Foreground2DLayer
   {
      Layer2D^ get(void);
   }

   property Layer2D^ Background2DLayer
   {
      Layer2D^ get(void);
   }

   property Layer3D^ RenderLayer3D
   {
      Layer3D^ get(void);
   }

   property BackGround^ RenderBackGround
   {
      BackGround^ get(void);
      void set(BackGround^ background);
   }

   //property LightManager^ LightManager
   //{
   //   LightManager^ get(void);
   //   void set(LightManager^ background);
   //}

   property bool Visible
   {
      bool get(void);
      void set(bool visible);
   }

   //property Point3f^ CurrentRotationCentre
   //{
   //   Point3f^ get(void);
   //   void set(Point3f^ crc);
   //}

    void ResetCurrentRotationCentre();

    void FitView();

    void FitView(const bool bUpdate);

    void ResetView();

    void Update();

   void* UnmanagedPtr();

protected:

    void* m_pUnmanagedPtr;
    Window^ m_parentWindow;
    Layer2D^ m_background2DLayer;
    Layer2D^ m_foreground2DLayer;
    Layer3D^ m_3DLayer;
    BackGround^ m_background;
};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
