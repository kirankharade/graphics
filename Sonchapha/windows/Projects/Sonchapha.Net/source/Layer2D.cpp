#include "Gfx.h"
#include "Graphics/GraphicsIncludes.h"
#include "Graphics/IGraphics.h"
#include "Graphics/CLayer2D.h"
#include "Window.h"
#include "Viewport.h"
#include "Layer2D.h"

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Layer2D::Layer2D(void* unmanagedPtr, Viewport^ parentViewport)
    : m_viewport(parentViewport)
    , m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in layer generation.");
    }
}
//---------------------------------------------------------------------------------------

Layer2D::~Layer2D()
{
    this->!Layer2D();
}
//---------------------------------------------------------------------------------------

Layer2D::!Layer2D()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

Viewport^ Layer2D::View::get()
{
    return m_viewport;
}
//---------------------------------------------------------------------------------------

void* Layer2D::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------
