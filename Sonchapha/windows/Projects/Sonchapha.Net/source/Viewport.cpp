#include "Graphics/IViewport.h"
#include "Graphics/CBackGround.h"
#include "Graphics/CLayer3D.h"
#include "Graphics/CLayer2D.h"
#include "Viewport.h"
#include "Window.h"
#include "Layer2D.h"
#include "Layer3D.h"
#include "BackGround.h"

using namespace Sonchapha::NET;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Viewport::Viewport(void* unmanagedPtr, Window^ parentWindow)
    : m_pUnmanagedPtr(unmanagedPtr)
    , m_parentWindow(parentWindow)
    , m_background2DLayer(nullptr)
    , m_foreground2DLayer(nullptr)
    , m_3DLayer(nullptr)
{
    if(m_pUnmanagedPtr == nullptr || m_parentWindow == nullptr)
    {
        throw gcnew System::Exception("Unable to initialize viewport.");
    }
}
//---------------------------------------------------------------------------------------

Viewport::Viewport()
    : m_pUnmanagedPtr(nullptr)
    , m_parentWindow(nullptr)
{
}
//---------------------------------------------------------------------------------------

Viewport::~Viewport()
{
    this->!Viewport();
}
//---------------------------------------------------------------------------------------

Viewport::!Viewport()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

Window^ Viewport::ParentWindow::get()
{
    return m_parentWindow;
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::Layer2D^ Viewport::Background2DLayer::get()
{
    if(m_background2DLayer == nullptr)
    {
        m_background2DLayer = gcnew Layer2D(((IViewport*) m_pUnmanagedPtr)->LayerBackground2D(), this);
    }
    return m_background2DLayer;
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::Layer2D^ Viewport::Foreground2DLayer::get()
{
    if(m_foreground2DLayer == nullptr)
    {
        m_foreground2DLayer = gcnew Layer2D(((IViewport*) m_pUnmanagedPtr)->LayerForeground2D(), this);
    }
    return m_foreground2DLayer;
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::Layer3D^ Viewport::RenderLayer3D::get()
{
    if(m_3DLayer == nullptr)
    {
        m_3DLayer = gcnew Sonchapha::NET::Layer3D(((IViewport*) m_pUnmanagedPtr)->Layer3D(), this);
    }
    return m_3DLayer;
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::BackGround^ Viewport::RenderBackGround::get()
{
    if(m_background == nullptr)
    {
        m_background = gcnew Sonchapha::NET::BackGround(((IViewport*) m_pUnmanagedPtr)->BackGround(), this);
    }
    return m_background;
}
//---------------------------------------------------------------------------------------

void Viewport::RenderBackGround::set(Sonchapha::NET::BackGround^ bg)
{
    if(bg == nullptr)
    {
        return;
    }
    ((IViewport*) m_pUnmanagedPtr)->SetBackGround((CBackGround*) bg->UnmanagedPtr());
}
//---------------------------------------------------------------------------------------

bool Viewport::Visible::get()
{
    return ((IViewport*) m_pUnmanagedPtr)->Visible();
}
//---------------------------------------------------------------------------------------

void Viewport::Visible::set(bool visible)
{
    ((IViewport*) m_pUnmanagedPtr)->SetVisible(visible);
}
//---------------------------------------------------------------------------------------

void* Viewport::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

void Viewport::ResetCurrentRotationCentre()
{
    ((IViewport*) m_pUnmanagedPtr)->ResetCurrentRotationCentre();
}
//---------------------------------------------------------------------------------------

void Viewport::FitView()
{
    ((IViewport*) m_pUnmanagedPtr)->FitView();
}
//---------------------------------------------------------------------------------------

void Viewport::FitView(const bool bUpdate)
{
    ((IViewport*) m_pUnmanagedPtr)->FitView(bUpdate);
}
//---------------------------------------------------------------------------------------

void Viewport::ResetView()
{
    ((IViewport*) m_pUnmanagedPtr)->ResetView();
}
//---------------------------------------------------------------------------------------

void Viewport::Update()
{
    ((IViewport*) m_pUnmanagedPtr)->Invalidate();
}
//---------------------------------------------------------------------------------------
