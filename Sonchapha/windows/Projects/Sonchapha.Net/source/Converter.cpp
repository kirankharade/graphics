#include "Converter.h"
#include <iostream>
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>

using namespace Sonchapha::NET;
using namespace System::Windows::Forms;
using namespace System::Runtime::InteropServices;
using namespace System::Drawing;
using namespace std;

//---------------------------------------------------------------------------------------

System::String^ Sonchapha::NET::Converter::Convert(const std::string& str)
{
	auto s = str.c_str();
	if(s == nullptr)
	{
		return gcnew System::String("");
	}
	return gcnew System::String(s);
}
//---------------------------------------------------------------------------------------

std::string Sonchapha::NET::Converter::Convert(System::String^ str)
{
	std::string unmanagedString = msclr::interop::marshal_as<std::string>(str);
	return unmanagedString;
}
//---------------------------------------------------------------------------------------

System::Drawing::Color Sonchapha::NET::Converter::Convert(const An::Graphics::CColor& color)
{
	auto a = (int) color.AlphaAsByte();
	auto r = (int) color.RedAsByte();
	auto g = (int) color.GreenAsByte();
	auto b = (int) color.BlueAsByte();

   return System::Drawing::Color::FromArgb(a, r, g, b);
}
//---------------------------------------------------------------------------------------

An::Graphics::CColor Sonchapha::NET::Converter::Convert(System::Drawing::Color color)
{
	return An::Graphics::CColor(color.ToArgb());
}
//---------------------------------------------------------------------------------------
