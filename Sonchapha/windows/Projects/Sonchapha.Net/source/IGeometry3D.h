//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "IDrawable.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

public interface class IGeometry3D: IDrawable
{

   //property AxisOrientedBox^ Bounds 
   //{
   //    AxisOrientedBox^ get(void);
   //}

   property bool UseVertexColors 
   {
       bool get(void);
	   void set(bool);
   }

};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
