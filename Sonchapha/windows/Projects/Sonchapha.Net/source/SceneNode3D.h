//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "ISceneNode.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;

interface class IScene;

public ref class SceneNode3D : ISceneNode
{

public:

    SceneNode3D(void* unmanagedPtr);

    ~SceneNode3D(void);

    !SceneNode3D(void);

public:

    virtual IScene^ Scene();

    virtual ISceneNode^ Parent();

    virtual bool AddChild(ISceneNode^ pChildNode);

    virtual bool DetachChild(ISceneNode^ pChildNode);

    virtual bool DetachChild(System::String^ nodeName);

    virtual bool DetachChild(System::Int64 nodeId);

    virtual void RemoveFromScene();

    virtual bool IsChildNode(System::String^ name);

    virtual bool IsChildNode(System::Int64 id);

    virtual ISceneNode^ Child(System::String^ name);

    virtual ISceneNode^ Child(System::Int64 id);

    virtual bool IsDescendentNode(System::String^ name);

    virtual bool IsDescendentNode(System::Int64 id);

    virtual System::Int32 ChildrenCount();

    virtual System::Collections::Generic::List<ISceneNode^>^ Children();

    virtual property System::Int64 ID 
    {
        System::Int64 get(void);
    }

    virtual property bool Visible 
    {
        bool get(void);
        void set(bool);
    }

    virtual property System::String^ Name 
    {
        System::String^ get(void);
        void set(System::String^);
    }

    virtual property Material^ NodeMaterial 
    {
        Material^ get(void);
        void set(Material^);
    }

    virtual void* UnmanagedPtr();

protected:

    void*  m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
