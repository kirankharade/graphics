#include "Gfx.h"
#include "Graphics/GraphicsIncludes.h"
#include "Graphics/IGraphics.h"
#include "Graphics/CLayer2D.h"
#include "Graphics/Scene/IScene.h"
#include "Graphics/Scene/ISceneNode.h"
#include "Graphics/Scene/C3DScene.h"
#include "Graphics/Scene/C3DSceneNode.h"
#include "Graphics/Scene/CSceneNodeCollection.h"
#include "Window.h"
#include "Viewport.h"
#include "Layer2D.h"
#include "Scene3D.h"
#include "IScene.h"
#include "ISceneNode.h"
#include "SceneNode3D.h"
#include "Material.h"
#include "Converter.h"
//---------------------------------------------------------------------------------------

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

SceneNode3D::SceneNode3D(void* unmanagedPtr)
: m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in layer generation.");
    }
}
//---------------------------------------------------------------------------------------

SceneNode3D::~SceneNode3D()
{
    this->!SceneNode3D();
}
//---------------------------------------------------------------------------------------

SceneNode3D::!SceneNode3D()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

void* SceneNode3D::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

System::String^ SceneNode3D::Name::get(void)
{
	std::string name = ((An::Graphics::IScene*) m_pUnmanagedPtr)->Name();
	return Converter::Convert(name);
}
//---------------------------------------------------------------------------------------

void SceneNode3D::Name::set(System::String^ name)
{
	std::string n = Converter::Convert(name);
	((An::Graphics::IScene*) m_pUnmanagedPtr)->SetName(n);
}
//---------------------------------------------------------------------------------------

System::Int64 SceneNode3D::ID::get(void)
{
	return (System::Int64) ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Id();
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::Visible::get(void)
{
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Visible();
}
//---------------------------------------------------------------------------------------

void SceneNode3D::Visible::set(bool visible)
{
	((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->SetVisible(visible);
}
//---------------------------------------------------------------------------------------

Material^ SceneNode3D::NodeMaterial::get(void)
{
	return gcnew Material(((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Material());
}
//---------------------------------------------------------------------------------------

void SceneNode3D::NodeMaterial::set(Material^ material)
{
	CMaterial* mat = (CMaterial*) material->UnmanagedPtr();
	((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->SetMaterial(mat);
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::IScene^ SceneNode3D::Scene()
{
    return gcnew Scene3D(((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Scene());
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ SceneNode3D::Parent()
{
    return gcnew SceneNode3D(((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Parent());
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::AddChild(Sonchapha::NET::ISceneNode^ childNode)
{
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->AddChild(((An::Graphics::ISceneNode*) childNode->UnmanagedPtr()));
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::DetachChild(Sonchapha::NET::ISceneNode^ childNode)
{
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->DetachChild(((An::Graphics::ISceneNode*) childNode->UnmanagedPtr()));
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::DetachChild(System::String^ nodeName)
{
	std::string name = Converter::Convert(nodeName);
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->DetachChild(name);
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::DetachChild(System::Int64 nodeId)
{
	An::AInt64 id = (An::AInt64) nodeId;
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->DetachChild(id);
}
//---------------------------------------------------------------------------------------

void SceneNode3D::RemoveFromScene()
{
	((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->RemoveFromScene();
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::IsChildNode(System::String^ nodeName)
{
	std::string name = Converter::Convert(nodeName);
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->IsChildNode(name);
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::IsChildNode(System::Int64 nodeId)
{
	An::AInt64 id = (An::AInt64) nodeId;
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->IsChildNode(id);
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ SceneNode3D::Child(System::String^ nodeName)
{
	std::string name = Converter::Convert(nodeName);
	return gcnew SceneNode3D(((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Child(name));
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ SceneNode3D::Child(System::Int64 nodeId)
{
	An::AInt64 id = (An::AInt64) nodeId;
	return gcnew SceneNode3D(((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Child(id));
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::IsDescendentNode(System::String^ nodeName)
{
	std::string name = Converter::Convert(nodeName);
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->IsDescendentNode(name);
}
//---------------------------------------------------------------------------------------

bool SceneNode3D::IsDescendentNode(System::Int64 nodeId)
{
	An::AInt64 id = (An::AInt64) nodeId;
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->IsDescendentNode(id);
}
//---------------------------------------------------------------------------------------

System::Int32 SceneNode3D::ChildrenCount()
{
	return ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->ChildrenCount();
}
//---------------------------------------------------------------------------------------

System::Collections::Generic::List<Sonchapha::NET::ISceneNode^>^ SceneNode3D::Children()
{
	auto list = gcnew System::Collections::Generic::List<Sonchapha::NET::ISceneNode^>();
	CSceneNodeCollection* nodes = ((An::Graphics::ISceneNode*) m_pUnmanagedPtr)->Children();
	for(unsigned int i = 0; i < nodes->Count(); i++)
	{
		An::Graphics::ISceneNode* node = (*nodes)[i];
		list->Add(gcnew SceneNode3D(node));
	}
	return list;
}
//---------------------------------------------------------------------------------------

