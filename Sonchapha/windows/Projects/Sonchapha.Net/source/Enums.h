//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "Graphics/GraphicsConfigurations.h"
#include "Graphics/GraphicsEnums.h"

using namespace An::Graphics;
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

public enum class OpenGLEngineType
{
    FixedFunction = EOpenGLEngineType_FixedFunction,
    ES = An::Graphics::EOpenGLEngineType_ES,
    Core3 = An::Graphics::EOpenGLEngineType_Core_3,
    Core4 = An::Graphics::EOpenGLEngineType_Core_4
};
//---------------------------------------------------------------------------------------

public enum class BackGroundType
{
    Ddefault = An::Graphics::EBackGroundType_Default,
    Colored = An::Graphics::EBackGroundType_Colored,
    Textured = An::Graphics::EBackGroundType_Textured,
    TextureBlendedWithColor = An::Graphics::EBackGroundType_TextureBlendedWithColor
};
//---------------------------------------------------------------------------------------

public enum class BackGroundTextureType
{
    Centred = An::Graphics::EBackGroundTextureType_Centred,
    Tiled = An::Graphics::EBackGroundTextureType_Tiled,
    Scaled = An::Graphics::EBackGroundTextureType_Scaled
};
//---------------------------------------------------------------------------------------

public enum class BackGroundColorType
{
    DefaultColor = An::Graphics::EBackGroundColorType_DefaultColor,
    SingleColor = An::Graphics::EBackGroundColorType_SingleColor,
    TwoColorsVerticalGradient = An::Graphics::EBackGroundColorType_TwoColorsVerticalGradient,
    TwoColorsHorizontalGradient = An::Graphics::EBackGroundColorType_TwoColorsHorizontalGradient,
    ThreeColorsVerticalGradient = An::Graphics::EBackGroundColorType_ThreeColorsVerticalGradient,
    ThreeColorsHorizontalGradient = An::Graphics::EBackGroundColorType_ThreeColorsHorizontalGradient,
    FourCornersColorsGradient = An::Graphics::EBackGroundColorType_FourCornersColorsGradient,
    FiveColorsCornersAndCentreGradient = An::Graphics::EBackGroundColorType_FiveColorsCornersAndCentreGradient
};
//---------------------------------------------------------------------------------------

public enum class LightType
{
    SpotDirectionalLight = An::Graphics::ELightType_SpotDirectionalLight,
    PointLight = An::Graphics::ELightType_PointLight,
    GeneralDirectionalLight = An::Graphics::ELightType_GeneralDirectionalLight
};
//---------------------------------------------------------------------------------------

public enum class FaceType
{
    Front = An::Graphics::EFaceType_Front,
    Back = An::Graphics::EFaceType_Back,
    Both = An::Graphics::EFaceType_Both,
	None = An::Graphics::EFaceType_None
};
//---------------------------------------------------------------------------------------

public enum class FrontFaceOrientation
{
    CounterClockWise = An::Graphics::EFrontFaceOrientation_CounterClockWise,
    ClockWise = An::Graphics::EFrontFaceOrientation_ClockWise,
    None = An::Graphics::EFrontFaceOrientation_None
};
//---------------------------------------------------------------------------------------

public enum class DrawableType
{
    Unknown = An::Graphics::EDrawableType_Unknown,
    Mesh3D = An::Graphics::EDrawableType_Mesh3D,
    Mesh2D = An::Graphics::EDrawableType_Mesh2D,
    Text3D = An::Graphics::EDrawableType_Text3D,
    Text2D = An::Graphics::EDrawableType_Text2D,
    Scene3D = An::Graphics::EDrawableType_Scene3D,
    Scene2D = An::Graphics::EDrawableType_Scene2D
};
//---------------------------------------------------------------------------------------

public enum class SceneNodeType
{
    Unknown = An::Graphics::ESceneNodeType_Unknown,
    Solid = An::Graphics::ESceneNodeType_Solid,
    Transparent = An::Graphics::ESceneNodeType_Transparent,
    Text = An::Graphics::ESceneNodeType_Text
};
//---------------------------------------------------------------------------------------

public enum class TransformationType
{
    CameraView = An::Graphics::ETransformationType_CameraView,
    Projection = An::Graphics::ETransformationType_Projection,
    ModelWorld = An::Graphics::ETransformationType_ModelWorld
};
//---------------------------------------------------------------------------------------

public enum class PresetViewType
{
   Left = An::Graphics::EPresetView_Left,
   Right = An::Graphics::EPresetView_Right,
   Top = An::Graphics::EPresetView_Top,
   Bottom = An::Graphics::EPresetView_Bottom,
   Front = An::Graphics::EPresetView_Front,
   Back = An::Graphics::EPresetView_Back,
   Isometric = An::Graphics::EPresetView_Isometric,
   Undefined = An::Graphics::EPresetView_Undefined
};
//---------------------------------------------------------------------------------------

public enum class TextureType
{
    OneDimensional = An::Graphics::ETexture_1D,
    TwoDimensional = An::Graphics::ETexture_2D,
    ThreeDimensional = An::Graphics::ETexture_3D
};
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
