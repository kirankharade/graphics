#include "Gfx.h"
#include "Graphics/GraphicsIncludes.h"
#include "Graphics/IGraphics.h"
#include "Graphics/CLayer2D.h"
#include "Graphics/Scene/IScene.h"
#include "Graphics/Scene/ISceneNode.h"
#include "Graphics/Scene/C3DScene.h"
#include "Graphics/Scene/C3DSceneNode.h"
#include "Window.h"
#include "Viewport.h"
#include "Layer2D.h"
#include "Scene3D.h"
#include "IScene.h"
#include "ISceneNode.h"
#include "SceneNode3D.h"
#include "Converter.h"
//---------------------------------------------------------------------------------------

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Scene3D::Scene3D(void* unmanagedPtr)
: m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in layer generation.");
    }
}
//---------------------------------------------------------------------------------------

Scene3D::~Scene3D()
{
    this->!Scene3D();
}
//---------------------------------------------------------------------------------------

Scene3D::!Scene3D()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

void* Scene3D::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ Scene3D::BaseNode()
{
    return gcnew SceneNode3D(((An::Graphics::IScene*) m_pUnmanagedPtr)->BaseNode());
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ Scene3D::SearchInScene(System::String^ name)
{
	std::string n = Converter::Convert(name);
    return gcnew SceneNode3D(((An::Graphics::IScene*) m_pUnmanagedPtr)->SearchInScene(n));
}
//---------------------------------------------------------------------------------------

Sonchapha::NET::ISceneNode^ Scene3D::SearchInScene(System::Int64 id)
{
	An::AInt64 i = (An::AInt64) id;
    return gcnew SceneNode3D(((An::Graphics::IScene*) m_pUnmanagedPtr)->SearchInScene(i));
}
//---------------------------------------------------------------------------------------

bool Scene3D::ExistsInScene(System::String^ nodeName)
{
	std::string n = Converter::Convert(nodeName);
	return ((An::Graphics::IScene*) m_pUnmanagedPtr)->ExistsInScene(n);
}
//---------------------------------------------------------------------------------------

bool Scene3D::ExistsInScene(System::Int64 nodeId)
{
	An::AInt64 i = (An::AInt64) nodeId;
	return ((An::Graphics::IScene*) m_pUnmanagedPtr)->ExistsInScene(i);
}
//---------------------------------------------------------------------------------------

System::Int64 Scene3D::ID::get(void)
{
	return (System::Int64) ((An::Graphics::IScene*) m_pUnmanagedPtr)->Id();
}
//---------------------------------------------------------------------------------------

bool Scene3D::Dirty::get(void)
{
	return ((An::Graphics::IScene*) m_pUnmanagedPtr)->IsDirty();
}
//---------------------------------------------------------------------------------------

void Scene3D::Dirty::set(bool dirty)
{
	if(dirty)
	{
		((An::Graphics::IScene*) m_pUnmanagedPtr)->SetDirty();
	}
}
//---------------------------------------------------------------------------------------

System::String^ Scene3D::Name::get(void)
{
	std::string name = ((An::Graphics::IScene*) m_pUnmanagedPtr)->Name();
	return Converter::Convert(name);
}
//---------------------------------------------------------------------------------------

void Scene3D::Name::set(System::String^ name)
{
	std::string n = Converter::Convert(name);
	((An::Graphics::IScene*) m_pUnmanagedPtr)->SetName(n);
}
//---------------------------------------------------------------------------------------

