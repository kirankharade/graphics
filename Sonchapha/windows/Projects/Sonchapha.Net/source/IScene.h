//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;
ref class Viewport;
interface class ISceneNode;

public interface class IScene : System::IDisposable
{

    ISceneNode^ BaseNode();

    ISceneNode^ SearchInScene(System::String^ name);

    ISceneNode^ SearchInScene(System::Int64 id);

    bool ExistsInScene(System::String^ nodeName);

    bool ExistsInScene(System::Int64 nodeId);

   property System::Int64 ID 
   {
       System::Int64 get(void);
   }

   property bool Dirty 
   {
       bool get(void);
       void set(bool);
   }

   property System::String^ Name 
   {
       System::String^ get(void);
       void set(System::String^);
   }

};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
