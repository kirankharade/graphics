
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "Enums.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;
ref class Viewport;

public ref class BackGround
{

public:

    BackGround(void* unmanagedPtr, Viewport^ parentViewport);

    ~BackGround(void);

    !BackGround(void);

public:

   property Viewport^ View
   {
      Viewport^ get(void);
   }

    void SetColors(System::Drawing::Color color);

    void SetColors(System::Drawing::Color color1,
                   System::Drawing::Color color2);

    void SetColors(System::Drawing::Color color1,
                   System::Drawing::Color color2,
                   System::Drawing::Color color3);

    void SetColors(System::Drawing::Color color1,
                   System::Drawing::Color color2,
                   System::Drawing::Color color3,
                   System::Drawing::Color color4);

    void SetColors(System::Drawing::Color color1, //bottomLeftColor
                   System::Drawing::Color color2, //bottomRightColor
                   System::Drawing::Color color3, //topRightColor
                   System::Drawing::Color color4, //topLeftColor 
                   System::Drawing::Color centreColor);

    //void SetTexture(Texture^ texture);

    virtual property BackGroundType Type 
    {
        BackGroundType get(void);
        void set(BackGroundType);
    }

    virtual property BackGroundColorType ColorType 
    {
        BackGroundColorType get(void);
        void set(BackGroundColorType);
    }

    virtual property BackGroundTextureType TextureType 
    {
        BackGroundTextureType get(void);
        void set(BackGroundTextureType);
    }

	System::Collections::Generic::List<System::Drawing::Color>^ Colors();

	void* UnmanagedPtr();

protected:

    Viewport^                           m_viewport;
    void*                               m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
