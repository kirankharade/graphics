#include <Graphics/CMaterial.h>
#include <Graphics/CColor.h>
#include "Material.h"
#include "Converter.h"

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Material::Material(void* unmanagedPtr)
    : m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in material creation.");
    }
}
//---------------------------------------------------------------------------------------

Material::~Material()
{
    this->!Material();
}
//---------------------------------------------------------------------------------------

Material::!Material()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

void* Material::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

Material::Material(System::Drawing::Color ambient, 
	System::Drawing::Color diffuse, 
	System::Drawing::Color emissive, 
	System::Drawing::Color specular, 
	float shininess)
    : m_pUnmanagedPtr(nullptr)
{
	auto a = Converter::Convert(ambient);
	auto d = Converter::Convert(diffuse);
	auto e = Converter::Convert(emissive);
	auto s = Converter::Convert(specular);

	m_pUnmanagedPtr = new CMaterial(a, d, e, s, shininess);
}
//---------------------------------------------------------------------------------------

Material::Material(System::Drawing::Color diffuse)
    : m_pUnmanagedPtr(nullptr)
{
	auto d = Converter::Convert(diffuse);
	m_pUnmanagedPtr = new CMaterial(d);
}
//---------------------------------------------------------------------------------------

Material::Material(Material^ material)
    : m_pUnmanagedPtr(nullptr)
{
	auto a = Converter::Convert(material->Ambient);
	auto d = Converter::Convert(material->Diffuse);
	auto e = Converter::Convert(material->Emissive);
	auto s = Converter::Convert(material->Specular);

	m_pUnmanagedPtr = new CMaterial(a, d, e, s, material->Shininess);
}
//---------------------------------------------------------------------------------------

System::Drawing::Color Material::Ambient::get(void)
{
	auto c = ((An::Graphics::CMaterial*) m_pUnmanagedPtr)->Ambient();
	return Converter::Convert(c);
}
//---------------------------------------------------------------------------------------

void Material::Ambient::set(System::Drawing::Color color)
{
	auto c = Converter::Convert(color);
	((An::Graphics::CMaterial*) m_pUnmanagedPtr)->SetAmbient(c);
}
//---------------------------------------------------------------------------------------

System::Drawing::Color Material::Diffuse::get(void)
{
	auto c = ((An::Graphics::CMaterial*) m_pUnmanagedPtr)->Diffuse();
	return Converter::Convert(c);
}
//---------------------------------------------------------------------------------------

void Material::Diffuse::set(System::Drawing::Color color)
{
	auto c = Converter::Convert(color);
	((An::Graphics::CMaterial*) m_pUnmanagedPtr)->SetDiffuse(c);
}
//---------------------------------------------------------------------------------------

System::Drawing::Color Material::Specular::get(void)
{
	auto c = ((An::Graphics::CMaterial*) m_pUnmanagedPtr)->Specular();
	return Converter::Convert(c);
}
//---------------------------------------------------------------------------------------

void Material::Specular::set(System::Drawing::Color color)
{
	auto c = Converter::Convert(color);
	((An::Graphics::CMaterial*) m_pUnmanagedPtr)->SetSpecular(c);
}
//---------------------------------------------------------------------------------------

System::Drawing::Color Material::Emissive::get(void)
{
	auto c = ((An::Graphics::CMaterial*) m_pUnmanagedPtr)->Emissive();
	return Converter::Convert(c);
}
//---------------------------------------------------------------------------------------

void Material::Emissive::set(System::Drawing::Color color)
{
	auto c = Converter::Convert(color);
	((An::Graphics::CMaterial*) m_pUnmanagedPtr)->SetEmissive(c);
}
//---------------------------------------------------------------------------------------

float Material::Shininess::get(void)
{
	return ((An::Graphics::CMaterial*) m_pUnmanagedPtr)->Shininess();
}
//---------------------------------------------------------------------------------------

void Material::Shininess::set(float shininess)
{
	((An::Graphics::CMaterial*) m_pUnmanagedPtr)->SetShininess(shininess);
}
//---------------------------------------------------------------------------------------

Material^ Material::operator = (Material^ material)
{
   try
   {
	   CMaterial* mat = (CMaterial*) material->UnmanagedPtr();

	   if(mat == nullptr)
	   {
		   throw gcnew System::Exception("Error occurred in material assignment.");
	   }
	   if(m_pUnmanagedPtr == nullptr)
	   {
		   m_pUnmanagedPtr = new CMaterial();
	   }

	   ((CMaterial*) m_pUnmanagedPtr)->SetAmbient(mat->Ambient());
	   ((CMaterial*) m_pUnmanagedPtr)->SetDiffuse(mat->Diffuse());
	   ((CMaterial*) m_pUnmanagedPtr)->SetEmissive(mat->Emissive());
	   ((CMaterial*) m_pUnmanagedPtr)->SetSpecular(mat->Specular());
	   ((CMaterial*) m_pUnmanagedPtr)->SetShininess(mat->Shininess());

      return this;
   }
   catch(...)
   {
		throw gcnew System::Exception("Error occurred in material assignment.");
   }
}
//---------------------------------------------------------------------------------------

bool Material::operator == (Material^ left, Material^ right)
{
   try
   {
      if(Object::ReferenceEquals(left, nullptr) && 
		 Object::ReferenceEquals(right, nullptr))
      {
         return false;
      }
      else if(Object::ReferenceEquals(left, nullptr) || 
		  Object::ReferenceEquals(right, nullptr))
      {
		  return false;
      }
      else
      {
		  CMaterial* l = (CMaterial*) left->UnmanagedPtr();
		  CMaterial* r = (CMaterial*) right->UnmanagedPtr();
		  return (*l == *r);
      }
   }
   catch(...)
   {
      throw gcnew System::Exception("Error in == operator for Material.");
   }
}
//---------------------------------------------------------------------------------------

bool Material::operator != (Material^ left, Material^ right)
{
   try
   {
      if(Object::ReferenceEquals(left, nullptr) && 
		 Object::ReferenceEquals(right, nullptr))
      {
         return false;
      }
      else if(Object::ReferenceEquals(left, nullptr) || 
		  Object::ReferenceEquals(right, nullptr))
      {
         return true;
      }
      else
      {
		  CMaterial* l = (CMaterial*) left->UnmanagedPtr();
		  CMaterial* r = (CMaterial*) right->UnmanagedPtr();
		  return (*l != *r);
      }
   }
   catch(...)
   {
      throw gcnew System::Exception("Error in != operator for Material.");
   }
}
//---------------------------------------------------------------------------------------

