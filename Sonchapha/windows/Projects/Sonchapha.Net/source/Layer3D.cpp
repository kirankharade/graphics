#include "Window.h"
#include "Viewport.h"
#include "Layer3D.h"
#include <Graphics/CLayer3D.h>

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Layer3D::Layer3D(void* unmanagedPtr, Viewport^ parentViewport)
    : m_viewport(parentViewport)
    , m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in layer generation.");
    }
}
//---------------------------------------------------------------------------------------

Layer3D::~Layer3D()
{
    this->!Layer3D();
}
//---------------------------------------------------------------------------------------

Layer3D::!Layer3D()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

Viewport^ Layer3D::View::get()
{
    return m_viewport;
}
//---------------------------------------------------------------------------------------

void Layer3D::FitToScene()
{
    ((CLayer3D*) m_pUnmanagedPtr)->FitToScene();
}
//---------------------------------------------------------------------------------------

void Layer3D::InitializeForScene()
{
    ((CLayer3D*) m_pUnmanagedPtr)->InitializeForScene();
}
//---------------------------------------------------------------------------------------

void* Layer3D::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------
