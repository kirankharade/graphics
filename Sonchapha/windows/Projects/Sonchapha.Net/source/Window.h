
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;
ref class Viewport;

public ref class Window
{

public:

    Window(System::Windows::Forms::Control^ control);

    ~Window(void);

    !Window(void);

public:

   property Viewport^ View
   {
      Viewport^ get(void);
   }

   property System::Windows::Forms::Control^ Control
   {
      System::Windows::Forms::Control^ get(void);
   }

   property int Height
   {
      int get(void);
      void set(int h);
   }

   property int Width
   {
      int get(void);
      void set(int w);
   }

   void SetSize(const int width, const int height);

   void* UnmanagedPtr();

protected:

    System::Windows::Forms::Control^    m_control;
    Viewport^                           m_viewport;
    void*                               m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
