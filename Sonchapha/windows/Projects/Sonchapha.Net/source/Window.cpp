#include "Gfx.h"
#include "Graphics/IFactory.h"
#include "Graphics/IGraphics.h"
#include "Graphics/IWindow.h"
#include "Window.h"
#include "Viewport.h"

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

Window::Window(System::Windows::Forms::Control^ control)
{
    m_control = control;
    WindowID hWnd = (WindowID) m_control->Handle.ToPointer();
    An::Graphics::IFactory* f = An::Graphics::IGraphics::Factory();
    m_pUnmanagedPtr = f->Window(hWnd);
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in canvas generation.");
    }
}
//---------------------------------------------------------------------------------------

Window::~Window()
{
    this->!Window();
}
//---------------------------------------------------------------------------------------

Window::!Window()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

Viewport^ Window::View::get()
{
    if(m_viewport == nullptr)
    {
        m_viewport = gcnew Viewport(((An::Graphics::IWindow*) m_pUnmanagedPtr)->Viewport(), this);
    }
    return m_viewport;
}
//---------------------------------------------------------------------------------------

System::Windows::Forms::Control^ Window::Control::get()
{
    return m_control;
}
//---------------------------------------------------------------------------------------

int Window::Height::get()
{
    return ((An::Graphics::IWindow*) m_pUnmanagedPtr)->Height();
}
//---------------------------------------------------------------------------------------

void Window::Height::set(int h)
{
    ((An::Graphics::IWindow*) m_pUnmanagedPtr)->SetSize(Width, h);
}
//---------------------------------------------------------------------------------------

int Window::Width::get()
{
    return ((An::Graphics::IWindow*) m_pUnmanagedPtr)->Width();
}
//---------------------------------------------------------------------------------------

void Window::Width::set(int w)
{
    ((An::Graphics::IWindow*) m_pUnmanagedPtr)->SetSize(w, Height);
}
//---------------------------------------------------------------------------------------

void Window::SetSize(const int width, const int height)
{
    ((An::Graphics::IWindow*) m_pUnmanagedPtr)->SetSize(width, height);
}
//---------------------------------------------------------------------------------------

void* Window::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

