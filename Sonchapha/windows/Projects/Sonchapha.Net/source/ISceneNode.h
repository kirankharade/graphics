//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;
interface class IScene;
ref class Material;
//---------------------------------------------------------------------------------------

public interface class ISceneNode
{
    IScene^ Scene();

    ISceneNode^ Parent();

    bool AddChild(ISceneNode^ pChildNode);

    bool DetachChild(ISceneNode^ pChildNode);

    bool DetachChild(System::String^ nodeName);

    bool DetachChild(System::Int64 nodeId);

    void RemoveFromScene();

    bool IsChildNode(System::String^ name);

    bool IsChildNode(System::Int64 id);

    ISceneNode^ Child(System::String^ name);

    ISceneNode^ Child(System::Int64 id);

    bool IsDescendentNode(System::String^ name);

    bool IsDescendentNode(System::Int64 id);

    System::Int32 ChildrenCount();

    System::Collections::Generic::List<ISceneNode^>^ Children();

   property System::Int64 ID 
   {
       System::Int64 get(void);
   }

   property bool Visible 
   {
       bool get(void);
       void set(bool);
   }

   property System::String^ Name 
   {
       System::String^ get(void);
       void set(System::String^);
   }

   property Material^ NodeMaterial 
   {
       Material^ get(void);
       void set(Material^);
   }

    void* UnmanagedPtr();

};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
