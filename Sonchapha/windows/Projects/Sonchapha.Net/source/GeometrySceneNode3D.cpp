#include "Gfx.h"
#include "Graphics/GraphicsIncludes.h"
#include "Graphics/IGraphics.h"
#include "Graphics/Scene/ISceneNode.h"
#include "Graphics/Scene/C3DScene.h"
#include "Graphics/Scene/C3DSceneNode.h"
#include "Graphics/Scene/I3DGeometry.h"
#include "Graphics/Scene/C3DGeometrySceneNode.h"
#include "ISceneNode.h"
#include "SceneNode3D.h"
#include "IGeometry3D.h"
#include "GeometrySceneNode3D.h"
#include "Converter.h"
//---------------------------------------------------------------------------------------

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

GeometrySceneNode3D::GeometrySceneNode3D(void* unmanagedPtr)
: SceneNode3D(unmanagedPtr)
{
}
//---------------------------------------------------------------------------------------

GeometrySceneNode3D::~GeometrySceneNode3D()
{
    this->!GeometrySceneNode3D();
}
//---------------------------------------------------------------------------------------

GeometrySceneNode3D::!GeometrySceneNode3D()
{
}
//---------------------------------------------------------------------------------------

bool GeometrySceneNode3D::AddDrawable(IGeometry3D^ drawable)
{
	An::Graphics::I3DGeometry* g = (An::Graphics::I3DGeometry*) drawable->UnmanagedPtr();
	return ((An::Graphics::C3DGeometrySceneNode*) m_pUnmanagedPtr)->AddDrawable(g);
}
//---------------------------------------------------------------------------------------

bool GeometrySceneNode3D::RemoveDrawable(IGeometry3D^ drawable)
{
	An::Graphics::I3DGeometry* g = (An::Graphics::I3DGeometry*) drawable->UnmanagedPtr();
	return ((An::Graphics::C3DGeometrySceneNode*) m_pUnmanagedPtr)->RemoveDrawable(g);
}
//---------------------------------------------------------------------------------------

