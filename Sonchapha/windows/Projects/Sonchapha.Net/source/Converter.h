
//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "Graphics/CColor.h"
#include <iostream>
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

public ref class Converter
{
public:

	static System::String^ Convert(const std::string& str);

	static std::string Convert(System::String^ str);

	static System::Drawing::Color Convert(const An::Graphics::CColor& color);

	static An::Graphics::CColor Convert(System::Drawing::Color str);

};
//---------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
