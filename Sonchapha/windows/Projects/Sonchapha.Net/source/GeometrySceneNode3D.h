//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Defines.h"
#include "SceneNode3D.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;

interface class IGeometry3D;

public ref class GeometrySceneNode3D : SceneNode3D
{

public:

    GeometrySceneNode3D(void* unmanagedPtr);

    ~GeometrySceneNode3D(void);

    !GeometrySceneNode3D(void);

public:

    bool AddDrawable(IGeometry3D^ drawable);

    bool RemoveDrawable(IGeometry3D^ drawable);

};
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
