#include <Graphics/CBackGround.h>
#include "Window.h"
#include "Viewport.h"
#include "Layer3D.h"
#include "BackGround.h"
#include "Converter.h"

using namespace Sonchapha::NET;
using namespace An::Graphics;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

BackGround::BackGround(void* unmanagedPtr, Viewport^ parentViewport)
    : m_viewport(parentViewport)
    , m_pUnmanagedPtr(unmanagedPtr)
{
    if(m_pUnmanagedPtr == nullptr)
    {
        throw gcnew System::Exception("Error in background creation.");
    }
}
//---------------------------------------------------------------------------------------

BackGround::~BackGround()
{
    this->!BackGround();
}
//---------------------------------------------------------------------------------------

BackGround::!BackGround()
{
    m_pUnmanagedPtr = nullptr;
}
//---------------------------------------------------------------------------------------

Viewport^ BackGround::View::get()
{
    return m_viewport;
}
//---------------------------------------------------------------------------------------

void* BackGround::UnmanagedPtr()
{
    return m_pUnmanagedPtr;
}
//---------------------------------------------------------------------------------------

void BackGround::SetColors(System::Drawing::Color color)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColors(Converter::Convert(color));
}
//---------------------------------------------------------------------------------------

void BackGround::SetColors(System::Drawing::Color color1,
                System::Drawing::Color color2)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColors(Converter::Convert(color1), 
		Converter::Convert(color2));
}
//---------------------------------------------------------------------------------------

void BackGround::SetColors(System::Drawing::Color color1,
                System::Drawing::Color color2,
                System::Drawing::Color color3)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColors(Converter::Convert(color1), 
		Converter::Convert(color2),
		Converter::Convert(color3));
}
//---------------------------------------------------------------------------------------

void BackGround::SetColors(System::Drawing::Color color1,
                System::Drawing::Color color2,
                System::Drawing::Color color3,
                System::Drawing::Color color4)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColors(Converter::Convert(color1), 
		Converter::Convert(color2),
		Converter::Convert(color3),
		Converter::Convert(color4));
}
//---------------------------------------------------------------------------------------

void BackGround::SetColors(System::Drawing::Color color1, //bottomLeftColor
                System::Drawing::Color color2, //bottomRightColor
                System::Drawing::Color color3, //topRightColor
                System::Drawing::Color color4, //topLeftColor 
                System::Drawing::Color centreColor)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColors(Converter::Convert(color1), 
		Converter::Convert(color2),
		Converter::Convert(color3),
		Converter::Convert(color4),
		Converter::Convert(centreColor));
}
//---------------------------------------------------------------------------------------

BackGroundType BackGround::Type::get(void)
{
	return (BackGroundType) ((An::Graphics::CBackGround*) m_pUnmanagedPtr)->Type();
}
//---------------------------------------------------------------------------------------

void BackGround::Type::set(BackGroundType t)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetType((An::Graphics::EBackGroundType) t);
}
//---------------------------------------------------------------------------------------

BackGroundColorType BackGround::ColorType::get(void)
{
	return (BackGroundColorType) ((An::Graphics::CBackGround*) m_pUnmanagedPtr)->ColoringType();
}
//---------------------------------------------------------------------------------------

void BackGround::ColorType::set(BackGroundColorType t)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetColoringType((An::Graphics::EBackGroundColorType) t);
}
//---------------------------------------------------------------------------------------

BackGroundTextureType BackGround::TextureType::get(void)
{
	return (BackGroundTextureType) ((An::Graphics::CBackGround*) m_pUnmanagedPtr)->TextureLayoutType();
}
//---------------------------------------------------------------------------------------

void BackGround::TextureType::set(BackGroundTextureType t)
{
	((An::Graphics::CBackGround*) m_pUnmanagedPtr)->SetTextureLayoutType((An::Graphics::EBackGroundTextureType) t);
}
//---------------------------------------------------------------------------------------

System::Collections::Generic::List<System::Drawing::Color>^ BackGround::Colors()
{
	System::Collections::Generic::List<System::Drawing::Color>^ colors = gcnew System::Collections::Generic::List<System::Drawing::Color>();
	const int NUM_COLORS = 5;
	An::Graphics::CColor* unmanagedColors = ((An::Graphics::CBackGround*) m_pUnmanagedPtr)->Colors();
	if(colors != nullptr)
	{
		for(unsigned int i = 0; i < NUM_COLORS; i++)
		{
			auto color = unmanagedColors[i];
			auto c = Converter::Convert(color);
			colors->Add(c);
		}
	}
	return colors;
}
//---------------------------------------------------------------------------------------


