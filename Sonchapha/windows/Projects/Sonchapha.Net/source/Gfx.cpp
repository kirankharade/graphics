#include "Gfx.h"
#include "../../../../source/Graphics/IGraphics.h"
#include "../../../../source/Graphics/IFactory.h"

using namespace An::Graphics;
using namespace Sonchapha::NET;
using namespace System::Windows::Forms;
using namespace System::Drawing;
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------

void Gfx::Initialize(OpenGLEngineType engineType, System::Windows::Forms::Control^ control)
{
    WindowID hWnd = (WindowID) control->Handle.ToPointer();
    An::Graphics::IGraphics::InitializeGraphics((WindowID) hWnd, EWindowingSystemType_Win32, (EOpenGLEngineType) engineType);
    m_pUnmanagedPtr = An::Graphics::IGraphics::Gfx();
}
//---------------------------------------------------------------------------------------

void Gfx::Initialize(OpenGLEngineType engineType)
{
    An::Graphics::IGraphics::InitializeGraphics(EWindowingSystemType_Win32, (EOpenGLEngineType) engineType);
    m_pUnmanagedPtr = An::Graphics::IGraphics::Gfx();
}
//---------------------------------------------------------------------------------------

//void Gfx::Initialize(OpenGLEngineType engineType, System::Windows::Forms::Control^ control, SGLContextParams contextParams)
//{
//    HWND hWnd = (HWND) control->Handle.ToPointer();
//
//    IGraphics::InitializeGraphics(hWnd, contextParams, EWindowingSystemType_Win32, (EOpenGLEngineType) engineType);
//}
////---------------------------------------------------------------------------------------

