//---------------------------------------------------------------------------------------
#pragma once
//---------------------------------------------------------------------------------------
#include "Includes.h"
//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_START_NAMESPACE
//---------------------------------------------------------------------------------------

using namespace System::Windows::Forms;
using namespace System::Drawing;

public ref class Scene3D : IScene
{

public:

    Scene3D(void* unmanagedPtr);

    ~Scene3D(void);

    !Scene3D(void);

public:

    virtual ISceneNode^ BaseNode();

    virtual ISceneNode^ SearchInScene(System::String^ name);

    virtual ISceneNode^ SearchInScene(System::Int64 id);

    virtual bool ExistsInScene(System::String^ nodeName);

    virtual bool ExistsInScene(System::Int64 nodeId);

    virtual property System::Int64 ID 
    {
        System::Int64 get(void);
    }

    virtual property bool Dirty 
    {
        bool get(void);
        void set(bool);
    }

    virtual property System::String^ Name 
    {
        System::String^ get(void);
        void set(System::String^);
    }

    void* UnmanagedPtr();

protected:

    void*  m_pUnmanagedPtr;
};
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
AN_GRAPHICS_NET_END_NAMESPACE
//---------------------------------------------------------------------------------------
